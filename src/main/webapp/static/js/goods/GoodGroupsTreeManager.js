/**
 * Класс наполнения и управления группами товаров. Внутрь передается признак административная ли учетная запись
 * @param adminMode
 * @constructor
 */
SHOP.GoodGroupsTreeManager = function () {
    this.htmlTreeElement = document.getElementById('catalogTree');
    SHOP.goodGroupsTreeManager = this;

    this.selectedGroup = undefined;

    // Кнопка удаления группы
    this.removeActionButton = document.createElement("div");
    this.removeActionButton.setAttribute("class", "goodGroupActionButton buttonIcon buttonIconRemove");
    this.removeActionButton.setAttribute("title", "Удалить группу");
    this.removeActionButton.onclick = this.__removeActionButtonHandler.bind(this);

    // Кнопка перемещения вверх
    this.moveUpActionButton = document.createElement("div");
    this.moveUpActionButton.setAttribute("class", "goodGroupActionButton buttonIcon buttonIconUp");
    this.moveUpActionButton.setAttribute("title", "Переместить вверх");

    // Кнопка перемещения  вниз
    this.moveDownActionButton = document.createElement("div");
    this.moveDownActionButton.setAttribute("class", "goodGroupActionButton buttonIcon buttonIconDown");
    this.moveDownActionButton.setAttribute("title", "Переместить вниз");

    // Кнопка добавления подгруппы
    this.addSubGroupActionButton = document.createElement("div");
    this.addSubGroupActionButton.setAttribute("class", "goodGroupActionButton buttonIcon buttonIconPlus");
    this.addSubGroupActionButton.setAttribute("title", "Добавить подгруппу");

    // Кнопка редактирования
    this.editActionButton = document.createElement("div");
    this.editActionButton.setAttribute("class", "goodGroupActionButton buttonIcon buttonIconEdit");
    this.editActionButton.setAttribute("title", "Редактировать");

};

SHOP.GoodGroupsTreeManager.prototype = {
    constructor: SHOP.GoodGroupsTreeManager,

    /**
     * Заполнить дерево групп
     */
    fillGoodGroupsTree: function () {
        this.htmlTreeElement.innerHTML = "";

        var dataReady = function (result) {
            const goodGroups = JSON.parse(result.result);

            if (SHOP.adminMode) {
                this.__addManageTopGroupsPanel();
            }

            goodGroups.forEach(function (goodGroup) {
                this.__addGroupItem(goodGroup);
                if (goodGroup.children) {
                    // подгруппы
                    goodGroup.children.forEach(function (subGroup) {
                        this.__addGroupItem(subGroup);
                    }.bind(this));
                }
            }.bind(this));

        };

        SHOP.apiRestClient.getCatalogTree(dataReady.bind(this));
    },
    //------------------------------------------------------------------------------

    /**
     * Добавить в дерево групп на экране пункт с группой верхнего уровня
     * @param goodGroup
     * @private
     */
    __addGroupItem: function (goodGroup) {
        // плашка, где все данные группы, ее кнопки и подгруппы
        goodGroup.groupContainer = document.createElement("div");
        goodGroup.groupContainer.setAttribute("class", "groupContentContainer");
        goodGroup.groupContainer.setAttribute("id", "group_" + goodGroup.id);
        goodGroup.groupContainer.goodGroupRecord = goodGroup;

        // Место для названия группы и кнопок
        goodGroup.titleContainer = document.createElement("div");
        goodGroup.groupContainer.appendChild(goodGroup.titleContainer);
        goodGroup.titleContainer.classList.add("good-groups-group-title-container");
        goodGroup.titleContainer.classList.add("form-animate-text-shadow");

        goodGroup.titleContainer.onclick = this.__openGoodsForGroup.bind(this, goodGroup);

        if (goodGroup.topLevel) {
            goodGroup.titleContainer.innerHTML = "<div style='float: left;'><h1>" + goodGroup.name + "</h1></div>";
        } else {
            goodGroup.titleContainer.innerHTML = "<div style='float: left;'><h2>" + goodGroup.name + "</h2></div>";
        }

        if (!goodGroup.topLevel) {
            const groupDiv = document.getElementById("group_" + goodGroup.parentId);
            groupDiv.goodGroupRecord.groupContainer.appendChild(goodGroup.groupContainer);
        }

        if (this.adminPanel) {
            if (goodGroup.topLevel) {
                this.htmlTreeElement.insertBefore(goodGroup.groupContainer, this.adminPanel);
            }
            //Элементы управления
            goodGroup.titleContainer.addEventListener('mouseenter', this.__showActionButtons.bind(this), false);
            goodGroup.titleContainer.addEventListener('mouseleave', this.__hideActionButtons.bind(this), false);

        } else {
            if (goodGroup.topLevel) {
                this.htmlTreeElement.appendChild(goodGroup.groupContainer);
            }
        }


        if (SHOP.adminMode) {

        }
    },
    //------------------------------------------------------------------------------

    /**
     * Метод срабатывает при наведении мыши на панельку с названием группы. При этом он отображает на ней, если разрешено
     * кнопки управления группой
     * @param mouseEvent
     * @private
     */
    __showActionButtons: function (mouseEvent) {
        const groupDiv = mouseEvent.currentTarget;

        this.__hideActionButtons();

        // кнопка удаления
        groupDiv.appendChild(this.removeActionButton);

        // Кнопка перемещения  вниз
        groupDiv.appendChild(this.moveDownActionButton);
        this.moveDownActionButton.onclick = this.__moveDownActionButtonHandler.bind(this, groupDiv.parentNode);

        // Кнопка перемещения вверх
        groupDiv.appendChild(this.moveUpActionButton);
        this.moveUpActionButton.onclick = this.__moveUpActionButtonHandler.bind(this, groupDiv.parentNode);


        // кнопка добавления подгруппы
        if (groupDiv.parentNode.goodGroupRecord.topLevel) {
            groupDiv.appendChild(this.addSubGroupActionButton);
            this.addSubGroupActionButton.onclick = this.__addGoodGroupHandler.bind(this, groupDiv.parentNode.goodGroupRecord.id);
        }

        // кнопка редактирования подгруппы
        groupDiv.appendChild(this.editActionButton);
        this.editActionButton.onclick = this.__editActionButtonHandler.bind(this, groupDiv.parentNode);

    },
    //------------------------------------------------------------------------------

    /**
     * Скрывает кноки управления группами с панели группы, если они отображались где-то ранее
     * @private
     */
    __hideActionButtons: function () {
        // кнопка удаления
        if (this.removeActionButton.parentNode) {
            this.removeActionButton.parentNode.removeChild(this.removeActionButton);
        }
        this.removeActionButton.parentNode = undefined;

        // Кнопка перемещения вверх
        if (this.moveUpActionButton.parentNode) {
            this.moveUpActionButton.parentNode.removeChild(this.moveUpActionButton);
        }
        this.moveUpActionButton.parentNode = undefined;

        // Кнопка перемещения  вниз
        if (this.moveDownActionButton.parentNode) {
            this.moveDownActionButton.parentNode.removeChild(this.moveDownActionButton);
        }
        this.moveDownActionButton.parentNode = undefined;

        // кнопка добавления подгруппы
        if (this.addSubGroupActionButton.parentNode) {
            this.addSubGroupActionButton.parentNode.removeChild(this.addSubGroupActionButton);
        }
        this.addSubGroupActionButton.parentNode = undefined;

        // кнопка добавления редактирования
        if (this.editActionButton.parentNode) {
            this.editActionButton.parentNode.removeChild(this.editActionButton);
        }
        this.editActionButton.parentNode = undefined;
    },
    //------------------------------------------------------------------------------


    //------------------------------------------------------------------------------

    /**
     * Реакция на кнопку удаления группы товаров
     * @param mouseEvent
     * @private
     */
    __removeActionButtonHandler: function (mouseEvent) {
        const groupDiv = mouseEvent.currentTarget.parentNode.parentNode;
        const goodGroup = groupDiv.goodGroupRecord;
        if (goodGroup.children && goodGroup.children.length > 0) {
            $.alert("Группа содержит подгруппы. Удаление невозможно.");
        } else {
            $.confirm({
                title: "Удаление группы",
                content: "Удалить группу товаров \"" + goodGroup.name + "\"",
                buttons: {
                    Удалить: function () {
                        SHOP.apiRestClient.removeGoodGroup(function (response) {
                            if (!response.failed) {
                                groupDiv.parentNode.removeChild(groupDiv);

                                // если это была подгруппа, то удалить ее из списка у главной группы
                                if (!goodGroup.topLevel) {
                                    const parenGroupRecord = document.getElementById("group_" + goodGroup.parentId).goodGroupRecord;
                                    parenGroupRecord.children = parenGroupRecord.children.filter(function (subGroup) {
                                        subGroup.id != goodGroup.id
                                    }.bind(this));
                                }
                            }
                        }.bind(this), goodGroup.id)
                    },
                    Отмена: function () {
                    }
                }
            });
        }
    },
    //------------------------------------------------------------------------------

    /**
     * Реакция на кнопку перемещения группы по списку вверх
     * @param groupDiv
     * @private
     */
    __moveUpActionButtonHandler: function (groupDiv) {
        const goodGroup = groupDiv.goodGroupRecord;
        SHOP.apiRestClient.moveUpOrDownGoodGroup(function (response) {
            if (!response.failed) {
                if (response.result != "null") {
                    // перемещение выполнено. поменяем местами с пришедшим
                    const goodGroupElementChangedWith = document.getElementById("group_" + JSON.parse(response.result).id);
                    groupDiv.parentNode.removeChild(groupDiv);
                    goodGroupElementChangedWith.parentNode.insertBefore(groupDiv, goodGroupElementChangedWith);
                }
            }
        }.bind(this), goodGroup.id, "UP");
    },
    //------------------------------------------------------------------------------

    /**
     * Реакция на кнопку перемещения группы по списку вниз
     * @param groupDiv
     * @private
     */
    __moveDownActionButtonHandler: function (groupDiv) {
        const goodGroup = groupDiv.goodGroupRecord;
        SHOP.apiRestClient.moveUpOrDownGoodGroup(function (response) {
            if (!response.failed) {
                if (response.result != "null") {
                    // перемещение выполнено. поменяем местами с пришедшим
                    const goodGroupElementChangedWith = document.getElementById("group_" + JSON.parse(response.result).id);
                    groupDiv.parentNode.removeChild(goodGroupElementChangedWith);
                    groupDiv.parentNode.insertBefore(goodGroupElementChangedWith, groupDiv);
                }
            }
        }.bind(this), goodGroup.id, "DOWN");
    },
    //------------------------------------------------------------------------------

    /**
     * Создать панельку для добавления новой группы верхнего уровня
     * @private
     */
    __addManageTopGroupsPanel: function () {
        this.adminPanel = document.createElement("div");
        this.adminPanel.setAttribute("class", "catalogAdminPanel");
        this.htmlTreeElement.appendChild(this.adminPanel);


        const addButton = document.createElement("div");
        addButton.onclick = this.__addGoodGroupHandler.bind(this, null);
        addButton.setAttribute("class", "buttonIcon buttonIconPlus");
        addButton.setAttribute("title", "Добавить основную группу товаров");
        this.adminPanel.appendChild(addButton);

    },
    //------------------------------------------------------------------------------

    /**
     * Реакция на кнопку добавления новой группы товара
     * @private
     */
    __addGoodGroupHandler: function (parentId) {

        SHOP.navigator.openGoodGroupForm(null, parentId);

    },
    //=============================================================================================

    /**
     * Реакция на кнопку редактирования группы
     * @param groupDiv
     * @private
     */
    __editActionButtonHandler: function (groupDiv) {
        const goodGroup = groupDiv.goodGroupRecord;
        SHOP.navigator.openGoodGroupForm(goodGroup.id, goodGroup.parentId);
    },
    //=============================================================================================

    /**
     * Открыть товары и подгруппы выбранной группы или подгруппы
     * @param goodGroup
     * @private
     */
    __openGoodsForGroup: function (goodGroup, event) {
        if (event.target == goodGroup.titleContainer || event.target.nodeName == "H1" || event.target.nodeName == "H2") {

            // снять отметку с ранее выбранной группы
            if (this.selectedGroup) {
                this.selectedGroup.titleContainer.classList.remove("good-groups-group-title-container-selected");
            }
            this.selectedGroup = goodGroup;
            this.selectedGroup.titleContainer.classList.add("good-groups-group-title-container-selected");

            if (goodGroup) {
                SHOP.goodsListFilter.group = goodGroup;
                if (!goodGroup.topLevel) {
                    SHOP.goodsListFilter.topLevelGroup = document.getElementById("group_" + goodGroup.parentId).goodGroupRecord;
                } else {
                    SHOP.goodsListFilter.topLevelGroup = undefined;
                }
            }

            SHOP.navigator.openGoodsList();
        }
    },
    //------------------------------------------------------------------------------

    /**
     * Отобразить содержимое группы, после перехода к выбранной группе товаров
     * @param goodsListFilter
     * @private
     */
    showGoodGroupContainment: function () {
        // отобразим подгруппы, если есть
        this.__showSubGroupOfSelectedGroup();
        // отобразим товары, если есть
        SHOP.goodsListManager.__showGoodsOfSelectedGroup();
    },
    //------------------------------------------------------------------------------

    /**
     * Отобразить подгруппы выбранной группы верхнего уровня
     * @param goodsListFilter
     */
    __showSubGroupOfSelectedGroup: function () {
        const groupsListContainer = document.getElementById("groupRefContainer");
        const subGroups = JSON.parse(document.getElementById("subGroupsListScript").innerHTML);
        groupsListContainer.innerHTML = "";

        if (subGroups.length == 0) {
            document.getElementById("goodsSubGroupListRow").style.display = "none";
            document.getElementById("goodsSubGroupListSeparatorRow").style.display = "none";
        } else {
            document.getElementById("goodsSubGroupListRow").style.display = "";
            document.getElementById("goodsSubGroupListSeparatorRow").style.display = "";
        }

        subGroups.forEach(function (subGroup) {
            const subGroupContainer = document.createElement("div");
            subGroupContainer.setAttribute("class", "goodsListSubGroupContainer goodsListSubGroupContainer-ext");

            const subGroupImg = document.createElement("img");
            subGroupImg.setAttribute("class", "goodsListSubGroupIcon");
            subGroupImg.setAttribute("src", "api/buyer/images/200x200/" + subGroup.iconId + ".png");
            subGroupContainer.appendChild(subGroupImg);

            const subGroupTitle = document.createElement("div");
            subGroupTitle.setAttribute("class", "goodsListSubGroupTitle");
            subGroupTitle.innerHTML = "<h1>" + subGroup.name + "</h1>";
            subGroupContainer.appendChild(subGroupTitle);

            subGroup.titleContainer = subGroupImg;
            subGroupContainer.onclick = this.__openGoodsForGroup.bind(this, subGroup);

            groupsListContainer.appendChild(subGroupContainer);
        }.bind(this));

    },
    //=============================================================================================

};