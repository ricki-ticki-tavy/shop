/**
 * Опции для формы создания / редактирования товара
 * @constructor
 */

SHOP.EditGoodForm = function () {
    SHOP.editGoodForm = this;

    this.imageGalery = undefined;
    this.attributeRows = undefined;
};


SHOP.EditGoodForm.prototype = {
    constructor: SHOP.EditGoodForm,

    /**
     * Заполнить выпадающий список для выбора группы.
     * @param parentGroupId - код родительской группы. Для заполнения корневыми группами undefined или -1
     * @param selectorId - id элемента выпадающего списка.
     * @param defaultValue - значение, после заполнения списка
     * @param firstElement - Элемент, который будет добавлен перед загруженными
     * @param callback - коллбэк после заполнения
     */
    fillGroupSelector: function (parentGroupId, selectorId, defaultValue, firstElement, callback) {
        SHOP.apiRestClient.getGroupList(function (response) {
                if (!response.failed) {
                    const selector = document.getElementById(selectorId);
                    var innerHtml = firstElement == undefined
                        ? ""
                        : "<option value='" + firstElement.id + "' "
                        + ((defaultValue == undefined || defaultValue == -1 ) ? "selected" : "") + ">" + firstElement.name + "</option>";
                    const groups = JSON.parse(response.result);
                    groups.result.forEach(function (group) {
                        innerHtml += "<option value='" + group.id + "' "
                            + ((defaultValue != undefined && defaultValue != -1 && defaultValue == group.id) ? "selected" : "")
                            + ">" + group.name + "</option>";
                    }.bind(this));

                    selector.innerHTML = innerHtml;

                    if (callback) {
                        callback(response);
                    }
                }
            }.bind(this)
            , parentGroupId);
    },
    //--------------------------------------------------------------------------------------------

    /**
     * Подготовка формы после ее загрузки для создания или редактирования товара
     * @param goodId
     * @param parentGroupId
     * @param response
     */
    prepareForm: function (goodId) {
        this.fillGroupSelector("-1"
            , "goodGroup"
            , SHOP.goodsListFilter.topLevelGroup ? SHOP.goodsListFilter.topLevelGroup.id : SHOP.goodsListFilter.group.id
            , undefined
            , function () {
                // после обновления группы первого уровня загрузим список групп нижнего уровня
                this.fillGroupSelector(
                    SHOP.goodsListFilter.topLevelGroup ? SHOP.goodsListFilter.topLevelGroup.id : SHOP.goodsListFilter.group.id
                    , "goodSubGroup"
                    , SHOP.goodsListFilter.topLevelGroup ? SHOP.goodsListFilter.group.id : -1
                    , {id: -1, name: "Нет подгруппы"}
                    , function () {
                        const ids = document.getElementById("imageIds").value.split(',');
                        this.imageGalery = SHOP.imageGallery("goodFormImageGallery", ids, 100, 100, true, "api/buyer/images");
                        this.__updateAttributeList();

                    }.bind(this)
                );
            }.bind(this));
    },
    //--------------------------------------------------------------------------------------------

    /**
     * Обработка события выбора группы верхнего уровня на форме
     */
    topLevelGroupChanged: function () {
        this.fillGroupSelector(
            document.getElementById("goodGroup").value
            , "goodSubGroup"
            , -1
            , {id: -1, name: "Нет подгруппы"}
            , this.__updateAttributeList.bind(this)
        );
    },
    //--------------------------------------------------------------------------------------------

    __updateAttributeList: function () {
        SHOP.apiRestClient.getTopLevelGroupAttributes(function (response) {
                if (!response.failed) {
                    const attributes = JSON.parse(response.result);
                    // удалим старые элементы
                    if (this.attributeRows) {
                        // удалим старые атрибуты группы товара
                        this.attributeRows.forEach(function (row) {
                            row.parentNode.removeChild(row);
                        }.bind(this));
                        this.attributeRows = undefined;
                    }

                    this.attributeRows = new Array();
                    attributes.forEach(function(attribute){
                        const newRow = document.createElement("tr");
                        newRow.classList.add("formCompactItemRow");
                        var cell = document.createElement("td");
                        cell.innerHTML = attribute.name;
                        cell.classList.add("formParamCaption");
                        newRow.appendChild(cell);
                        cell = document.createElement("td");
                        newRow.appendChild(cell);

                        const attributeSelector = document.createElement("select");
                        attributeSelector.id = "goodAttribute_" + attribute.id;
                        attributeSelector.name = "goodAttribute_" + attribute.id;
                        attributeSelector.classList.add("formParamValue");
                        cell.appendChild(attributeSelector);

                        var valueOptions = "";
                        attribute.attributeValues.split(";").forEach(function(value){
                            valueOptions += "<option value='" + value + "'>" + value + "</option>";
                        }.bind(this));
                        attributeSelector.innerHTML = valueOptions;

                        const rowAfterAttributes = $("#goodFormTrAfterAttributes")[0];
                        rowAfterAttributes.parentNode.insertBefore(newRow, rowAfterAttributes);

                        this.attributeRows.push(newRow);
                    }.bind(this));

                    // Проставим значения атрибутов товара
                    var attributesValue = document.getElementById("attributesValue").value;
                    if (attributesValue != "") {
                        const attrValues = JSON.parse(attributesValue);
                        attrValues.forEach(function (attrValue) {
                            $("#goodAttribute_" + attrValue.id).val(attrValue.value);
                        }.bind(this));
                    }
                }
            }.bind(this)
            , document.getElementById("goodGroup").value);

    },
    //--------------------------------------------------------------------------------------------

    attachImage: function () {
        const formData = new FormData(document.getElementById("goodForm"));
        // formData.append("photoData", document.getElementById("goodForm"));
        SHOP.apiRestClient.uploadImage(function (response) {
                if (!response.failed) {
                    // все в норме
                    imgId = response.result;
                    this.imageGalery.addImage(imgId);
                    SHOP.ddd = document.getElementById("photoData").files = undefined;
                }
            }.bind(this)
            , formData);
    },
    //--------------------------------------------------------------------------------------------

    /**
     * Сохраняет изменения по товару
     */
    saveOrUpdateGood: function (formElement) {
        document.getElementById("imageIds").value = this.imageGalery.getImageIdsAsString();
        SHOP.apiRestClient.saveOrUpdateGood(
            function (response) {
                if (!response.failed) {
                    SHOP.navigator.openGoodsList();
                }
            }.bind(this)
            , new FormData(formElement));
    },
    //--------------------------------------------------------------------------------------------
};