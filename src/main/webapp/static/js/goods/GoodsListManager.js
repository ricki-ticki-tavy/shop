SHOP.GoodsListManager = function(){
    SHOP.goodsListManager = this;

    // фильтр товаров
    this.goodsFilter = undefined;
};

SHOP.GoodsListManager.prototype = {
    constructor: SHOP.GoodsListManager,

    /**
     * Показать панель с фильтрами товаров
      */
    showFilterPanel: function(){
        document.getElementById("showGoodsFilterButton").style.display = "none";
        document.getElementById("hideGoodsFilterButton").style.display = "";

        $("#filtersContainer").show();
    },
    //====================================================================================

    /**
     * Скрыть панель с фильтрами товаров
     */
    hideFilterPanel: function(){
        document.getElementById("showGoodsFilterButton").style.display = "";
        document.getElementById("hideGoodsFilterButton").style.display = "none";

        $("#filtersContainer").hide();
    },
    //====================================================================================

    /**
     * Отобразить товары выбранной группы товаров. Согласно набору фильтров
     * @param goodsListFilter
     * @private
     */
    __showGoodsOfSelectedGroup: function(){
        SHOP.apiRestClient.getGoodsList(function(response){
            if (!response.failed){
                const container = document.getElementById("goodsListContainer");
                const goods = JSON.parse(response.result);
                goods.forEach(function(good){
                    SHOP.goodWidget(container, good);
                }.bind(this));
            }
        }.bind(this)
        , encodeURI(SHOP.goodsListFilter.toString()));

    },
    //=============================================================================================

    //=============================================================================================
    //=============================================================================================

};