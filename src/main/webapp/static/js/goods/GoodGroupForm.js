SHOP.GoodGroupForm = function () {
    SHOP.goodGroupForm = this;

    this.attributeDialog = undefined;
    this.attributesTable = undefined;
    this.attributeIdCounter = -2;
    this.selectedRow = undefined;
    this.attributDialogMode = undefined;

};

SHOP.GoodGroupForm.prototype = {
    constructor: SHOP.GoodGroupForm,

    prepare: function () {

        if (this.attributeDialog) {
            this.attributeDialog[0].parentNode.removeChild(this.attributeDialog[0])
        }

        this.attributeDialog = $("#goodAttributeFormContainer").dialog({
            autoOpen: false,
            height: 234,
            width: 700,
            modal: true,
            overlay: {
                opacity: 0.4,
                background: "black"
            },
            buttons: {
                "Сохранить": this.appendAttributeToTable.bind(this),
                "Отмена": function () {
                    this.attributeDialog.dialog("close");
                }.bind(this),
            },
            close: function () {
                $("#goodAttributeForm")[0].reset();
                SHOP.apiRestClient.lockCursor.hide();
            },
            open: function () {
                SHOP.apiRestClient.lockCursor.show();
            }.bind(this)
        });

        this.attributeButtonPlus = $("#attributeButtonIconPlus")[0];
        this.attributeButtonEdit = $("#attributeButtonIconEdit")[0];
        this.attributeButtonRemove = $("#attributeButtonIconRemove")[0];
        this.__updateAttributeButtons();

        const strAttributes = $("#groupAttributes")[0].value;
        if (strAttributes && strAttributes != "") {
            const attributes = JSON.parse($("#groupAttributes")[0].value);
            attributes.forEach(function (attribute) {
                this.appendAttributeToTable(null, attribute);
            }.bind(this));
        }

    },
    //-----------------------------------------------------------------------------

    appendAttributeToTable: function (event, attribute) {
        if (attribute) {
            const newRow = document.createElement("tr");
            newRow.innerHTML = "<td>" + attribute.id + "</td>"
                + "<td>" + attribute.name + "</td>"
                + "<td>" + (attribute.showOnWidget ? "Да" : "") + "</td>"
                + "<td>" + attribute.attributeValues + "</td>";
            this.__prepareNewAttributeRowAndAppend(newRow);
        } else if (this.attributDialogMode == "add") {
            const newRow = document.createElement("tr");
            newRow.innerHTML = "<td>" + this.attributeIdCounter-- + "</td>"
                + "<td>" + $("#attributeName")[0].value + "</td>"
                + "<td>" + ($("#attributeShowOnWidget")[0].checked ? "Да" : "") + "</td>"
                + "<td>" + $("#attributeAvailableValues")[0].value + "</td>";
            this.__prepareNewAttributeRowAndAppend(newRow);
        } else if (this.attributDialogMode == "edit") {
            const children = this.selectedRow.children;
            children[1].innerHTML = $("#attributeName")[0].value;
            children[2].innerHTML = $("#attributeShowOnWidget")[0].checked ? "Да" : "";
            children[3].innerHTML = $("#attributeAvailableValues")[0].value;
            this.selectedAttribute = this.__rowToAttribute(this.selectedRow);
        } else {
            $.alert("Неверный формат типа перации");
        }
        this.attributeDialog.dialog("close");
    },
    //-----------------------------------------------------------------------------

    __prepareNewAttributeRowAndAppend: function(newRow){
        newRow.classList.add("good-group-form-attributes-table-row");
        $("#goodGroupFormAttributesTable")[0].appendChild(newRow);
        newRow.onclick = this.__itemRowClickHandler.bind(this);

        $(newRow).draggable({
            // containment: "parent",
            cursor: "move",
            revert: true,
            revertDuration: 0,
            distance: 5,
            scroll: true,
            // helper: "copy",
        });

        // $("#" + this.namePrefix + photoId).droppable({
        //     accept: ".photoSlider-small-image-div",
        //     tolerance: "intersect",
        //     over: this.__onIconOver.bind(this),
        //     out: this.__onIconOut.bind(this),
        //     drop: this.__onIconDropped.bind(this),
        //     over: this.__onIconOver.bind(this),
        // });
    },
    //-----------------------------------------------------------------------------

    __addAttributeDialogOpen: function () {
        this.attributDialogMode = "add";
        this.attributeDialog.dialog('option', 'title', "Добавление атрибута товара");
        this.attributeDialog.dialog("open");
        // предотвратить срабатывание формы, с которой вызван метод
        return false;
    },
    //-----------------------------------------------------------------------------

    __editAttributeDialogOpen: function () {
        if (!this.attributeButtonEdit.disabled) {
            this.attributDialogMode = "edit";
            this.attributeDialog.dialog('option', 'title', "Редактирование атрибута \"" + this.selectedAttribute.name + "\"");
            $("#attributeName")[0].value = this.selectedAttribute.name;
            $("#attributeShowOnWidget")[0].checked = this.selectedAttribute.showOnWidget;
            $("#attributeAvailableValues")[0].value = this.selectedAttribute.attributeValues;
            this.attributeDialog.dialog("open");
        }
        return false;
    },
    //-----------------------------------------------------------------------------

    /**
     * Реакция на кнопку удаления атрибута
     * @private
     */
    __deleteAttributeButtonHandler: function () {
        if (!this.attributeButtonRemove.disabled) {
            $.confirm({
                title: "Удаление атрибута",
                content: "Удалить атрибут \"" + this.selectedAttribute.name + "\" ?",
                buttons: {
                    Удалить: function () {
                        $("#goodGroupFormAttributesTable")[0].removeChild(this.selectedRow);
                        this.selectedRow = undefined;
                        this.selectedAttribute = undefined;
                        this.__updateAttributeButtons();
                    }.bind(this)
                    ,
                    Отмена: function () {
                    }
                }
            });

        }
    },
    //-----------------------------------------------------------------------------

    __updateAttributeButtons: function () {
        this.attributeButtonPlus.disabled = false;
        this.__setButtonDisabled(this.attributeButtonEdit, this.selectedRow == undefined);
        this.__setButtonDisabled(this.attributeButtonRemove, this.selectedRow == undefined);
    },
    //-----------------------------------------------------------------------------

    /**
     * Стиль для разрешенной и запрещенной кнопки
     * @param button
     * @private
     */
    __setButtonDisabled: function (button, disabled) {
        button.disabled = disabled;
        if (button.disabled) {
            button.classList.add("buttonIcon-disabled");
        } else {
            button.classList.remove("buttonIcon-disabled");
        }
    },
    //-----------------------------------------------------------------------------

    /**
     * Преобразовать строку таблицы в структуру атрибута
     * @param rowElement
     * @private
     */
    __rowToAttribute: function (rowElement) {
        const children = rowElement.children;
        return {
            id: children[0].innerHTML,
            name: children[1].innerHTML,
            showOnWidget: children[2].innerHTML == "Да",
            attributeValues: children[3].innerHTML
        }
    },
    //-----------------------------------------------------------------------------

    /**
     * Реакция на клик по строке с атрибутом
     * @private
     */
    __itemRowClickHandler: function (event) {
        const row = event.currentTarget;
        if (this.selectedRow) {
            this.selectedRow.classList.remove("good-group-form-attributes-tr-selected");
        }
        this.selectedRow = row;
        this.selectedRow.classList.add("good-group-form-attributes-tr-selected");

        this.__updateAttributeButtons();

        this.selectedAttribute = this.__rowToAttribute(row);
    },
    //-----------------------------------------------------------------------------


    /**
     * Сохранить изменения или создать новую группу товаров
     * @param backUrl
     * @param groupName
     * @param currentId
     * @param parentId
     */
    saveOrUpdateGroup: function (form) {

        if ($("#goodGroupFormAttributesTable").length > 0) {
            // это группа с атрибутами. Соберем их все кучкой
            const attributes = new Array();
            // все строки таблицы с третьей (пропуск тулбара и заголовков)
            var children = $("#goodGroupFormAttributesTable")[0].children;
            for (var rowIndex = 1; rowIndex < children.length; rowIndex++) {
                attributes.push(this.__rowToAttribute(children[rowIndex]));
            }

            $("#groupAttributes")[0].value = JSON.stringify(attributes);
        } else {
            $("#groupAttributes")[0].value = "";
        }

        const formData = new FormData(form);

        SHOP.apiRestClient.saveOrUpdateGoodGroup(function (response) {
            if (!response.failed) {
                const goodGroup = JSON.parse(response.result);
                const currentId = form.elements.currentId.value;

                if (currentId == "-1") {
                    // Это добавление группы
                    // если не группа верхнего уровня, то добавить чилда в структуру групп
                    if (!goodGroup.topLevel) {
                        const parentGoodGroup = document.getElementById("group_" + goodGroup.parentId).goodGroupRecord;
                        if (parentGoodGroup.children) {
                            parentGoodGroup.children.push(goodGroup);
                        } else {
                            parentGoodGroup.children = [goodGroup];
                        }
                    }
                    SHOP.goodGroupsTreeManager.__addGroupItem(goodGroup);
                } else {
                    // Это было редактирование
                    const editedGoodGroup = document.getElementById("group_" + goodGroup.id).goodGroupRecord;
                    editedGoodGroup.titleContainer.firstElementChild.firstElementChild.innerHTML = goodGroup.name;
                    editedGoodGroup.name = goodGroup.name;
                }
                SHOP.navigator.openGoodsList();
            }
        }.bind(this), formData);
    },
    //------------------------------------------------------------------------------

//-----------------------------------------------------------------------------

};