SHOP.GoodsListFilter = function () {
    this.__topLevelGroup = undefined;
    this.__group = undefined;
    SHOP.goodsListFilter = this;
};

SHOP.GoodsListFilter.prototype = {
    constructor: SHOP.GoodsListFilter,

    toString: function () {
        const anFilter = new Map();
        anFilter.set("groupId",  this.group.id);

        var result = "";
        anFilter.forEach(function(value, key){
            result += (result == "" ? "" : ",") + "\"" + key + "\":\"" + value +"\"";
        }.bind(this));
        return "{" + result + "}";
    },
    //-------------------------------------------------------------------------------------

    /**
     * Возвращает в виде параметров выбранные коды групп
     */
    groupsToString: function(){
        var result = "";
        if (this.group) {
            result = "groupId=" + this.group.id;
        }
        if (this.topLevelGroup) {
            result += (result == "" ? "" : "&") + "topGroupId=" + this.topLevelGroup.id;
        }
        return result;
    },
    //-------------------------------------------------------------------------------------

    set topLevelGroup(value) {
        this.__topLevelGroup = value;
    },

    get topLevelGroup() {
        return this.__topLevelGroup;
    },

    set group(value) {
        this.__group = value;
    },

    get group() {
        return this.__group;
    }


};