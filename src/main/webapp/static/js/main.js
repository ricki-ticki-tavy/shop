function start(adminMode) {

    SHOP.adminMode = adminMode;
    new SHOP.ApiRestClient();
    new SHOP.ShopingCart();
    new SHOP.GoodGroupsTreeManager();
    new SHOP.ProfileManager();
    new SHOP.GoodsListManager();
    new SHOP.GoodsListFilter();

    if (SHOP.adminMode) {
        new SHOP.EditGoodForm();
        new SHOP.GoodGroupForm();
    }

    SHOP.goodGroupsTreeManager.fillGoodGroupsTree();
}