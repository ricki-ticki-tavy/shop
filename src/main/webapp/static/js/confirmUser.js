
var registerResult;

function showMessage(message){
    window.document.getElementById("message").innerHTML = message;
    $( "#dialog" ).dialog( "open" );
}

function doConfirm() {
    $('body').append('<div id="requestOverlay" class="overlay"></div>');

    var url = new URL(window.location.href);
    var confirmCode = url.searchParams.get("confirmCode");

    $.ajax({
        type: "POST",
        url: "security/confirmUser",
        data: "confirmCode=" + confirmCode,
        success: responseConfirmUser.bind(this),
        error: responseConfirmUserFailed.bind(this),
        dataType: "json"
    });
}

function responseConfirmUserFailed(result){
    $("#requestOverlay").hide();
    showMessage(result.responseJSON.error);
}

function responseConfirmUser(result){
    $("#requestOverlay").hide();
    registerResult = result;
    if (result.failed){
        showMessage(result.errorMessage);
    } else {
        showMessage("Пользователь подтвержден. Вы можете выполнить вход.")
    }
}

$( function() {
    $( "#dialog" ).dialog({
        modal: true,
        autoOpen: false,
        width: "550px",
        buttons: {
            Ok: function() {
                $( this ).dialog( "close" );
                if (!registerResult.failed){
                    // успешно
                    window.location.replace("login");
                }
            }
        }
    });
} );

