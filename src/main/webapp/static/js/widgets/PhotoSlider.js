SHOP.PhotoSliderParams = function () {
    this.divContainerId = undefined;
    this.imageIdList = new Array();
    this.smallImageSize = {
        width: 100,
        height: 100
    };
};

SHOP.PhotoSliderParams.prototype = {
    constructor: SHOP.PhotoSliderParams,
};

/**
 * Преобразует div в галлерею изображений.
 * @param divContainerId
 * @param imageIdList
 * @param smallImageSizeWidth
 * @param smallImageSizeHeight
 * @param editable
 * @param baseImageUrlPath
 * @returns {SHOP.PhotoSlider}
 */
SHOP.imageGallery = function (divContainerId, imageIdList, smallImageSizeWidth, smallImageSizeHeight, editable, baseImageUrlPath, useRequiredSize) {
    return new SHOP.PhotoSlider({
        divContainerId: divContainerId,
        imageIdList: imageIdList,
        smallImageSize: {
            width: smallImageSizeWidth,
            height: smallImageSizeHeight
        },
        editable: editable,
        baseImageUrlPath: baseImageUrlPath,
        useRequiredSize: useRequiredSize == undefined ? true : useRequiredSize,
    })
};

SHOP.PhotoSlider = function (photoSliderParams) {
    this.sliderParams = photoSliderParams;
    this.activeIcon = undefined;
    this.scrollDirection = 0;
    this.imagePreloader = new Image();

    this.divContainer = undefined;
    this.largeImage = undefined;
    this.scrollableSmallImagesContainerDiv = undefined;
    this.smallImagesContainerDiv = undefined;
    this.icons = new Array();


    if (!this.sliderParams.divContainerId) {
        // не задан контэйнер
        console.error("Не задан ID контейнера для слайдера фотографий");
    } else {
        // ищем контейнер
        this.sliderParams.elements = {};
        this.divContainer = document.getElementById(this.sliderParams.divContainerId);
        if (!this.divContainer) {
            // не найден контэйнер
            console.error("Контейнера для слайдера фотографий с ID \"" + this.sliderParams.divContainerId + "\" не найден");
        } else {
            this.namePrefix = this.sliderParams.divContainerId + "_icon_";
            // найден. Надо его наполнить элементами
            this.divContainer.classList.add("photoSlider-main-div-container");

            // если разрешено редактирование, то кнопка удаления
            if (this.sliderParams.editable) {
                const buttonDeleteDiv = document.createElement("div");
                buttonDeleteDiv.onclick = this.__onDeleteImageClickHandler.bind(this);
                buttonDeleteDiv.classList.add("photoSlider-button-delete");
                buttonDeleteDiv.classList.add("form-animate-color");
                buttonDeleteDiv.style.color = "red";
                this.divContainer.appendChild(buttonDeleteDiv);
            }

            // сначала создадим большой див для основной фото
            const largePhotoDiv = document.createElement("div");
            largePhotoDiv.setAttribute("class", "photoSlider-large-image-container");
            this.divContainer.appendChild(largePhotoDiv);

            this.largeImage = largePhotoDiv;

            largePhotoDiv.onclick = this.getImageIdList.bind(this);

            // контейнер для прокрутки внизу
            const bottomDivEl = document.createElement("div");
            bottomDivEl.setAttribute("class", "photoSlider-bottom-container");
            bottomDivEl.style.height = (this.sliderParams.smallImageSize.height + 6) + "px";
            bottomDivEl.style.width = "100%";
            this.divContainer.appendChild(bottomDivEl);
            largePhotoDiv.style.height = (this.divContainer.clientHeight - bottomDivEl.clientHeight) + "px";

            // в контейнер для прокрутки поместим две кнопки и субконтейнер для самих слайдов
            const backButtonDiv = document.createElement("div");
            backButtonDiv.setAttribute("class", "photoSlider-button-toBackward");
            bottomDivEl.appendChild(backButtonDiv);
            backButtonDiv.onmousedown = this.__startScroll.bind(this, -10);
            backButtonDiv.onmouseup = this.__stopScroll.bind(this);

            const scrollableSmallImagesContainerDiv = document.createElement("div");
            scrollableSmallImagesContainerDiv.setAttribute("class", "photoSlider-scrollable-small-images-container");
            scrollableSmallImagesContainerDiv.style.height = bottomDivEl.style.height;
            bottomDivEl.appendChild(scrollableSmallImagesContainerDiv);

            // Прокрутка к концу
            const forwardButtonDiv = document.createElement("div");
            forwardButtonDiv.setAttribute("class", "photoSlider-button-toForward");
            forwardButtonDiv.onmousedown = this.__startScroll.bind(this, 5);
            forwardButtonDiv.onmouseup = this.__stopScroll.bind(this);
            bottomDivEl.appendChild(forwardButtonDiv);
            scrollableSmallImagesContainerDiv.style.width = (this.divContainer.clientWidth - forwardButtonDiv.clientWidth - backButtonDiv.clientWidth ) + "px";
            this.scrollableSmallImagesContainerDiv = scrollableSmallImagesContainerDiv;

            // контейнер для размещения миниатюр. Он будет прокручиваться внутри scrollable контэйнера
            const smallImagesContainerDiv = document.createElement("div");
            this.smallImagesContainerDiv = smallImagesContainerDiv;
            smallImagesContainerDiv.setAttribute("class", "photoSlider-small-images-container");
            smallImagesContainerDiv.setAttribute("id", this.namePrefix + "iconsContainer");
            scrollableSmallImagesContainerDiv.appendChild(smallImagesContainerDiv);

            // наполним нижнюю прокрутку иконками
            this.sliderParams.imageIdList.forEach(function (photoId) {
                this.__appendIconToCarousel(photoId);
            }.bind(this));

            if (this.icons.length > 0) {
                this.__onIconClickEventHandler(this.sliderParams.imageIdList[0], {target: this.icons[0]});
            }

        }
    }
};

SHOP.PhotoSlider.prototype = {
    constructor: SHOP.PhotoSlider,

    /**
     * Реакция на клик по иконке в полосе прокрутки изображений
     * @param event
     * @param photoId
     * @private
     */
    __onIconClickEventHandler: function (photoId, event) {
        const iconDiv = event.target;
        if (this.activeIcon) {
            this.activeIcon.setAttribute("class", "photoSlider-small-image-div");
        }
        iconDiv.setAttribute("class", "photoSlider-small-image-div photoSlider-small-image-div-active");
        this.activeIcon = iconDiv;

        // предзагрузим изображение, чтобы не было белых вспышек на картинке при переключении с одного слайда на другой
        this.imagePreloader.onload = function () {
            this.largeImage.style.backgroundImage = "url(" + this.imagePreloader.src + ")";
        }.bind(this);

        this.imagePreloader.src = this.sliderParams.baseImageUrlPath + "/"
            + (this.sliderParams.useRequiredSize
                ? (this.largeImage.clientWidth + "x" + this.largeImage.clientHeight + "/")
                : "")
            + photoId + ".png";
    },
    //---------------------------------------------------------------------------------------------

    /**
     * Добавить изображение к слайдеру
     * @param imageId
     */
    addImage: function (photoId) {
        const newIconDivId = this.namePrefix + photoId;
        // проверим нет ли уже этого изображения
        const founds = this.icons.filter(function (icon) {
            return icon.id == newIconDivId
        });
        if (founds.length > 0) {
            $.alert("Такое изображение уже есть");
            this.__onIconClickEventHandler(photoId, {target: founds[0]});
        } else {
            this.__onIconClickEventHandler(photoId, {target: this.__appendIconToCarousel(photoId)});
        }
    },
    //---------------------------------------------------------------------------------------------

    /**
     * Запускает процесс прокрутки
     * @param direction
     * @private
     */
    __startScroll: function (direction) {
        this.scrollDirection = direction;
        if (direction != 0) {
            window.setTimeout(this.__doScroll.bind(this), 10);
        }
    },
    //---------------------------------------------------------------------------------------------

    /**
     *  Прерывает процесс прокрутки
     * @private
     */
    __stopScroll: function () {
        this.scrollDirection = 0;
    },
    //---------------------------------------------------------------------------------------------

    /**
     * Выполняет один шаг в цикле прокрутки карусели миниатюр. Если прокрутка успешна, то назначает срабатывание
     * себя же через некоторый интервал для следующего шага. Если прокручивать уже некуда, то прокрутка отключается
     * @private
     */
    __doScroll: function () {
        const scrollDiv = this.scrollableSmallImagesContainerDiv;
        const innerDiv = this.smallImagesContainerDiv;
        if (((this.scrollDirection > 0 && (innerDiv.clientWidth - scrollDiv.scrollLeft > scrollDiv.clientWidth))
                || (this.scrollDirection < 0 && scrollDiv.scrollLeft > 0))) {
            // есть куда скролить
            scrollDiv.scrollLeft += this.scrollDirection;
            window.setTimeout(this.__doScroll.bind(this), 10);
        } else {
            this.scrollDirection = 0;
        }
    },
    //---------------------------------------------------------------------------------------------

    __appendIconToCarousel: function (photoId) {
        const imageDiv = document.createElement("div");
        imageDiv.setAttribute("class", "photoSlider-small-image-div");
        imageDiv.setAttribute("id", this.namePrefix + photoId);
        imageDiv.style.width = this.sliderParams.smallImageSize.width + "px";
        imageDiv.style.height = this.sliderParams.smallImageSize.height + "px";
        imageDiv.style.backgroundImage = "url(" + this.sliderParams.baseImageUrlPath + "/"
            + (this.sliderParams.useRequiredSize
                ? (this.sliderParams.smallImageSize.width + "x" + this.sliderParams.smallImageSize.height + "/")
                : "")
            + photoId + ".png)";

        this.smallImagesContainerDiv.appendChild(imageDiv);

        if (this.sliderParams.editable) {
            $('#' + this.namePrefix + photoId).draggable({
                containment: "parent",
                cursor: "move",
                revert: true,
                revertDuration: 0,
                distance: 5,
                scroll: true,
                helper: function (event) {

                    return "<div class='photoSlider-small-image-div' style='width: " + event.target.style.width
                        + "; height: " + event.target.style.height + "; background-image: " + event.target.style.backgroundImage + "; opacity: 0.5'>" +
                        "</div>";
                }.bind(this),
            });

            $("#" + this.namePrefix + photoId).droppable({
                accept: ".photoSlider-small-image-div",
                tolerance: "intersect",
                over: this.__onIconOver.bind(this),
                out: this.__onIconOut.bind(this),
                drop: this.__onIconDropped.bind(this),
                over: this.__onIconOver.bind(this),
            });
        }

        imageDiv.onclick = this.__onIconClickEventHandler.bind(this, photoId);
        this.icons.push(imageDiv);
        return imageDiv;
    },
    //---------------------------------------------------------------------------------------------

    /**
     * Возвращает список изображений в той последовательности, как они идут в галерее
     */
    getImageIdList: function () {
        const indexes = new Array();
        this.smallImagesContainerDiv.childNodes.forEach(function (child) {
            indexes.push(child.id.substr(this.namePrefix.length));
        }.bind(this));
        return indexes;
    },
    //---------------------------------------------------------------------------------------------

    /**
     * Возвращает список изображений в той последовательности, как они идут в галерее
     */
    getImageIdsAsString: function () {
        var indexes = "";
        this.smallImagesContainerDiv.childNodes.forEach(function (child) {
            indexes += (indexes != "" ? "," : "" ) + child.id.substr(this.namePrefix.length);
        }.bind(this));
        return indexes;
    },
    //---------------------------------------------------------------------------------------------

    __onIconDropped: function (event, ui) {
        const scrLft = event.target.parentNode.parentNode.scrollLeft;
        event.target.classList.remove("photoSlider-small-image-div-drag-over");
        ui.draggable[0].parentNode.removeChild(ui.draggable[0]);
        event.target.parentNode.insertBefore(ui.draggable[0], event.target);
        event.target.parentNode.parentNode.scrollLeft = scrLft;
    },
    //---------------------------------------------------------------------------------------------

    __onIconOver: function (event, ui) {
        event.target.classList.add("photoSlider-small-image-div-drag-over");
    },
    //---------------------------------------------------------------------------------------------

    __onIconOut: function (event, ui) {
        event.target.classList.remove("photoSlider-small-image-div-drag-over");
    },
    //---------------------------------------------------------------------------------------------

    /**
     * Реакция на кнопку удаления изображения
     * @private
     */
    __onDeleteImageClickHandler: function () {
        if (this.activeIcon) {
            $.confirm({
                title: "Отвязка изображения",
                content: "Отвязать изображение ?",
                buttons: {
                    Отвязать: function () {
                        this.icons = this.icons.filter(function (icon) {
                            return icon != this.activeIcon;
                        }.bind(this));
                        this.activeIcon.parentNode.removeChild(this.activeIcon);
                        this.activeIcon = undefined;
                        if (this.smallImagesContainerDiv.childNodes.length != 0) {
                            // если есть картинки, то переключим на первую
                            this.__onIconClickEventHandler(this.smallImagesContainerDiv.childNodes[0].id.substr(this.namePrefix.length)
                                , {target: this.smallImagesContainerDiv.childNodes[0]});

                        } else {
                            // вообще очистим картинку
                            this.largeImage.style.backgroundImage = "";
                        }
                    }.bind(this),
                    Отмена: function () {
                    }
                }
            });
        }
    }
    //---------------------------------------------------------------------------------------------
};



