SHOP.ShopingCart = function(){
    SHOP.shoppingCart = this;
    this.container = document.getElementById("cartContainer");

    this.cartImage = undefined;

    this.__buildShoppingCart();
};

SHOP.ShopingCart.prototype = {
    constructor: SHOP.ShopingCart,

    /**
     * Формирует корзину
     * @private
     */
    __buildShoppingCart: function(){
        this.container.classList.add("shopping-cart-container");

        // изображение корзины
        this.cartImage = document.createElement("div");
        this.cartImage.classList.add("shopping-cart-image-main");
        this.container.appendChild(this.cartImage);
    }
};