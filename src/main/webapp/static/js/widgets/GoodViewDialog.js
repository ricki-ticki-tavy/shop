SHOP.GoodViewDialog = function(good){
    this.good = good;
    this.screenLocker = new SHOP.HtmlLockScreenCursor(this.__closeGoodViewDialog.bind(this));
    return this.__buildDialog();
};

SHOP.GoodViewDialog.prototype = {
    constructor: SHOP.GoodViewDialog,

    __buildDialog: function () {
        SHOP.apiRestClient.loadGoodViewContainment(function(response){
                this.screenLocker.show();
                this.dialogContainer = document.createElement("div");
                this.dialogContainer.classList.add("good-view-dialog-container");
                this.dialogContainer.innerHTML = response;
                document.body.appendChild(this.dialogContainer);
                document.getElementById('goodViewFormContainer').onload();
                const closeButtonContainer = document.createElement("div");
                closeButtonContainer.classList.add("good-view-form-button-close-container");
                const closeButton = document.createElement("div");
                closeButton.classList.add("good-view-form-button-close");
                closeButtonContainer.appendChild(closeButton);
                closeButton.onclick = this.__closeGoodViewDialog.bind(this);
                this.dialogContainer.appendChild(closeButtonContainer);

        }.bind(this)
        , this.good);
    },
    //------------------------------------------------------------------------------------------------------

    __closeGoodViewDialog: function(){
        this.screenLocker.hide();
        document.body.removeChild(this.dialogContainer);
    },
    //------------------------------------------------------------------------------------------------------


};