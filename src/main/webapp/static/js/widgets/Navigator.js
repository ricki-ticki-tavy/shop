SHOP.Navigator = function () {
    SHOP.navigator = this;
    this.howToBuyFormName = "howToBuy";
    this.aboutFormName = "about";
    this.profileFormName = "profile";
    this.aboutPayAndDeliveryFormName = "aboutPayAndDelivery";
    this.faqFormName = "faq";
    this.ordersFormName = "orders";
    this.goodsListFormName = "goodsList";

    this.goodsFormName = "about";

    this.usersFormName = "users";

    this.goodGroupFormFormName = "goodGroupForm";
    this.goodFormFormName = "goodForm";
    this.goodViewFormFormName = "goodViewForm";
};

SHOP.Navigator.prototype = {
    constructor: SHOP.Navigator(),

    openHowToBuy: function () {
        SHOP.apiRestClient.loadForm(null, this.howToBuyFormName, "");
    },

    openAbout: function () {
        SHOP.apiRestClient.loadForm(function () {
        }.bind(this), this.aboutFormName, "");
    },

    openProfile: function () {
        SHOP.apiRestClient.loadForm(null, this.profileFormName, "");
    },

    openUsers: function () {
        SHOP.apiRestClient.loadForm(null, this.usersFormName, "");
    },

    openAboutPayAndDelivery: function () {
        SHOP.apiRestClient.loadForm(null, this.aboutPayAndDeliveryFormName, "");
    },

    openFaq: function () {
        SHOP.apiRestClient.loadForm(null, this.faqFormName, "");
    },

    openGoodsList: function () {
        SHOP.apiRestClient.loadForm(
            SHOP.goodGroupsTreeManager.showGoodGroupContainment.bind(SHOP.goodGroupsTreeManager)
            , this.goodsListFormName
            , SHOP.goodsListFilter.groupsToString());
    },

    openOrders: function (formData) {
        SHOP.apiRestClient.loadForm(null, this.ordersFormName, formData);
    },

    /**
     * Открытие добавления / редактирования группы товара
     * @param currentId - null для создания группы в группе parentId
     * @param parentId - null для группы верхнего уровня
     */
    openGoodGroupForm: function (currentId, parentId) {
        SHOP.apiRestClient.loadForm(function (response) {
                SHOP.goodGroupForm.prepare();
                document.getElementById("groupName").focus();
            }.bind(this)
            , this.goodGroupFormFormName, "currentId=" + (currentId ? currentId : "-1")
            + "&parentId=" + (parentId ? parentId : "-1"));
    },

    /**
     * Открытие добавления / редактирования товара
     * @param goodId - null для создания товара
     * @param parentGroupId - null для группы верхнего уровня
     */
    openGoodForm: function (goodId, parentGroupId) {
        SHOP.apiRestClient.loadForm(function (response) {
                SHOP.editGoodForm.prepareForm(goodId);
                document.getElementById("goodName").focus();
            }.bind(this)
            , this.goodFormFormName, "goodId=" + (goodId ? goodId : "-1")
            + "&parentGroupId=" + (parentGroupId ? parentGroupId : "-1"));
    },

    /**
     * Открытие формы просмотра товара покупателем
     * @param goodId - null для создания товара
     * @param parentGroupId - null для группы верхнего уровня
     */
    openGoodViewForm: function (goodId, parentGroupId) {
        SHOP.apiRestClient.loadForm(function (response) {
                document.getElementById('goodViewFormContainer').onload();
            }.bind(this)
            , this.goodViewFormFormName, "nav=true&goodId=" + (goodId ? goodId : "-1")
            + "&parentGroupId=" + (parentGroupId ? parentGroupId : "-1"));
    },

};

