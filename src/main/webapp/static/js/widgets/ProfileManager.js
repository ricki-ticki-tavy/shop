/**
 * Любые действия с данными своего профиля
 * @constructor
 */

SHOP.ProfileManager = function () {
    SHOP.profileManager = this;
};

SHOP.ProfileManager.prototype = {
    constructor: SHOP.ProfileManager,

    saveProfile: function (profileForm) {
        const formData = new FormData(profileForm);
        SHOP.apiRestClient.updateProfile(function (response) {
            if (!response.failed) {
                document.getElementById("newPassword").value = "";
                document.getElementById("repeatNewPassword").value = "";
                $.alert("Изменения в профиле сохранены");
            }
        }.bind(this), formData);
    }
};