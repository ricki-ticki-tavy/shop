SHOP.GoodWidget = function (good) {
    this.good = good;
    this.namePrefix = "goodWidget_";
    this.urlPrefix = "api/buyer/images";

    this.container = undefined;
    this.mainImage = undefined;
    this.title = undefined;
    this.toolButton = undefined;
    this.priceLabel = undefined;
    this.quantityField = undefined;
    this.buttonAddToCart = undefined;

    this.__buildWidget();

    return this.container;
};

SHOP.GoodWidget.prototype = {
    constructor: SHOP.GoodWidget,

    __buildWidget: function () {
        // контейнер
        this.container = document.createElement("div");
        this.container.classList.add("good-widget-container");
        this.container.classList.add("form-animate-box-shadow");
        this.container.id = this.namePrefix + this.good.id;

        // кнопка опций
        if (SHOP.adminMode) {
            this.__createAdminToolsMenu();
        }

        // картинка товара
        this.mainImage = document.createElement("div");
        this.mainImage.classList.add("good-widget-main-image");
        this.mainImage.classList.add("form-animate-opacity");
        this.mainImage.style.backgroundImage = "url(" + this.urlPrefix + "/280x280/" + this.good.imageIdList[0] + ".png)";
        this.container.appendChild(this.mainImage);
        this.mainImage.onclick = this.__showGoodViewDialog.bind(this);

        // SHOP.navigator.openGoodViewForm(this.good.id, this.good.goodGroup);

        // название товара
        this.title = document.createElement("div");
        this.title.classList.add("good-widget-title");
        this.title.innerHTML = this.good.name;
        this.container.appendChild(this.title);

        // блок с ценой и помещением товара в корзину
        const priceAndBuyContainer = document.createElement("div");
        priceAndBuyContainer.classList.add("good-widget-container-price-and-buy");
        this.container.appendChild(priceAndBuyContainer);

        // Цена
        this.priceLabel = document.createElement("div");
        this.priceLabel.classList.add("good-widget-label-price");
        this.priceLabel.innerHTML = this.good.price + " р.";
        priceAndBuyContainer.appendChild(this.priceLabel);

        // кол-во для заказа
        this.quantityField = document.createElement("input");
        this.quantityField.type = "number";
        this.quantityField.min = "1";
        this.quantityField.max = this.good.quantity;
        this.quantityField.onkeydown = function (evt){evt.preventDefault();};
        this.quantityField.placeholder = this.good.quantity > this.good.freeQuantity ? "> " + this.good.freeQuantity : "ост. " + this.good.quantity;
        this.quantityField.title = this.quantityField.placeholder;
        this.quantityField.classList.add("good-widget-field-quantity");
        priceAndBuyContainer.appendChild(this.quantityField);

        // кнопка ПОЛОЖИТЬ В КОРЗИНУ
        this.buttonAddToCart = document.createElement("div");
        this.buttonAddToCart.classList.add("good-widget-button-add-to-cart");
        priceAndBuyContainer.appendChild(this.buttonAddToCart);

    },
    //------------------------------------------------------------------------------------------------------

    __createAdminToolsMenu: function(){
        const toolButtonContainer = document.createElement("div");
        toolButtonContainer.classList.add("good-widget-button-tool-container");
        this.container.appendChild(toolButtonContainer);
        this.toolButton = document.createElement("div");
        this.toolButton.classList.add("good-widget-button-tool");
        this.toolButton.classList.add("form-animate-opacity");
        toolButtonContainer.appendChild(this.toolButton);

        $(this.toolButton).materialMenu('init', {
            position: 'overlay',
            items: [
                {
                    type: 'normal',
                    text: 'Редактировать',
                    click: this.__onEditGood.bind(this)
                },
                {
                    type: 'divider'
                },
                {
                    type: 'normal',
                    text: 'Удалить',
                    click: this.__onDeleteGood.bind(this)
                },
            ]
        }).click(function () {
            $(this).materialMenu('open');
        });;

    },
    //------------------------------------------------------------------------------------------------------

    __onEditGood: function(){
        SHOP.navigator.openGoodForm(this.good.id, this.good.goodGroup);
    },
    //------------------------------------------------------------------------------------------------------

    __onDeleteGood: function(){
        $.confirm({
            title: "Удаление товара",
            content: "Удалить товар " + this.good.name + " ?",
            buttons: {
                Удалить: function () {
                }.bind(this),
                Отмена: function () {
                }
            }
        });
    },
    //------------------------------------------------------------------------------------------------------

    __showGoodViewDialog: function () {
        this.viewDialog = new SHOP.GoodViewDialog(this.good);

    },
    //------------------------------------------------------------------------------------------------------

};

SHOP.goodWidget = function (container, good) {
    const goodWidget = new SHOP.GoodWidget(good);
    container.appendChild(goodWidget);
    return goodWidget;
};