var registerResult;
var confirmCode;

function showMessage(message) {
    window.document.getElementById("message").innerHTML = message;
    $("#dialog").dialog("open");
}

function createOverlay() {
    $('body').append('<div id="requestOverlay" class="overlay"></div>');
    $("#requestOverlay").hide();
}

function initConfirmForm(){
    createOverlay();

    var url = new URL(window.location.href);
    confirmCode = url.searchParams.get("confirmCode");
}

function doRequestSetPassword() {
    $("#requestOverlay").show();
    var params = "confirmCode=" + confirmCode
        + "&password=" + window.document.getElementById("password").value
        + "&retypePassword=" + window.document.getElementById("retypePassword").value;

    $.ajax({
        type: "POST",
        url: "security/confirmRestoreAccountAccess",
        data: params,
        success: responseSuccess.bind(this),
        error: responseFailed.bind(this),
        dataType: "json"
    });
}

function responseFailed(result) {
    $("#requestOverlay").hide();
    showMessage(result.responseJSON.error);
}

function responseSuccess(result) {
    $("#requestOverlay").hide();
    registerResult = result;
    if (result.failed) {
        showMessage(result.errorMessage);
    } else {
        showMessage("Новый парольуспешно задан. Вы можете выполнить вход в учетную запись.")
    }
}

$(function () {
    $("#dialog").dialog({
        modal: true,
        autoOpen: false,
        width: "550px",
        buttons: {
            Ok: function () {
                $(this).dialog("close");
                if (!registerResult.failed) {
                    // успешно
                    window.location.replace("login");
                }
            }
        }
    });
});

