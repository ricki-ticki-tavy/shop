/**
 * Хранит любые настройки, индивидуальные для пользователя
 * @constructor
 */
GAME.UserSettingsKeeper = function () {

    GAME.userSettingsKeeper = this;

    // настройки
    this.chat = {
        // время жизни сообщений на экране чата арены. секунд
        arenaMessageLiveTimeSec: 10,
    };

    // настройки рендера
    this.render = {
        anisotropy: 16,
        antialias: true,
    };

    this.arena = {
        scale: 0.3,
        wheelCoef: 0.05,
        activeBorders: false,
    };
};

GAME.UserSettingsKeeper.prototype = {
    constructor: GAME.UserSettingsKeeper,

    load: function () {

    },
};