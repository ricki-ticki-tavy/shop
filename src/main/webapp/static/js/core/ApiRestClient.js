/**
 * Класс, выполняющий запросы к движку
 * @param externalShowErrorMethod
 * @param lockCursor  - форма-блокиратор UI, для показа во время запросов к серверу
 * @param gameApiUrl
 * @constructor
 */
SHOP.ApiRestClient = function () {
    SHOP.apiRestClient = this;
    this.shopApiUrl = "api/";

    this.adminApi = this.shopApiUrl + "admin/";
    this.managerApi = this.shopApiUrl + "manager/";
    this.buyerApi = this.shopApiUrl + "buyer/";

    this.lockCursor = new SHOP.HtmlLockScreenCursor();
};

SHOP.ApiRestClient.prototype = {
    constructor: SHOP.ApiRestClient,

    /**
     * Загрузить форму в окно для форм
     * @param formName
     */
    loadForm: function(callback, formName, formData){
        const formContainer = document.getElementById("formContainer");
        // formContainer.innerHTML = "";


        $.ajax({
            url: formName,
            data: formData,
            success: function (response) {
                formContainer.innerHTML = response;
                if (callback) {
                    callback(formName);
                }
            }.bind(this),
            error: this.__showError.bind(this, null),
            dataType: "html"
        })
    },
    //=============================================================================================

    loadGoodViewContainment: function(callback, good ){
        $.ajax({
            url: "goodViewForm",
            data: "nav=true&goodId=" + (good.id ? good.id : "-1") + "&parentGroupId=" + (good ? good.goodGroup : "-1"),
            success: function (response) {
                if (callback) {
                    callback(response);
                }
            }.bind(this),
            error: this.__showError.bind(this, null),
            dataType: "html"
        })

    },
    //=============================================================================================

    /**
     * Загрузить на сервер изображение
     * @param callback
     * @param formData
     */
    uploadImage: function(callback, formData){
        this.lockCursor.show();
        $.ajax({
            url: this.adminApi + "uploadImage",
            data: formData,
            processData: false,
            contentType: false,
            type: 'POST',
            success: this.__successResponse.bind(this, callback),
            error: this.__showError.bind(this, null),
            dataType: "json"
        });
    },
    //=============================================================================================

    /**
     * Получить список атрибутов товаров для группы
     * @param callback
     * @param groupId
     */
    getTopLevelGroupAttributes: function(callback, groupId){
        this.lockCursor.show();
        $.ajax({
            url: this.adminApi + "getGroupAttributes",
            data: "groupId=" + groupId,
            processData: false,
            contentType: false,
            type: 'GET',
            success: this.__successResponse.bind(this, callback),
            error: this.__showError.bind(this, null),
            dataType: "json"
        });
    },
    //=============================================================================================

    /**
     * Получить дерево групп товаров
     * @param callback
     */
    getCatalogTree: function (callback) {
        this.lockCursor.show();
        $.ajax({
            url: this.buyerApi + "getGoodGroupsTree",
            data: "",
            success: function (response) {
                this.lockCursor.hide();
                callback(response)
            }.bind(this),
            error: this.__showError.bind(this, null),
            dataType: "json"
        });
    },
    //=============================================================================================

    /**
     * Получить список групп для подгруппы
     * @param callback
     * @param parentGroupId - undefined или -1 для корневых групп
     */
    getGroupList: function(callback, parentGroupId){
        this.lockCursor.show();
        $.ajax({
            url: this.buyerApi + "getGoodGroupsList",
            data: "parentGroupId=" + (parentGroupId == undefined ? "-1" : parentGroupId),
            success: function (response) {
                this.lockCursor.hide();
                callback(response)
            }.bind(this),
            error: this.__showError.bind(this, null),
            dataType: "json"
        });
    },
    //=============================================================================================

    /**
     * Сохранить или отредактировать группу товара
     * @param callback
     * @param formData
     */
    saveOrUpdateGoodGroup: function(callback, formData){
        this.lockCursor.show();
        $.ajax({
            url: this.adminApi + "saveOrUpdateGoodGroup",
            data: formData,
            processData: false,
            contentType: false,
            type: 'POST',
            success: this.__successResponse.bind(this, callback),
            error: this.__showError.bind(this, null),
            dataType: "json"
        });
    },
    //=============================================================================================

    /**
     * отредактировать свой профиль пользователя
     * @param callback
     * @param formData
     */
    updateProfile: function(callback, formData){
        this.lockCursor.show();
        $.ajax({
            url: this.buyerApi + "updateProfile",
            data: formData,
            processData: false,
            contentType: false,
            type: 'POST',
            success: this.__successResponse.bind(this, callback),
            error: this.__showError.bind(this, null),
            dataType: "json"
        });
    },
    //=============================================================================================

    /**
     * Удалить группу товаров
     * @param callback
     * @param groupId
     */
    removeGoodGroup: function(callback, groupId){
        this.lockCursor.show();
        $.ajax({
            url: this.adminApi + "removeGoodGroup",
            data: "currentId=" + groupId,
            processData: false,
            contentType: false,
            success: this.__successResponse.bind(this, callback),
            error: this.__showError.bind(this, null),
            dataType: "json"
        });
    },
    //=============================================================================================

    /**
     * Подвинуть группу в списке вверх
     * @param callback
     * @param groupId
     */
    moveUpOrDownGoodGroup: function(callback, groupId, direction){
        this.lockCursor.show();
        $.ajax({
            url: this.adminApi + "moveUpOrDownGoodGroup",
            data: "currentId=" + groupId + "&direction=" + direction,
            processData: false,
            contentType: false,
            success: this.__successResponse.bind(this, callback),
            error: this.__showError.bind(this, null),
            dataType: "json"
        });
    },
    //=============================================================================================

    /**
     * Подвинуть группу в списке вверх
     * @param callback
     * @param groupId
     */
    saveOrUpdateGood: function(callback, formData){
        this.lockCursor.show();
        $.ajax({
            url: this.adminApi + "saveOrUpdateGood",
            data: formData,
            processData: false,
            type: "POST",
            contentType: false,
            success: this.__successResponse.bind(this, callback),
            error: this.__showError.bind(this, null),
            dataType: "json"
        });
    },
    //=============================================================================================

    getGoodsList: function(callback, goodsFilter){
        this.lockCursor.show();
        $.ajax({
            url: this.buyerApi + "getGoodsList",
            data: "goodsFilter=" + goodsFilter.toString(),
            processData: false,
            type: "GET",
            contentType: false,
            success: this.__successResponse.bind(this, callback),
            error: this.__showError.bind(this, null),
            dataType: "json"
        });
    },
    //=============================================================================================

    __successResponse: function(callback, response){
        if (response.failed) {
            this.__externalShowErrorMethod(response.errorMessage, callback.bind(this, response));
            this.lockCursor.hide();
        } else {
            this.lockCursor.hide();
            callback(response);
        }
    },
    //=============================================================================================


    __externalShowErrorMethod: function (message, callback) {
        $.alert(message);
        if (callback) {
            callback(message);
        }
    },
    //============================================================================================


    __showError: function (callback, result) {
        this.lockCursor.hide();
        if (result.statusText != "error" && result.responseText && result.responseText.startsWith("<!DOCTYPE html>")) {
            // это страница логина. Бесполезно париться: переходим на странницу логина
            redirect = function () {
                window.document.location.reload();
            };
            this.__externalShowErrorMethod("Сессия потеряна. Необходима повторная авторизация", redirect);
        } else if (result.statusText == "error") {
            this.__externalShowErrorMethod(result.responseText, callback);
        } else if (result.responseJSON.message) {
            this.__externalShowErrorMethod(result.responseJSON.message, callback);
        } else {
            this.__externalShowErrorMethod(result.responseJSON.error, callback);
        }

    },
    //============================================================================================


};
