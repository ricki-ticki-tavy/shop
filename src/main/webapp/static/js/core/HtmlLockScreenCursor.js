/**
 * Класс для затемнения и блокировки экрана во время запросов
 * @constructor
 */

SHOP.HtmlLockScreenCursor = function(onclickHandler) {
    this.visible = false;
    this.onclickHandler = onclickHandler;
    this.STYLE = "background: black;\n" +
        "    z-index: 3;\n" +
        "    position: fixed;\n" +
        "    left: 0;\n" +
        "    top: 0;\n" +
        "    width: 100%;\n" +
        "    height: 100%;\n" +
        "    display: block;\n" +
        "    text-align: center;\n" +
        "    opacity: .4;\n" +
        "    filter: alpha(opacity=40);"

    this.lockDiv = document.createElement("div");
    this.lockDiv.setAttribute("id", "requestOverlay");
    this.lockDiv.setAttribute("style", this.STYLE);
    if (onclickHandler){
        this.lockDiv.onclick = onclickHandler;
    }
};

SHOP.HtmlLockScreenCursor.prototype = {
    constructor: SHOP.HtmlLockScreenCursor,


    show: function(){
        if (!this.visible) {
            document.body.appendChild(this.lockDiv);
            this.visible = true;
        }
    },
    //============================================================================

    hide: function(){
        if (this.visible) {
            document.body.removeChild(this.lockDiv);
            this.visible = false;
        }
    },
    //============================================================================

};
