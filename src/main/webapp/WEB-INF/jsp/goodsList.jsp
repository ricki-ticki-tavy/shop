<%@ page import="org.springframework.security.core.context.SecurityContextHolder" %>
<%@ page import="org.springframework.security.core.authority.SimpleGrantedAuthority" %>
<%@ page import="static core.database.entity.security.Role.SPRING_ROLE_ADMIN_NAME" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<link rel="stylesheet" type="text/css" href="css/page/pageGoodsList.css"/>
<script type="application/json" id="subGroupsListScript">${subGroupsListScript}</script>
<table class="pageGoodsListTable">
    <tr id="goodsSubGroupListRow" style="height: 0px">
        <td>
            <div id="groupRefContainer" class="goodsListGroupRefContainer"></div>
        </td>
    </tr>

    <tr id="goodsSubGroupListSeparatorRow" style="height: 20px">
        <td></td>
    </tr>

    <tr style="height: 0px">
        <td>
            <div class="goodsFilterPanel">
                <div id="filtersContainer" class="filtersContainer">
                    <div class="formLineSeparator"></div>
                    Фильтры товаров. В разработке
                </div>
                <div id="showGoodsFilterButton" class="sliderIcons goodsFilterSliderShow"
                     onclick="SHOP.goodsListManager.showFilterPanel();" style="display: none">
                    <div class="lineOnFilterSlider"></div>
                    <div class="textOnFilterSlider">фильтры</div>
                </div>
                <div id="hideGoodsFilterButton" class="sliderIcons goodsFilterSliderHide"
                     onclick="SHOP.goodsListManager.hideFilterPanel();">
                    <div class="lineOnFilterSlider"></div>
                    <div class="textOnFilterSlider">Скрыть</div>
                </div>
            </div>
        </td>
    </tr>

    <tr class="goodsListContainer">
        <td class="goodsListContainer">
            <div>
                <div class="goodsListToolsContainer">
                    <% if (SecurityContextHolder.getContext().getAuthentication().getAuthorities().contains(new SimpleGrantedAuthority(SPRING_ROLE_ADMIN_NAME))) {
                    %>
                    <div class="buttonIcon div-button buttonIconPlus"
                         onclick="SHOP.navigator.openGoodForm(undefined, ${groupId}); return false;">

                    </div>

                    <%
                        }
                    %>
                </div>
                <div id="goodsListContainer" class="goodsListContainer">

                </div>
            </div>
        </td>
    </tr>

    <tr>
        <td>

        </td>
    </tr>
</table>
