<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="css/login.css" />
    <link rel="stylesheet" href="css/jquery-ui.css">
    <script src="js/jquery-3.3.1.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/registerUser.js"></script>
</head>
<body onload="createOverlay()">

<div id="dialog" title="Регистрация пользователя">
    <p id="message">.</p>
</div>

<div class="mainContainer">
    <form method="POST" action="${contextPath}/security/registerUser">
        <div class="anyFormContainer registerFormContainer">
            <table class="loginFormTable">
                <tr>
                    <td colspan="2" class="loginFormHeader">
                        Регистрация
                    </td>
                </tr>
                <tr>
                    <td class="loginFormFieldTitle">
                        Логин :
                    </td>
                    <td>
                        <input id="username" name="username" type="text" class="form-control" placeholder="пользователь"
                               autofocus="true"/>
                    </td>
                </tr>
                <tr>
                    <td class="loginFormFieldTitle">
                        Пароль :
                    </td>
                    <td>
                        <input id="password" name="password" type="password" class="form-control" placeholder="пароль"/>
                    </td>
                </tr>
                <tr>
                    <td class="loginFormFieldTitle">
                        Повтор пароля :
                    </td>
                    <td>
                        <input id="retryPassword" name="retryPassword" type="password" class="form-control" placeholder="повтор пароля"/>
                    </td>
                </tr>
                <tr>
                    <td class="loginFormFieldTitle">
                        электронная почта :
                    </td>
                    <td>
                        <input id="e-mail" name="e-mail" type="text" class="form-control" placeholder="электронная почта"
                               autofocus="true"/>
                    </td>
                </tr>
                <tr>
                    <td class="loginFormFieldTitle">
                        <img src="security/captcha"/>
                    </td>
                    <td>
                        <input id="captcha" name="captcha" type="text" class="form-control" placeholder="captcha"
                               autofocus="true"/>
                    </td>
                </tr>
                <tr class="loginFormFieldButtonContainer">
                    <td colspan="2">
                        <button class="button registerButton" onclick="doRegister();" style="margin-left: 190px" type="button">Регистрация</button>
                        <button class="button cancelButton" type="button" onclick="window.location='${contextPath}/login'">Отмена</button>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</div>
</body>

</html>


