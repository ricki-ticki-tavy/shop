<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <link rel="stylesheet" type="text/css" href="css/login.css"/>
    <link rel="stylesheet" href="css/jquery-ui.css">
    <script src="js/jquery-lib/jquery-3.3.1.js"></script>
    <script src="js/jquery-lib/jquery-ui.js"></script>
    <script src="js/confirmRestoreAccountAccess.js"></script>
    <style type="text/css">
        input {
            width: 270px;
        }
    </style>
</head>
<body onload="initConfirmForm()">

<div id="dialog" title="Подтверждение восстановления доступа">
    <p id="message">.</p>
</div>

<div class="mainContainer">
    <form method="POST" action="${contextPath}/restorePassword">
        <div class="anyFormContainer restorePasswordFormContainer">
            <table class="loginFormTable">
                <tr>
                    <td colspan="2" class="loginFormHeader">
                        Создание нового пароля
                    </td>
                </tr>
                <tr>
                    <td class="restorePasswordFormFieldTitle">
                        Новый пароль:
                    </td>
                    <td>
                        <input id="password" name="password" type="password" class="form-control" placeholder="Пароль"
                               autofocus="true"/>
                    </td>
                </tr>
                <tr>
                    <td class="restorePasswordFormFieldTitle">
                        Повторно пароль :
                    </td>
                    <td>
                        <input id="retypePassword" name="retypePassword" type="password" class="form-control"
                               placeholder="Повторно пароль"
                               autofocus="true"/>
                    </td>
                </tr>
                <tr class="loginFormFieldButtonContainer">
                    <td colspan="2">
                        <button class="button resetPasswordButton" style="margin-left: 190px" type="button"
                                onclick="doRequestSetPassword();">Установить
                        </button>
                        <button class="button cancelButton" type="button"
                                onclick="window.location='${contextPath}/login'">Отмена
                        </button>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</div>

</body>


</body>

</html>


