<%@ page import="org.springframework.util.StringUtils" %>
<%@ page import="core.database.dao.good.GoodAttributeDto" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<link rel="stylesheet" type="text/css" href="css/page/pageGoodForm.css"/>
<form action="${priorUrl}" method="get" id="goodForm" class="formBase">
    <input type="hidden" name="goodId" id="goodId" value="${goodId}"/>
    <input type="hidden" name="parentGroupId" id="parentGroupId" value="${parentGroupId}"/>
    <input type="hidden" name="parentSubGroupId" id="parentSubGroupId" value="${parentSubGroupId}"/>
    <input type="hidden" name="imageIds" id="imageIds" value="${imageIds}"/>
    <input type="hidden" name="attributesValue" id="attributesValue" value='${attributesValue}'/>
    <table id="pageGoodForm" class="pageGoodForm">
        <tr class="formHeader">
            <td colspan="2">
                <%
                    if (request.getParameter("goodId").equals("-1")) {
                %>
                Добавление товара
                <%
                } else {
                %>
                Редактирование товара "${goodName}"
                <%
                    }
                %>
            </td>
        </tr>
        <tr class="formCompactItemRow">
            <td class="formParamCaption">
                Название товара
            </td>
            <td>
                <input class="formParamValue" id="goodName" name="goodName" value="${goodName}">
            </td>
        </tr>

        <tr class="formCompactItemRow">
            <td class="formParamCaption">
                группа
            </td>
            <td>
                <select id="goodGroup" name="goodGroup" class="formParamValue"
                        onchange="SHOP.editGoodForm.topLevelGroupChanged();">
                </select>
            </td>
        </tr>

        <tr class="formCompactItemRow">
            <td class="formParamCaption">
                Подгруппа
            </td>
            <td>
                <select id="goodSubGroup" name="goodSubGroup" class="formParamValue">
                </select>
            </td>
        </tr>

        <tr class="formCompactItemRow">
            <td class="formParamCaption">
                Описание
            </td>
            <td>
                <textarea style="height: 200px" id="goodDescription" name="goodDescription"
                          class="formParamValue">${goodDescription}</textarea>
            </td>
        </tr>

        <tr id="goodFormTrAfterAttributes" class="formCompactItemRow">
            <td class="formParamCaption">
                Цена
            </td>
            <td>
                <input type="number" id="goodPrice" name="goodPrice" class="formIntegerParamValue" value="${goodPrice}"/>
            </td>
        </tr>

        <tr class="formCompactItemRow">
            <td class="formParamCaption">
                Остаток. Не для показа.
            </td>
            <td>
                <input type="number" id="goodQuantity" name="goodQuantity"
                       class="formIntegerParamValue" value="${goodQuantity}"/>
            </td>
        </tr>

        <tr class="formCompactItemRow">
            <td class="formParamCaption">
                Показывать остаток менее
            </td>
            <td>
                <input type="number" id="goodFreeQuantity" name="goodFreeQuantity"
                       class="formIntegerParamValue" value="${goodFreeQuantity}"/>
            </td>
        </tr>

        <tr class="formCompactItemRow">
            <td class="formParamCaption">
                Товар в продаже
            </td>
            <td>
                <input type="checkbox" id="goodPublished" name="goodPublished" class="formCheckboxParamValue"
                       checked="${goodPublished}"/>
            </td>
        </tr>

        <tr class="formCompactItemRow">
            <td class="formParamCaption">
                Изображения
            </td>
            <td>
                <div style="width: 100%; height: 500px" class="good-form-image-gallery" id="goodFormImageGallery"></div>
                <form method="post" action="#" id="uploadImageForm">
                    <button onclick="document.getElementById('photoData').click(); return false;">Добавить</button>
                    <input onchange="SHOP.editGoodForm.attachImage();" class="formParamValue" type="file" id="photoData" name="photoData" value="test"
                           style="display: none">
                </form>
            </td>
        </tr>

        <tr class="formSpaceSeparator">
            <td colspan="2">

            </td>
        </tr>

        <tr class="formItemRow">
            <td class="formParamCaption">
                <button onclick="SHOP.editGoodForm.saveOrUpdateGood(document.getElementById('goodForm')); return false;">
                    Сохранить
                </button>
            </td>
            <td>
                <button onclick="SHOP.navigator.openGoodsList(); return false;">Отмена</button>
            </td>
        </tr>

        <tr>
            <td colspan="2">

            </td>
        </tr>
    </table>
</form>