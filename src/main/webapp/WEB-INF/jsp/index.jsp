<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="org.springframework.security.core.authority.SimpleGrantedAuthority" %>
<%@ page import="org.springframework.security.core.context.SecurityContextHolder" %>
<%@ page import="static core.database.entity.security.Role.SPRING_ROLE_ADMIN_NAME" %>
<%@ page import="static core.database.entity.security.Role.ROLE_BUYER_NAME" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta name="yandex-verification" content="fe7ed4737f05ac47"/>
    <link href="${jstlCss}" rel="stylesheet"/>
    <link href="css/index.css" rel="stylesheet"/>
    <link href="css/forms.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="webjars/bootstrap/3.3.7/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="webjars/bootstrap/3.3.7/css/bootstrap-responsive.css"/>
    <link rel="stylesheet" type="text/css" href="css/jquery-libs/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="css/jquery-libs/jquery-ui.structure.css">
    <link rel="stylesheet" type="text/css" href="css/jquery-libs/jquery-ui.theme.css">
    <link rel="stylesheet" type="text/css" href="css/jquery-libs/jquery-confirm.min.css">
    <link rel="stylesheet" type="text/css" href="css/buttons.css">
    <link rel="stylesheet" type="text/css" href="css/addons/materialIcons.css">
    <link rel="stylesheet" type="text/css" href="css/addons/material-menu.css">
    <link rel="stylesheet" type="text/css" href="css/widgets/photoSlider.css">
    <link rel="stylesheet" type="text/css" href="css/widgets/shoppingCart.css">
    <link rel="stylesheet" type="text/css" href="css/widgets/goodWidget.css">
    <link rel="stylesheet" type="text/css" href="css/widgets/goodViewDialog.css">
    <script src="js/jquery-lib/jquery-3.3.1.js"></script>
    <script src="js/jquery-lib/jquery-ui.js"></script>
    <script src="js/jquery-lib/jquery-confirm.min.js"></script>
    <script src="js/addons/material-menu.js"></script>
    <script type="text/javascript" src="webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="js/core/defineShopGlobal.js"></script>
    <script src="js/core/HtmlLockScreenCursor.js"></script>
    <script src="js/core/ApiRestClient.js"></script>
    <script src="js/goods/GoodGroupsTreeManager.js"></script>
    <script src="js/goods/GoodsListFilter.js"></script>
    <script src="js/goods/GoodsListManager.js"></script>
    <script src="js/widgets/ProfileManager.js"></script>
    <script src="js/widgets/PhotoSlider.js"></script>
    <script src="js/widgets/ShoppingCart.js"></script>
    <script src="js/widgets/GoodWidget.js"></script>
    <script src="js/widgets/GoodViewDialog.js"></script>

    <%
        if (SecurityContextHolder.getContext().getAuthentication().getAuthorities().contains(new SimpleGrantedAuthority(SPRING_ROLE_ADMIN_NAME))) {
    %>
    <script src="js/goods/EditGoodForm.js"></script>
    <script src="js/goods/GoodGroupForm.js"></script>
    <%
        }
    %>


    <script src="js/main.js"></script>

    <link href="${jstlCss}" rel="stylesheet"/>
</head>
<body onload="start(<%=SecurityContextHolder.getContext().getAuthentication().getAuthorities().contains(new SimpleGrantedAuthority(SPRING_ROLE_ADMIN_NAME))%>)">

<table class="indexContainer">
    <tr>
        <td class="indexHorizontalContainer" colspan="2">
            <%@ include file="widgets/topPanel.jsp" %>
        </td>
    </tr>
    <tr>
        <td class="indexHorizontalContainer" colspan="2">
            <%@ include file="widgets/menu.jsp" %>
        </td>
    </tr>
    <tr>
        <td class="catalogContainer">
            <%@ include file="widgets/catalogPanel.jsp" %>
        </td>
        <td class="pageContextContainer" id="formContainer">
        </td>
    </tr>
    <tr>
        <td class="indexHorizontalContainer" colspan="2">
            <%@ include file="widgets/footerPanel.jsp" %>
        </td>
    </tr>
</table>


</body>

</html>
