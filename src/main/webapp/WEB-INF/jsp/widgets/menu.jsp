<%@ page import="static core.database.entity.security.Role.ROLE_BUYER_NAME" %>
<%@ page import="static core.database.entity.security.Role.SPRING_ROLE_BUYER_NAME" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<link rel="stylesheet" type="text/css" href="css/widgets/mainMenu.css"/>
<table class="main-menu-container">
    <tr>

        <td class="topMenuMainPageItemContainer topMenuItemContainer">
            <div class="menuBackground">
                <span class="menuItemText" onclick="SHOP.navigator.openAbout();">Главная</span>
            </div>
        </td>
        <td class="topMenuItemContainer">
            <div class="menuBackground">
                <a href="/spec" class="menuItemText" onclick="SHOP.navigator.openHowToBuy(); return false;">Спецпредложения</a>
            </div>
        </td>
        <td class="topMenuItemContainer">
            <div class="menuBackground">
                <span class="menuItemText" onclick="SHOP.navigator.openHowToBuy();">Как сделать заказ</span>
            </div>
        </td>
        <td class="topMenuItemContainer">
            <div class="menuBackground">
                <span class="menuItemText" onclick="SHOP.navigator.openAboutPayAndDelivery();">Оплата и доставка</span>
            </div>
        </td>
        <td class="topMenuItemContainer">
            <div class="menuBackground">
                <span class="menuItemText" onclick="SHOP.navigator.openFaq();">FAQ</span>
            </div>
        </td>
        <td class="topMenuItemContainer topMenuAuthItem">
            <div class="menuBackground">
                <% if (SecurityContextHolder.getContext().getAuthentication().getAuthorities().contains(new SimpleGrantedAuthority(SPRING_ROLE_BUYER_NAME))) { %>
                <span class="menuItemText"
                      onclick="SHOP.navigator.openProfile();"><%=SecurityContextHolder.getContext().getAuthentication().getName()%></span>
                /
                <span class="menuItemText" onclick="window.location='logout'">Выход</span>
                <% } else {%>
                <span class="menuItemText" onclick="window.location='login'">Вход / регистрация</span>
                <%}%>
            </div>
        </td>
    </tr>
</table>
<script src="js/widgets/Navigator.js"></script>
<script language="JavaScript">
    new SHOP.Navigator();
</script>