
<%@ page contentType="text/html; charset=UTF-8" %>
<link rel="stylesheet" type="text/css" href="css/widgets/topPanel.css"/>
<table class="topPanel">
    <tr>
        <td class="top-panel-td-wildcard">
            <div class="top-panel-wildcard-container">
                <div class="top-panel-wildcard-image-main"></div>
            </div>
        </td>
        <td class="contactInfo">

        </td>
        <td class="promo">

        </td>
        <td class="top-panel-td-cart">
            <div id="cartContainer">

            </div>

        </td>
    </tr>
</table>
