<%@ page import="static core.database.entity.security.Role.SPRING_ROLE_ADMIN_NAME" %>
<%@ page import="org.springframework.security.core.authority.SimpleGrantedAuthority" %>
<%@ page import="org.springframework.security.core.context.SecurityContextHolder" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<link rel="stylesheet" type="text/css" href="css/page/pageProfile.css"/>
<form class="pageProfile" name="profileForm" id="profileForm">
    <table class="pageProfile">
        <tr class="formHeader">
            <td colspan="2">
                Профиль
            </td>
        </tr>

        <tr class="formItemRow">
            <td class="formParamCaption">
                логин
            </td>
            <td>
                ${userName}
            </td>
        </tr>

        <tr class="formItemRow">
            <td class="formParamCaption">
                Электронная почта
            </td>
            <td>
                <input class="formParamValue" name="eMail" value="${eMail}">
            </td>
        </tr>

        <tr class="formItemRow">
            <td class="formParamCaption">
                Как к Вам обращаться
            </td>
            <td>
                <input class="formParamValue" name="personalization" value="${personalization}">
            </td>
        </tr>

        <tr class="formItemRow">
            <td colspan="2" style="text-align: center;" class="formParamCaption">
                Для смены пароля
            </td>
        </tr>

        <tr class="formItemRow">
            <td class="formParamCaption">
                Новый пароль
            </td>
            <td>
                <input class="formParamValue" type="password" name="newPassword" id="newPassword">
            </td>
        </tr>

        <tr class="formItemRow">
            <td class="formParamCaption">
                Повторно новый пароль
            </td>
            <td>
                <input class="formParamValue" type="password" name="repeatNewPassword" id="repeatNewPassword">
            </td>
        </tr>

        <tr class="formItemRow">
            <td>
                <button class="saveProfileButton" onclick="SHOP.profileManager.saveProfile(document.getElementById('profileForm')); return false;">Сохранить
                </button>
            </td>
            <td>

            </td>
        </tr>


        <% if (SecurityContextHolder.getContext().getAuthentication().getAuthorities().contains(new SimpleGrantedAuthority(SPRING_ROLE_ADMIN_NAME))) {
        %>

        <tr class="formSpaceSeparator">
            <td colspan="2"></td>
        </tr>

        <tr class="formItemRow">
            <td colspan="2">
                <span onclick="SHOP.navigator.openUsers();" class="innerRef activeItem">Пользователи</span>
            </td>
        </tr>

        <tr class="formItemRow">
            <td colspan="2">
                <span onclick="SHOP.navigator.openOrders();" class="innerRef activeItem">Заказы</span>
            </td>
        </tr>

        <%
            }
        %>

        <tr>
            <td colspan="2">

            </td>
        </tr>
    </table>
</form>

