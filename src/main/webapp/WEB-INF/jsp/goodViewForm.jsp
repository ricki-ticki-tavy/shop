<%@ page contentType="text/html; charset=UTF-8" %>
<%--Форма для отображения товара покупателю--%>
<%
    // При открытии как авономной страницы
    if (request.getParameter("nav") == null) {
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="css/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="css/jquery-confirm.min.css">
    <link rel="stylesheet" type="text/css" href="css/widgets/photoSlider.css">
    <link rel="stylesheet" type="text/css" href="css/widgets/goodWidget.css">
    <link href="css/forms.css" rel="stylesheet"/>
    <link href="css/index.css" rel="stylesheet"/>

    <script src="js/jquery-lib/jquery-3.3.1.js"></script>
    <script src="js/core/defineShopGlobal.js"></script>
    <script src="js/core/HtmlLockScreenCursor.js"></script>
    <script src="js/core/ApiRestClient.js"></script>
    <script src="js/widgets/PhotoSlider.js"></script>

</head>
<body onload="document.getElementById('goodViewFormContainer').onload();">
<%
    }
%>
<link rel="stylesheet" type="text/css" href="css/page/pageGoodViewForm.css"/>
<div id="goodViewFormContainer" class="form-flex-vertical-container good-view-form-container"
     onload="SHOP.imageGallery('goodImageCarousel', '${goodImagesIds}'.split(','), 160, 160, false, 'api/buyer/images');">
    <input type="hidden" value="${goodId}">
    <div class="good-view-form-container-image-and-description">
        <div class="good-view-form-container-images-and-buy form-flex-vertical-container">
            <div id="goodImageCarousel" style="width: 600px; height: 600px" class="good-view-image-carousel"></div>
            <div style="height: 37px" class="good-view-form-container-buy form-flex-horizontal-container">
                <div class="good-view-form-good-price">${goodPrice}</div>
                <input class="good-view-form-good-quantity-to-buy" type="number" min="0" max="${goodQuantity}"
                       placeholder="${goodFreeQuantity}"/>
                <div class="good-widget-button-add-to-cart"></div>
            </div>
        </div>
        <span class="good-view-form-title">${goodName}</span><br><br>
        <span class="good-view-good-description">${goodDescription}</span>
        <%--<div class="form-flex-vertical-container good-view-form-container-title-and-description">--%>
        <%--<div class="good-view-form-title">${goodName}</div>--%>
        <%--<div class="good-view-good-description">${goodDescription}</div>--%>
        <%--</div>--%>
    </div>
</div>
<%
    if (request.getParameter("nav") == null) {
%>
</body>
</html>
<%
    }
%>