<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page import="org.springframework.util.StringUtils" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<link rel="stylesheet" type="text/css" href="css/page/pageGoodGroupForm.css"/>
<form action="${priorUrl}" method="get" id="goodGroupForm" class="formBase">
    <input type="hidden" name="currentId" id="currentId" value="${currentId}"/>
    <input type="hidden" name="parentId" value="${parentId}"/>
    <input type="hidden" name="groupAttributes" id="groupAttributes" value='${groupAttributes}'/>
    <table class="pageGoodGroupForm">
        <tr class="formHeader">
            <td colspan="2">
                <%
                    if (request.getParameter("currentId").equals("-1")) {
                %>
                Добавление
                <%
                    if (!request.getParameter("parentId").equals("-1")) {
                %>
                подгруппы в "${parentName}"
                <%
                } else {
                %>
                главной группы
                <%
                    }
                %>
                <%
                } else {
                %>
                Редактирование группы "${groupName}"
                <%
                    }
                %>
            </td>
        </tr>
        <tr class="formItemRow">
            <td class="formParamCaption">
                Название группы
            </td>
            <td>
                <input class="formParamValue" id="groupName" name="groupName" value="${groupName}">
            </td>
        </tr>

        <tr class="formItemRow">
            <td class="formParamCaption">
                Картинка группы
            </td>
            <td>
                <img src="${imageId}" style="width: 200px; height: 200px"/>
                <input class="formParamValue" type="file" name="photoData">
            </td>
        </tr>

        <tr class="formSpaceSeparator">
            <td colspan="2">

            </td>
        </tr>

        <%
            if (request.getParameter("parentId").equals("-1")) {
        %>
        <tr>
            <td colspan="2" class="formParamCaption">Атрибуты товаров группы</td>
        </tr>

        <tr>
            <td colspan="2">
                <table id="goodGroupFormAttributesTable" class="good-group-form-attributes-table">
                    <tr>
                        <td colspan="4" >
                            <div class="good-group-form-attributes-tool-container">
                                <div id="attributeButtonIconPlus" class="buttonIcon buttonIconPlus good-group-form-attributes-tool-button"
                                     title="Добавить атрибут"
                                     onclick="SHOP.goodGroupForm.__addAttributeDialogOpen();"></div>
                                <div id="attributeButtonIconEdit" class="buttonIcon buttonIconEdit good-group-form-attributes-tool-button"
                                     title="Редактировать атрибут"
                                     onclick="SHOP.goodGroupForm.__editAttributeDialogOpen();"></div>
                                <div id="attributeButtonIconRemove" class="buttonIcon buttonIconRemove good-group-form-attributes-tool-button"
                                     title="Удалить атрибут"
                                     onclick="SHOP.goodGroupForm.__deleteAttributeButtonHandler();"></div>
                            </div>

                        </td>
                    </tr>
                    <tr class="good-group-form-attributes-table-header">
                        <td width="50px">ID</td>
                        <td width="180px">Название</td>
                        <td width="80px">Показывать<br>на виджете</td>
                        <td>Возможные значения</td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr class="formSpaceSeparator">
            <td colspan="2">

            </td>
        </tr>

        <%
            }
        %>


        <tr class="formItemRow">
            <td class="formParamCaption">
                <button onclick="SHOP.goodGroupForm.saveOrUpdateGroup(document.getElementById('goodGroupForm')); return false;">
                    Сохранить
                </button>
            </td>
            <td>
                <button onclick="SHOP.navigator.openGoodsList(); return false;">Отмена</button>
            </td>
        </tr>

        <tr>
            <td colspan="2">

            </td>
        </tr>
    </table>
</form>

<div id="goodAttributeFormContainer" class="good-attribute-form-container form-flex-vertical-container" title="Атрибут">
    <form method="post" action="#" id="goodAttributeForm" class="good-attribute-form-form">
        <%--<div id="goodAttributeFormTitle" class="good-attribute-form-title"></div>--%>
        <div class="good-attribute-form-row-container form-flex-horizontal-container">
            <div class="good-attribute-form-field-label">Название</div>
            <div class="good-attribute-form-field-container">
                <input name="attributeName" id="attributeName" type="text"/>
            </div>
        </div>
        <div class="good-attribute-form-row-container form-flex-horizontal-container">
            <div class="good-attribute-form-field-label">Показывать на виджете</div>
            <div class="good-attribute-form-field-container">
                <input type="checkbox" name="attributeShowOnWidget" id="attributeShowOnWidget"
                       style="width: 24px; height: 20px;"/>
            </div>
        </div>
        <div id="goodAttributeValuesContainer"
             class="good-attribute-form-row-container form-flex-horizontal-container">
            <div class="good-attribute-form-field-label">Значения через ";"</div>
            <div class="good-attribute-form-field-container">
                <input name="attributeAvailableValues" id="attributeAvailableValues" type="text"/>
            </div>
        </div>
    </form>
</div>