package api.core.database.service.good;

import api.core.Result;
import api.dto.good.GoodDto;
import api.dto.good.SimpleGoodGroupDto;
import core.database.dao.good.GoodAttributeDto;
import core.database.dao.good.GoodGroupDao;
import core.database.entity.good.GoodAttribute;
import core.database.entity.good.GoodGroup;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Сервис всех операций по товарам, группам, атрибутам
 */
public interface GoodService {
  /**
   * Вернуть дерево групп товаров
   *
   * @return
   */
  List<SimpleGoodGroupDto> getGoodGroupsTree();

  /**
   * Вернуть подгруппы для группы
   *
   * @param parentGroupId -1 для корня
   * @return
   */
  Result<List<SimpleGoodGroupDto>> getGoodGroupsList(Long parentGroupId);

  /**
   * Получить простой DTO по коду
   *
   * @param id
   * @return
   */
  SimpleGoodGroupDto getSimpleGoodGroupById(Long id);

  /**
   * Получить аттрибуты товаров группы. Только для верхнего уровня
   *
   * @param groupId
   * @return
   */
  Result<Set<GoodAttributeDto>> getGoodGroupAttributes(Long groupId);

  /**
   * Получить простой DTO по коду
   *
   * @param id
   * @return
   */
  Result<GoodDto> getGoodDtoById(Long id);

  /**
   * Создать или отредактировать группу товара
   *
   * @param id
   * @param parentId
   * @param groupName
   * @param fileName
   * @param photoData
   * @param groupAttributes - JSON набор атрибутов группы
   * @return
   */
  Result<SimpleGoodGroupDto> saveOrUpdateGroup(Long id, Long parentId, String groupName, String fileName, byte[] photoData, String groupAttributes);

  /**
   * Удалить группу товаров
   *
   * @param id
   * @return
   */
  Result<String> removeGoodGroup(Long id);

  /**
   * Сдвинуть вверх на 1 группу в порядке сортировки
   *
   * @param id
   * @return Если успешно, то возвращает запись группы с которой поменяла местами
   */
  Result<SimpleGoodGroupDto> moveGoodGroupUpOrDown(Long id, GoodGroupDao.MoveDirection direction);

  /**
   * Получить один уровень подгрупп для заданного парента.
   *
   * @param parentGroupId -1 для корневых групп
   * @return
   */
  Result<List<SimpleGoodGroupDto>> getSubGroupsByParent(Long parentGroupId);

  /**
   * Создать или изменить товар
   *
   * @param id
   * @param name
   * @param goodGroup
   * @param goodSubGroup
   * @param imageIds
   * @param goodDescription
   * @param goodPrice
   * @param goodQuantity
   * @param goodFreeQuantity
   * @param goodPublished
   * @return
   */
  Result<GoodDto> saveOrUpdateGood(Long id
          , String name
          , Long goodGroup
          , Long goodSubGroup
          , String goodDescription
          , Long imageIds[]
          , Double goodPrice
          , Double goodQuantity
          , Double goodFreeQuantity
          , boolean goodPublished
          , Map<Long, String> attributes);

  /**
   * Получить список товаров
   *
   * @param filter
   * @param pageId
   * @param pageSize
   * @return
   */
  Result<List<GoodDto>> getGoodsList(Map<String, String> filter, int pageId, int pageSize);

  /**
   * Получить список значений атрибутов товара
   * @param goodId
   * @return
   */
  Result<String> getGoodAttributesValue(Long goodId);
}
