package api.core.database.service;

import api.core.Result;
import core.database.entity.photo.Photo;

public interface ImageService {
  /**
   * Получить файл изображения.
   * @param id                   код базового, оригинального изображения
   * @param resolution          строка с разрешением изображения в формате 0000X0000. Может быть пустой для получения
   *                            полноразмерного изображения
   * @return
   */
  Result<byte[]> getImageBytes(Long id, String resolution);

  Result<Photo> saveImage(String ext, byte[] data, Photo.ImageType imageType);
}
