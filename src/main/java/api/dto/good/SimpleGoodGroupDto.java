package api.dto.good;

import core.database.dto.BaseIdentityDto;
import core.database.entity.good.GoodGroup;

import java.util.List;

public class SimpleGoodGroupDto extends BaseIdentityDto {
  private String name;
  private Long parentId;
  private List<SimpleGoodGroupDto> children;
  private boolean topLevel;
  private Long iconId;

  public SimpleGoodGroupDto(GoodGroup goodGroup) {
    setId(goodGroup.getId());
    setName(goodGroup.getName());
    setTopLevel(goodGroup.getParentGroup() == null);
    setParentId(goodGroup.getParentGroup() == null ? null : goodGroup.getParentGroup().getId());
    setIconId(goodGroup.getIconId());
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<SimpleGoodGroupDto> getChildren() {
    return children;
  }

  public void setChildren(List<SimpleGoodGroupDto> children) {
    this.children = children;
  }

  public boolean isTopLevel() {
    return topLevel;
  }

  public void setTopLevel(boolean topLevel) {
    this.topLevel = topLevel;
  }

  public Long getParentId() {
    return parentId;
  }

  public void setParentId(Long parentId) {
    this.parentId = parentId;
  }

  public Long getIconId() {
    return iconId;
  }

  public void setIconId(Long iconId) {
    this.iconId = iconId;
  }
}
