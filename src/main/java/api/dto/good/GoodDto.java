package api.dto.good;

import core.database.dto.BaseNamedIdentityDto;
import core.database.entity.counter.ShowCounter;
import core.database.entity.good.Good;
import core.database.entity.good.GoodGroup;
import core.database.entity.good.GoodToPhoto;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class GoodDto extends BaseNamedIdentityDto {
  /**
   * Описание в HTML формате
   */
  private String description;

  /**
   * Группа товара
   */
  private Long goodGroup;

  /**
   * Баловая цена
   */
  private double price;

  /**
   * Сделать видимым для покупателей.
   */
  private boolean published;

  /**
   * Всего осталось
   */
  private double quantity;

  /**
   * Свободный остаток
   */
  private double freeQuantity;

  /**
   * Коды изображений
   */
  private List<Long> imageIdList;

  /**
   * Значения атрибутов товара
   */
  private Map<String, String> goodAttributeValues;

  /**
   * Схожие товары
   */
  private Set<Long> relatedGoodSet;

  /**
   * Счетчик просмотров
   */
  private ShowCounter showCounter;

  public String getDescription() {
    return description;
  }

  public GoodDto setDescription(String description) {
    this.description = description;
    return this;
  }

  public Long getGoodGroup() {
    return goodGroup;
  }

  public GoodDto setGoodGroup(Long goodGroup) {
    this.goodGroup = goodGroup;
    return this;
  }

  public double getPrice() {
    return price;
  }

  public GoodDto setPrice(double price) {
    this.price = price;
    return this;
  }

  public boolean isPublished() {
    return published;
  }

  public GoodDto setPublished(boolean published) {
    this.published = published;
    return this;
  }

  public double getQuantity() {
    return quantity;
  }

  public GoodDto setQuantity(double quantity) {
    this.quantity = quantity;
    return this;
  }

  public double getFreeQuantity() {
    return freeQuantity;
  }

  public GoodDto setFreeQuantity(double freeQuantity) {
    this.freeQuantity = freeQuantity;
    return this;
  }

  public List<Long> getImageIdList() {
    return imageIdList;
  }

  /**
   * Список кодов картинок в вие строки
   * @return
   */
  public String getImageIdListAsString() {
    StringBuilder sb = new StringBuilder(200);
    imageIdList.stream()
            .forEach(aLong -> {
              if (sb.toString().length() > 0) {
                sb.append(",");
              }
              sb.append(aLong.toString());
            });

    return sb.toString();
  }

  public GoodDto setImageIdList(List<Long> imageIdList) {
    this.imageIdList = imageIdList;
    return this;
  }

  public Map<String, String> getGoodAttributeValues() {
    return goodAttributeValues;
  }

  public GoodDto setGoodAttributeValues(Map<String, String> goodAttributeValues) {
    this.goodAttributeValues = goodAttributeValues;
    return this;
  }

  public Set<Long> getRelatedGoodSet() {
    return relatedGoodSet;
  }

  public GoodDto setRelatedGoodSet(Set<Long> relatedGoodSet) {
    this.relatedGoodSet = relatedGoodSet;
    return this;
  }

  public ShowCounter getShowCounter() {
    return showCounter;
  }

  public GoodDto setShowCounter(ShowCounter showCounter) {
    this.showCounter = showCounter;
    return this;
  }

  public static GoodDto fromEntity(Good good) {
    return new GoodDto()
            .setDescription(good.getDescription())
            .setGoodGroup(good.getGoodGroup().getId())
            .setPrice(good.getPrice())
            .setQuantity(good.getQuantity())
            .setFreeQuantity(good.getFreeQuantity())
            .setPublished(good.isPublished())
            .setImageIdList(new TreeSet<GoodToPhoto>(good.getPhotoSet()).stream()
                    .map(goodToPhoto -> goodToPhoto.id.photoId)
                    .collect(Collectors.toList()))
            .setName(good.getName())
            .setId(good.getId());
  }
}
