package boot.config;

import core.boot.BootEventListener;
import org.apache.tomcat.dbcp.dbcp2.BasicDataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@ComponentScan(basePackages = {"core", "api"})
@EnableTransactionManagement
public class DatabaseConfiguration {

  @Autowired
  private Environment env;

  @Value("${core.database.driverClassName}")
  String driverClassName;

  @Value("${core.database.url}")
  String databaseUrl;

  @Value("${core.database.username}")
  String databaseUsername;

  @Value("${core.database.password}")
  String databasePassword;

  @Value("${core.database.hibernate.ddl-auto}")
  String ddlAuto;

  @Value("${core.database.hibernate.dialect}")
  String hibernateDialect;




  Properties hibernateProperties() {
    return new Properties() {
      {
        setProperty("hibernate.hbm2ddl.auto", ddlAuto);
        setProperty("hibernate.dialect", hibernateDialect);
        setProperty("hibernate.globally_quoted_identifiers", "true");
        setProperty("hibernate.cache.region.factory_class", "org.hibernate.cache.infinispan.InfinispanRegionFactory");
        setProperty("hibernate.cache.infinispan.cachemanager", "java:CacheManager/entity");
        setProperty("hibernate.cache.inifinispan.statistics", "false");
        setProperty("hibernate.cache.infinispan.cfg", "infinispan.xml");
        setProperty("cache.default_cache_concurrency_strategy", "TRANSACTIONAL");
        setProperty("hibernate.search.default.directory_provider", "filesystem");
        setProperty("cache.use_query_cache", "true");
        setProperty("hibernate.cache.auto_evict_collection_cache", "true");
        setProperty("hibernate.jdbc.lob.non_contextual_creation", "true");
      }
    };
  }


  @Bean
  public LocalSessionFactoryBean sessionFactory() {
    LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
    sessionFactory.setDataSource(restDataSource());
    sessionFactory.setPackagesToScan(new String[]{"core.database.entity"});
    sessionFactory.setHibernateProperties(hibernateProperties());

    return sessionFactory;
  }

  @Bean
  public DataSource restDataSource() {
    BasicDataSource dataSource = new BasicDataSource();
    dataSource.setDriverClassName(driverClassName);
    dataSource.setUrl(databaseUrl);
    dataSource.setUsername(databaseUsername);
    dataSource.setPassword(databasePassword);
    return dataSource;
  }

  @Bean
  @Autowired
  public HibernateTransactionManager transactionManager(
          SessionFactory sessionFactory) {

    HibernateTransactionManager txManager
            = new HibernateTransactionManager();
    txManager.setSessionFactory(sessionFactory);

    return txManager;
  }

  @Bean
  public BootEventListener getDatabaseInitializer(){
    return new BootEventListener();
  }



}
