package core.security;

import com.rfksystems.blake2b.Blake2b;
import org.apache.tomcat.util.buf.HexUtils;

import java.io.UnsupportedEncodingException;

/**
 * Утилиты для безопасности.
 */
public class SecurityUtils {

  public static String blake2Free(int blake2bSize, String source) {
    try {
      Blake2b blake2b = new Blake2b(blake2bSize);
      byte[] data = source.getBytes("UTF-8");
      blake2b.update(data, 0, data.length);
      byte outBuffer[] = new byte[blake2b.getDigestSize()];
      blake2b.digest(outBuffer, 0);
      return HexUtils.toHexString(outBuffer);
    } catch (UnsupportedEncodingException nsae) {
      String errorMessage = "Calculating BLAKE2b hash error: " + nsae.getMessage();
      return null;
    }
  }
  //======================================================================================

  /**
   * Расчет хэше.
   *
   * @param source
   * @return
   */
  public static String blake2(String source) {
    return blake2Free(160, source);
  }
  //======================================================================================

  public static String blake2(byte[] source) {
    Blake2b blake2b = new Blake2b(160);
    blake2b.update(source, 0, source.length);
    byte outBuffer[] = new byte[blake2b.getDigestSize()];
    blake2b.digest(outBuffer, 0);
    return HexUtils.toHexString(outBuffer);
  }
  //======================================================================================

}
