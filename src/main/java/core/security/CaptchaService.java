package core.security;

import api.core.Result;
import core.system.error.SystemErrors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpSession;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.util.Date;
import java.util.Random;

import static core.system.ResultImpl.fail;
import static core.system.ResultImpl.success;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class CaptchaService {

  @Autowired
  private CaptchaStorageSingleton captchaStorage;

  private Random random = new Random(new Date().getTime());

  @Value("${core.security.captcha.width:150}")
  private int captchaWidth;

  @Value("${core.security.captcha.height:50}")
  private int captchaHeight;

  @Value("${core.security.captcha.fontSize:25}")
  private int captchaFontSize;

  @Value("${core.security.captcha.length:8}")
  private int captchaLength;


  private Color randomColor() {
    return new Color(random.nextInt(200), random.nextInt(200), random.nextInt(200));
  }

  public byte[] getImage(HttpSession session) {

    byte[] captchaChallengeAsJpeg = null;

    ByteArrayOutputStream jpegOutputStream = new ByteArrayOutputStream();

    try {
      String captcha = captchaStorage.createAndGet(session.getId());

      BufferedImage image = new BufferedImage(captchaWidth, captchaHeight, BufferedImage.TYPE_INT_RGB);
      Graphics2D g = image.createGraphics();
      g.setColor(Color.YELLOW);
      g.fillRect(0, 0, captchaWidth, captchaHeight);
      g.setColor(Color.blue);

      // шаг отрисовки символов
      int step = (captchaWidth - 10 - 10) / captchaLength;
      int position = 10;

      g.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, captchaFontSize));

      for (int i = 0; i < captchaLength; i++) {
        char chr = captcha.charAt(i);
        g.setColor(randomColor());

        int da = random.nextInt(10) - 5;
        g.rotate(Math.toRadians(da));
        g.drawString(chr + ""
                , position + random.nextInt(captchaFontSize / 2) - captchaFontSize / 42
                , captchaHeight - random.nextInt(captchaHeight - captchaFontSize - 8) - 4);
        g.rotate(-Math.toRadians(da));
        position += step;
      }

      ImageIO.write(image, "jpeg", jpegOutputStream);
    } catch (Throwable e) {
      throw new RuntimeException(e);
    }
    captchaChallengeAsJpeg = jpegOutputStream.toByteArray();

    return captchaChallengeAsJpeg;
  }

  public Result<Boolean> checkCaptcha(HttpSession session, String captcha) {
    return session == null
            ? success(true)
            : success(captchaStorage.findCaptcha(session.getId()))
            .chain(currentCaptcha ->
                    currentCaptcha == null
                            ? fail(SystemErrors.USER_BAD_CAPTCHA.getError())
                            : (captcha.equals(currentCaptcha)
                            ? success(true)
                            : fail(SystemErrors.USER_CAPTCHA_NOT_EQUIVALENT.getError())
                    )
            );
  }
}

