package core.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

/**
 * хранилище капчей
 */
@Component
public class CaptchaStorageSingleton{
  private final Map<String, String> storage = new ConcurrentHashMap<>(500);

  private final Random random = new Random(new Date().getTime());

  public static final String SYM_TABLE[] = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L"
          , "M", "N", "O", "P", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "1", "2", "3", "4", "5"
          , "6", "7", "8", "9"};

  @Value("${core.security.captcha.length:8}")
  private int captchaLength;

  /**
   * Получить капчу по коду сессии
   * @param sessionId
   * @return
   */
  public String findCaptcha(String sessionId){
    return storage.get(sessionId);
  }

  public String generateCaptcha(){
    StringBuilder sb = new StringBuilder(captchaLength);
    while (sb.append(SYM_TABLE[random.nextInt(SYM_TABLE.length)]).length() < captchaLength);
    return sb.toString();
  }

  /**
   * Создать и вернуть капчу
   * @param sessionId
   * @return
   */
  public String createAndGet(String sessionId){
    String captcha = generateCaptcha();
    storage.put(sessionId, captcha);
    return captcha;
  }
}
