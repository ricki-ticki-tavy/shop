package core.system.error;

public enum SystemErrors {

   USER_UNKNOWN_NAME("U-81", "Пользователь с именем %s не найден.")

  , SYSTEM_NOT_REALIZED("SE-1", "Метод %s не реализован.")
  , SYSTEM_USER_ERROR("SE-2", "%s.")
  , SYSTEM_RUNTIME_ERROR("SE-3", "%s.")
  , SYSTEM_OBJECT_ALREADY_INITIALIZED("SE-4", "Объект %s уже инициализирован (%s). Повторное переопределение значения невозможно.")
  , SYSTEM_BAD_PARAMETER("SE-5", "Недопустимое значение параметра %s (%s). %s")
  , SYSTEM_BAD_PARAMETERS("SE-6", "Недопустимый набор параметров. %s")
  , SYSTEM_NOT_AUTHORIZED("SE-7", "Пользователь не авторизован")

  , USER_PASSWORDS_NOT_EQUALS("US-1", "Пароли пусты или не совпадают")
  , USER_EMAIL_ALREADY_USED("US-2", "Адрес электронной почты '%s' уже используется")
  , USER_EMAIL_IS_EMPTY("US-3", "Адрес электронной почты не задан")
  , USER_NAME_IS_EMPTY("US-4", "Не задано имя пользователя")
  , USER_NAME_ALREADY_USED("US-5", "Пользователь '%s' уже существует")
  , USER_CONFIRM_CODE_IS_OUTDATED("US-6", "Ссылка не верна, либо срок действия истек")
  , USER_RESTORE_ACCOUNT_FIELDS_ARE_EMPTY("US-7", "Долно быть заполнено или поле электронной почты или поле логина")
  , USER_RESTORE_ACCOUNT_DOES_NOT_EXISTS("US-8", "Пользователь не найден")
  , USER_BAD_CAPTCHA("US-9", "В сессии нет проверочного кода")
  , USER_CAPTCHA_NOT_EQUIVALENT("US-10", "Проверочный код не совпал")

  , EMAIL_SEND_ERROR("EM-1", "Ошибка отправки письма: %s")
  , EMAIL_RECEIVE_ERROR("EM-2", "Ошибка отправки письма: %s")
  , EMAIL_LETTER_TEMPLATE_READ_ERROR("EM-3", "Не удалось загрузить шаблон письма '%s")


   , GOOD_GROUP_NOT_FOUND_BY_ID("GG-1", "Группа с кодом '%s' не найдена")
   , GOOD_GROUP_DUPLICATE_NAME("GG-2", "Группа с названием '%s' уже существует")
   , GOOD_GROUP_HAS_SUBGROUPS("GG-3", "Группа с кодом '%s' имеет подгруппы")
   , GOOD_GROUP_HAS_GOODS("GG-4", "Группа с кодом '%s' имеет товары")

  , GOOD_GROUP_ATTR_HAS_BAD_ID("GA-1", "Группа '%s' имеет атрибута с кодом %s")

  , GOOD_NOT_FOUND_BY_ID("GD-1", "Товар с кодом '%s' не существует")
  , GOOD_DUPLICATE_NAME("GD-2", "Товар с названием '%s' уже существует")

   , IMAGE_NOT_FOUND_BY_ID("IM-1", "Изображение с кодом '%s' не найдено")
   , IMAGE_BAD_REQUEST_BY_ID("IM-2", "Изображение с кодом '%s' недопустимо для скачивания")
   , IMAGE_BAD_RESOLUTION("IM-3", "Недопустимое разрешение '%s'")
   , IMAGE_BAD_DIMENTION_VALUE("IM-4", "Недопустимое значение '%s' : '%s")
  ;

  private SystemError error;
  private String message;
  private String errorCode;

  public boolean isMatchedTo(SystemError error){
    return error.getCode().equals(errorCode);

  }

  public String getErrorCode() {
    return errorCode;
  }

  public SystemError getError(String... args){
    return new SystemError(errorCode, String.format(String.format("%s - %s", errorCode, message), args));
  }

  public void error(String... args){
    throw getError(args);
  }

  SystemErrors(String errorCode, String message){
    this.errorCode = errorCode;
    this.message = message;
  }



}
