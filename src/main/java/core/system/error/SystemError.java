package core.system.error;

public class SystemError extends RuntimeException{
  private String message;
  private String errorCode;

  public String getCode(){
    return errorCode;
  }

  public SystemError(String errorCode, String message){
    super(message);
    this.message = message;
    this.errorCode = errorCode;
  }
}
