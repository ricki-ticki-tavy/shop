package core.system;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class ImageUtils {
  public static byte[] resizeImage(byte[] imageData, int width, int height){
    try {
      ImageInputStream stream = ImageIO.createImageInputStream(new ByteArrayInputStream(imageData));
      BufferedImage image = ImageIO.read(stream);

      BufferedImage newImage = resizeImage(image, width, height);
      ByteArrayOutputStream jpegOutputStream = new ByteArrayOutputStream();
      ImageIO.write(newImage, "png", jpegOutputStream);

      return jpegOutputStream.toByteArray();
    } catch (IOException ioe){
      throw new RuntimeException(ioe);
    }
  }

  public static BufferedImage resizeImage(final BufferedImage image, int width, int height) {

    int originWidth = image.getWidth();
    int originHeight = image.getHeight();

    // найдем новый размер изображения, чтобы сохранить оригинальное соотношение сторон
    double scaleCoef = Math.max ((double)originWidth / (double)width, (double)originHeight / (double)height);
    int drawWidth = (int)Math.round((double)originWidth / scaleCoef);
    int drawHeight = (int)Math.round((double)originHeight / scaleCoef);
    int drawX = (width - drawWidth) >> 1;
    int drawY = (height - drawHeight) >> 1;

    final BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);

//    int data[] = new int[width];
//    for (int h = 0; h < 10; h++){
//      bufferedImage.getRGB(0, h + height >> 1, width, 1, data, 0, 1);
//      for (int w = 0; w < width; w++){
//        data[w] = 0x300000FF;
//      }
//      bufferedImage.setRGB(0, h + height >> 1, width, 1, data, 0, 1);
//    }




        final Graphics2D graphics2D = bufferedImage.createGraphics();
    graphics2D.setComposite(AlphaComposite.Src);
    //below three lines are for RenderingHints for better image quality at cost of higher processing time
    graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION,RenderingHints.VALUE_INTERPOLATION_BILINEAR);
    graphics2D.setRenderingHint(RenderingHints.KEY_RENDERING,RenderingHints.VALUE_RENDER_QUALITY);
    graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
    graphics2D.drawImage(image, drawX, drawY, drawWidth, drawHeight, null);
    graphics2D.dispose();
    return bufferedImage;
  }

}

