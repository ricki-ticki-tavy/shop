package core.database.dto;

public abstract class BaseIdentityDto {
  private Long id;

  public Long getId() {
    return id;
  }

  public <O extends BaseIdentityDto> O setId(Long id) {
    this.id = id;
    return (O)this;
  }
}
