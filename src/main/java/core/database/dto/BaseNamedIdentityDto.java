package core.database.dto;

public abstract class BaseNamedIdentityDto extends BaseIdentityDto{
  private String name;

  public String getName() {
    return name;
  }

  public <O extends BaseNamedIdentityDto> O setName(String name) {
    this.name = name;
    return (O)this;
  }
}
