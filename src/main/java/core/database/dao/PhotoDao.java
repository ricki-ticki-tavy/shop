package core.database.dao;

import api.core.Result;
import core.database.dao.base.AbstractDao;
import core.database.entity.photo.Photo;
import core.security.SecurityUtils;
import core.system.ImageUtils;
import core.system.ResultImpl;
import core.system.error.SystemErrors;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Transactional(Transactional.TxType.MANDATORY)
public class PhotoDao extends AbstractDao {

  /**
   * Создать фотографию
   *
   * @param ext
   * @param data
   * @return
   */
  public Photo savePhoto(String ext, byte[] data, Photo.ImageType imageType) {
    String hash = SecurityUtils.blake2(data);
    Photo photo = (Photo) getSession().createQuery("from Photo where imageHash=:hash and imageType=:imageType")
            .setParameter("hash", hash)
            .setParameter("imageType", imageType)
            .uniqueResult();
    if (photo == null) {

      photo = (Photo) new Photo()
              .setImage(data)
              .setImageHash(hash)
              .setResolution("origin")
              .setImageType(imageType)
              .setName(ext);

      photo.setId((Long) getSession().save(photo));
    }
    return photo;
  }

  /**
   * Загрузить файл изображения из БД
   *
   * @param id         код оригинального изображения
   * @param resolution разрешение в котором нужно получить изображение. Для оригинального разрешения - пусто
   * @return
   */
  public Result<byte[]> getImageBytes(Long id, String resolution) {
    resolution = resolution.toLowerCase();
    Photo originPhoto = getSession().load(Photo.class, id);
    Photo resultPhoto = null;
    if (originPhoto == null) {
      return ResultImpl.fail(SystemErrors.IMAGE_NOT_FOUND_BY_ID.getError(id.toString()));
    } else if (originPhoto.getImageType() == Photo.ImageType.PROCEEDED) {
      // Попытка скачивания изображения напрямую
      return ResultImpl.fail(SystemErrors.IMAGE_BAD_REQUEST_BY_ID.getError(id.toString()));
    } else {
      if (!resolution.equalsIgnoreCase("origin")) {
        // возможен ресайзинг и обязательно наложение водяного знака, если есть для него изображение
        int width = 0;
        int height = 0;

        // попробуем загрузить изображение: вдруг оно уже есть
        resultPhoto = (Photo) getSession().createQuery("from Photo where originalPhoto = :originalPhoto and resolution= :resolution")
                .setParameter("originalPhoto", originPhoto)
                .setParameter("resolution", resolution)
                .uniqueResult();

        if (resultPhoto == null) {
          // нет еще готового изображения. Надо его обработать
          originPhoto = getSession().get(Photo.class, id);
          // Распарсим размер
          String parts[] = resolution.split("x");
          if (parts.length != 2) {
            // недопустимое кол-во элементов
            return ResultImpl.fail(SystemErrors.IMAGE_BAD_RESOLUTION.getError(id.toString()));
          } else {
            width = Integer.valueOf(parts[0]);
            height = Integer.valueOf(parts[1]);
            if (width < 64 || width > 2048) {
              return ResultImpl.fail(SystemErrors.IMAGE_BAD_DIMENTION_VALUE.getError("ширина", parts[0]));
            } else if (height < 64 || height > 2048) {
              return ResultImpl.fail(SystemErrors.IMAGE_BAD_DIMENTION_VALUE.getError("высота", parts[1]));
            } else {
              // все в норме. меняем размер
              byte[] newImageData = ImageUtils.resizeImage(originPhoto.getImage(), width, height);

              // наложить водяной знак

              // Сохранить в БД
              resultPhoto = (Photo) new Photo()
                      .setImageType(Photo.ImageType.PROCEEDED)
                      .setResolution(resolution)
                      .setImage(newImageData)
                      .setOriginalPhoto(originPhoto)
                      .setName(originPhoto.getName());

              getSession().save(resultPhoto);
            }
          }
        }
      } else {
        // отдаем оригинал, раз указано origin
        resultPhoto = getSession().get(Photo.class, id);
      }
    }
    return ResultImpl.success(resultPhoto.getImage());
  }
}
