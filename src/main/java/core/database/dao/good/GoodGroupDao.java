package core.database.dao.good;

import api.core.Result;
import api.dto.good.GoodDto;
import api.dto.good.SimpleGoodGroupDto;
import com.google.gson.GsonBuilder;
import core.database.dao.base.AbstractDao;
import core.database.entity.good.GoodAttribute;
import core.database.entity.good.GoodGroup;
import core.system.ResultImpl;
import core.system.error.SystemErrors;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import static core.system.ResultImpl.fail;
import static core.system.ResultImpl.success;

/**
 * Доступ к группам товаров
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Transactional(Transactional.TxType.MANDATORY)
public class GoodGroupDao extends AbstractDao {

  public List<GoodGroup> getAll() {
    return getSession().createQuery("from GoodGroup order by orderIndex").list();
  }

  public GoodGroup getById(Long id) {
    return (GoodGroup) getSession()
            .createQuery("from GoodGroup where id = :id")
            .setParameter("id", id)
            .uniqueResult();

  }
  //=====================================================================================================

  public void saveOrUpdate(GoodGroup goodGroup) {
    if (goodGroup.getId() == null) {
      goodGroup.setId((Long) getSession().save(goodGroup));
      goodGroup.setOrderIndex(goodGroup.getId());
      getSession().update(goodGroup);
    } else {
      getSession().update(goodGroup);
    }
  }
  //=====================================================================================================

  /**
   * Проверяет допустимость названия группы
   *
   * @param id
   * @param parentId
   * @param goodName
   * @return
   */
  public boolean nameIsAvail(Long id, Long parentId, String goodName) {
    Long count;
    // для новой группы проверяем внутри родительской без отсечки текущего названия
    if (id == null) {
      count = parentId == null
              ? (Long) getSession().createQuery("select count(id) from GoodGroup where name = :name and parentGroup is null")
              .setParameter("name", goodName)
              .uniqueResult()

              : (Long) getSession().createQuery("select count(id) from GoodGroup where name = :name and parentGroup.id=:parentId")
              .setParameter("name", goodName)
              .setParameter("parentId", parentId)
              .uniqueResult();
    } else {
      // Группа уже есть. Это редактироване
      count = parentId == null
              ? (Long) getSession().createQuery("select count(id) from GoodGroup where name = :name and id <> :id and parentGroup is null")
              .setParameter("name", goodName)
              .setParameter("id", id)
              .uniqueResult()

              : (Long) getSession().createQuery("select count(id) from GoodGroup where name = :name and id <> :id and parentGroup.id=:parentId")
              .setParameter("name", goodName)
              .setParameter("id", id)
              .setParameter("parentId", parentId)
              .uniqueResult();
    }
    return count == 0;
  }
  //=====================================================================================================

  /**
   * Удалить запись по коду
   *
   * @param id
   * @return
   */
  public Result<String> remove(Long id) {
    // проверка, что запись есть
    GoodGroup goodGroup = getSession().get(GoodGroup.class, id);
    if (goodGroup == null) {
      return ResultImpl.fail(SystemErrors.GOOD_GROUP_NOT_FOUND_BY_ID.getError(id.toString()));
    } else
      // проверим, что нет подгрупп
      if (goodGroup.getChildren() != null && goodGroup.getChildren().size() > 0) {
        return ResultImpl.fail(SystemErrors.GOOD_GROUP_HAS_SUBGROUPS.getError(id.toString()));

      } else
        // проверим, что нет товаров за этой группой
        if ((Long) getSession().createQuery("select count(id) from Good where goodGroup.id = :goodGroup")
                .setParameter("goodGroup", id)
                .uniqueResult() != 0) {
          return ResultImpl.fail(SystemErrors.GOOD_GROUP_HAS_GOODS.getError(id.toString()));
        } else {
          // все в норме. Можно удалять
          getSession().remove(goodGroup);
          return success("");
        }
  }
  //=====================================================================================================

  /**
   * Поднять или опустить на одну позицию в списке группу товара. Если успешно, то возвращает запись группы
   * которой поменяла местами
   *
   * @param id
   * @return запись группы с которой поменяла местами
   */
  public Result<GoodGroup> moveGoodGroupUpOrDown(Long id, MoveDirection direction) {
    GoodGroup goodGroup = getSession().get(GoodGroup.class, id);
    if (goodGroup == null) {
      return ResultImpl.fail(SystemErrors.GOOD_GROUP_NOT_FOUND_BY_ID.getError(id.toString()));
    } else {
      // пробуем передвинуть. Найдем минимальный, больший нашего порядок сортировки в заданной группе
      Long priorIndex = null;
      if (goodGroup.getParentGroup() == null) {
        // Это группа верхнего уровня
        switch (direction) {
          case UP:
            priorIndex = (Long) getSession().createQuery("select max(orderIndex) from GoodGroup where orderIndex < :currentOrderIndex and parentGroup is null")
                    .setParameter("currentOrderIndex", goodGroup.getOrderIndex())
                    .uniqueResult();
            break;
          case DOWN:
            priorIndex = (Long) getSession().createQuery("select min(orderIndex) from GoodGroup where orderIndex > :currentOrderIndex and parentGroup is null")
                    .setParameter("currentOrderIndex", goodGroup.getOrderIndex())
                    .uniqueResult();
            break;
        }
      } else {
        switch (direction) {
          case UP:
            priorIndex = (Long) getSession().createQuery("select max(orderIndex) from GoodGroup where orderIndex < :currentOrderIndex and parentGroup.id = :parentId")
                    .setParameter("currentOrderIndex", goodGroup.getOrderIndex())
                    .setParameter("parentId", goodGroup.getParentGroup().getId())
                    .uniqueResult();
            break;
          case DOWN:
            priorIndex = (Long) getSession().createQuery("select min(orderIndex) from GoodGroup where orderIndex > :currentOrderIndex and parentGroup.id = :parentId")
                    .setParameter("currentOrderIndex", goodGroup.getOrderIndex())
                    .setParameter("parentId", goodGroup.getParentGroup().getId())
                    .uniqueResult();
            break;
        }
      }

      if (priorIndex != null) {
        // есть куда двигать
        GoodGroup goodGroupReplaceWith = goodGroup.getParentGroup() == null
                ?
                (GoodGroup) getSession().createQuery("from GoodGroup where orderIndex = :orderIndex and parentGroup is null")
                        .setParameter("orderIndex", priorIndex)
                        .list()
                        .get(0)

                :
                (GoodGroup) getSession().createQuery("from GoodGroup where orderIndex = :orderIndex and parentGroup = :parentGroup")
                        .setParameter("orderIndex", priorIndex)
                        .setParameter("parentGroup", goodGroup.getParentGroup())
                        .list()
                        .get(0);

        Long currentOrderIndex = goodGroup.getOrderIndex();
        goodGroup.setOrderIndex(goodGroupReplaceWith.getOrderIndex());
        goodGroupReplaceWith.setOrderIndex(currentOrderIndex);
        getSession().update(goodGroup);
        getSession().update(goodGroupReplaceWith);
        return success(goodGroupReplaceWith);
      } else {
        return success(null);
      }
    }
  }
  //=====================================================================================================

  /**
   * Получить один уровень подгрупп для заданного парента.
   *
   * @param parentGroupId -1 для корневых групп
   * @return
   */
  public Result<List<SimpleGoodGroupDto>> getSubGroupsByParent(Long parentGroupId) {
    List<GoodGroup> goodGroupList =
            parentGroupId == null || parentGroupId == -1
                    ?
                    getSession().createQuery("from GoodGroup where parentGroup is null order by orderIndex")
                            .list()
                    :
                    getSession().createQuery("from GoodGroup where parentGroup.id = :parentGroupId order by orderIndex")
                            .setParameter("parentGroupId", parentGroupId)
                            .list();
    return success(goodGroupList.stream()
            .map(goodGroup -> new SimpleGoodGroupDto(goodGroup))
            .collect(Collectors.toList()));

  }

  //=====================================================================================================

  /**
   * Интеллектуальное сохранение/ удаление/ редактирование атрибутов товаров группы
   *
   * @param attributesJson
   * @return
   */
  public Result<String> saveGroupAttributes(GoodGroup goodGroup, String attributesJson) {
    if (goodGroup.getParentGroup() == null) {
      // Это руппа верхнего уровня. Привяжем атрибуты
      GoodAttributeDtoList attributeDtoList = new GsonBuilder().create().fromJson("{\"dtos\": " + attributesJson + "}", GoodAttributeDtoList.class);
      final Set<GoodAttribute> currentGoodAttributes = goodGroup.getAttributes() == null
              ? new TreeSet<>()
              : goodGroup.getAttributes();

      // сначала удалим те, которых нет в пришедших
      new ArrayList<GoodAttribute>(currentGoodAttributes).stream()
              .forEach(goodAttribute -> {
                // проверим по коду или имени
                Optional<GoodAttributeDto> foundGoodAttributeDto =
                        attributeDtoList.dtos.stream()
                                .filter(goodAttributeDto -> goodAttributeDto.getId().equals(goodAttribute.getId()))
                                .findFirst();
                //  Если не нашли в пришедших по коду, то ищем по имени
                if (!foundGoodAttributeDto.isPresent()) {
                  foundGoodAttributeDto = attributeDtoList.dtos.stream()
                          .filter(goodAttributeDto -> goodAttributeDto.getId() < 0
                                  && goodAttributeDto.getName().equalsIgnoreCase(goodAttribute.getName()))
                          .findFirst();
                  if (foundGoodAttributeDto.isPresent()) {
                    // Нашли по имени. (удалили по ошибке и тут же добавили обратно в интерфейсе) Восстановим код в пришедшей записи
                    foundGoodAttributeDto.get().setId(goodAttribute.getId());
                  }
                }

                if (!foundGoodAttributeDto.isPresent()) {
                  // не нашли никак. Значит этот атрибут был удален
                  // Удалим значение у всех товаров
                  getSession().createQuery("delete from GoodAttributeValue where goodAttribute.id = :attrId")
                          .setParameter("attrId", goodAttribute.getId())
                          .executeUpdate();
                  // удалим из БД и списка
                  getSession().remove(goodAttribute);
                  currentGoodAttributes.remove(goodAttribute);
                }
              });

      final AtomicInteger atomicInteger = new AtomicInteger(0);
      final AtomicReference<Result> resultAtomicReference = new AtomicReference<>();
      // теперь добавим новые и отредактируем имеющиеся
      new ArrayList<GoodAttributeDto>(attributeDtoList.dtos).stream()
              .forEach(goodAttributeDto -> {
                if (resultAtomicReference.get() == null) {
                  if (goodAttributeDto.getId() < 0) {
                    // это новый атрибут
                    GoodAttribute newGoodAttribute = (GoodAttribute) new GoodAttribute()
                            .setGroupOwner(goodGroup)
                            .setOrderIndex(atomicInteger.getAndIncrement())
                            .setShowValueOverPicture(goodAttributeDto.isShowOnWidget())
                            .setAttributeValues(goodAttributeDto.getAttributeValues())
                            .setName(goodAttributeDto.getName());
                    newGoodAttribute.setId((Long) getSession().save(newGoodAttribute));
                    currentGoodAttributes.add(newGoodAttribute);
                  } else {
                    // правка атрибута
                    // он должОн быть обязательно. Если нету, то тут левый атрибут. Не из это группы
                    Optional<GoodAttribute> editableGoodAttributeOption = currentGoodAttributes.stream()
                            .filter(goodAttribute -> goodAttribute.getId() == goodAttributeDto.getId())
                            .findFirst();
                    if (!editableGoodAttributeOption.isPresent()) {
                      resultAtomicReference.set(fail(SystemErrors.GOOD_GROUP_ATTR_HAS_BAD_ID.getError(goodGroup.getName(), goodAttributeDto.getId().toString())));
                    } else {
                      // нашлось все
                      editableGoodAttributeOption.get()
                              .setGroupOwner(goodGroup)
                              .setOrderIndex(atomicInteger.getAndIncrement())
                              .setShowValueOverPicture(goodAttributeDto.isShowOnWidget())
                              .setAttributeValues(goodAttributeDto.getAttributeValues())
                              .setName(goodAttributeDto.getName());
                      getSession().update(editableGoodAttributeOption.get());
                    }
                  }
                }
              });

      if (resultAtomicReference.get() != null) {
        getSession().getTransaction().markRollbackOnly();
        return resultAtomicReference.get();
      }
    }
    return success("");
  }
  //=====================================================================================================
  //=====================================================================================================
  //=====================================================================================================

  private class GoodAttributeDtoList {
    public List<GoodAttributeDto> dtos;
  }

  public enum MoveDirection {
    UP, DOWN;
  }
}