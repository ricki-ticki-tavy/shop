package core.database.dao.good;

import core.database.dto.BaseNamedIdentityDto;
import core.database.entity.good.GoodAttribute;

/**
 * Дто для атрибутов
 */
public class GoodAttributeDto extends BaseNamedIdentityDto implements Comparable<GoodAttributeDto>{
  /**
   *  Список возможных значений атрибута, если fixedValue включен
   */
  private String attributeValues;

  /**
   * Порядок сортировки при отображении
   */
  private int orderIndex;

  /**
   * Отображать значение данного атрибута на виджете товара поверх фото.
   */
  private boolean showOnWidget;

  public String getAttributeValues() {
    return attributeValues;
  }

  public GoodAttributeDto setAttributeValues(String attributeValues) {
    this.attributeValues = attributeValues;
    return this;
  }

  public int getOrderIndex() {
    return orderIndex;
  }

  public GoodAttributeDto setOrderIndex(int orderIndex) {
    this.orderIndex = orderIndex;
    return this;
  }

  public boolean isShowOnWidget() {
    return showOnWidget;
  }

  public GoodAttributeDto setShowOnWidget(boolean showOnWidget) {
    this.showOnWidget = showOnWidget;
    return this;
  }

  public static GoodAttributeDto fromEntity(GoodAttribute source){
    return new GoodAttributeDto()
    .setOrderIndex(source.getOrderIndex())
    .setAttributeValues(source.getAttributeValues())
    .setShowOnWidget(source.isShowValueOverPicture())
    .setName(source.getName())
    .setId(source.getId());
  }

  @Override
  public int compareTo(GoodAttributeDto o) {
    return ((Integer)orderIndex).compareTo(o.getOrderIndex());
  }

}
