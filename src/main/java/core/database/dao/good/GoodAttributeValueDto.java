package core.database.dao.good;

public class GoodAttributeValueDto {
  private Long id;
  private String value;

  public Long getId() {
    return id;
  }

  public GoodAttributeValueDto setId(Long id) {
    this.id = id;
    return this;
  }

  public String getValue() {
    return value;
  }

  public GoodAttributeValueDto setValue(String value) {
    this.value = value;
    return this;
  }
}
