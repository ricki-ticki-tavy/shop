package core.database.dao.good;

import api.core.Result;
import api.dto.good.GoodDto;
import com.google.gson.GsonBuilder;
import core.database.dao.base.AbstractDao;
import core.database.entity.good.*;
import core.database.entity.photo.Photo;
import core.system.ResultImpl;
import core.system.error.SystemError;
import core.system.error.SystemErrors;
import org.hibernate.query.Query;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Transactional(Transactional.TxType.MANDATORY)
public class GoodDao extends AbstractDao {

  public static final String GROUP_ID_PARAM_NAME = "groupId";

  /**
   * Сохранить новый или отредактировать имеющийся товар
   *
   * @param id
   * @param name
   * @param goodGroup
   * @param goodDescription
   * @param imageIds
   * @param goodPrice
   * @param goodQuantity
   * @param goodFreeQuantity
   * @param goodPublished
   * @return
   */
  public Result<GoodDto> saveOrUpdateGood(Long id
          , String name
          , Long goodGroup
          , String goodDescription
          , Long imageIds[]
          , Double goodPrice
          , Double goodQuantity
          , Double goodFreeQuantity
          , boolean goodPublished
          , Map<Long, String> attributes) {
    Good good = (id != null && id > -1)
            ? getSession().get(Good.class, id)
            : new Good();

    if (good == null) {
      // может быть только если мы его искали в БД и не нашли
      return ResultImpl.fail(SystemErrors.GOOD_NOT_FOUND_BY_ID.getError(id.toString()));
    }

    Query checkNameQuery = getSession().createQuery("select count(*) from Good where name = :name"
            + ((id != null && id > -1)
            ? " and id <> :id"
            : ""));

    if (id != null && id > -1) {
      // проверим на дубликат названия с учетом того, что этот товар уже есть
      checkNameQuery.setParameter("id", id);
    }
    Long counter = (Long) checkNameQuery.setParameter("name", name).uniqueResult();
    if (counter > 0) {
      return ResultImpl.fail(SystemErrors.GOOD_DUPLICATE_NAME.getError(name));
    }

    // группа товара
    GoodGroup group = getSession().load(GoodGroup.class, goodGroup);
    if (group == null) {
      return ResultImpl.fail(SystemErrors.GOOD_GROUP_NOT_FOUND_BY_ID.getError(goodGroup.toString()));
    }

    // прежние атрибуты
    if (id != null && id > -1) {
      good.getGoodAttributeValues().stream()
              .forEach(goodAttributeValue -> getSession().remove(goodAttributeValue));
    }

    good.setGoodGroup(group)
            .setDescription(goodDescription)
            .setPrice(goodPrice)
            .setFreeQuantity(goodFreeQuantity)
            .setQuantity(goodQuantity)
            .setPublished(goodPublished)
            .setGoodAttributeValues(
                    attributes.keySet().stream()
                            .map(valueId -> new GoodAttributeValue()
                                    .setGoodAttribute(getSession().load(GoodAttribute.class, valueId))
                                    .setGood(good)
                                    .setAttributeValue(attributes.get(valueId)))
//                            .peek(goodAttributeValue -> getSession().save(goodAttributeValue))
                            .collect(Collectors.toSet())
            )
            .setName(name);

    if (id != null && id > -1) {
      getSession().update(good);
    } else {
      good.setId((Long) getSession().save(good));
    }


    return bindImages(good, imageIds)
            .map(fineGood -> {
              getSession().update(good);
              return ResultImpl.success(GoodDto.fromEntity(good));
            });

  }
  //=====================================================================================================

  public Result<List<GoodDto>> getGoodsListByFilter(Map<String, String> filter, int pageId, int pageSize) {
    Long goodId = Long.parseLong(filter.get(GROUP_ID_PARAM_NAME));
    StringBuilder qBuilder = new StringBuilder(1024)
            .append("from Good where goodGroup.id=:goodGroup");

    // фильтр. пока пропускаем
    filter.keySet().stream()
            .filter(s -> !s.equalsIgnoreCase(GROUP_ID_PARAM_NAME))
            .forEach(key -> {

            });

    Query query = getSession().createQuery(qBuilder.toString())
            .setFirstResult(pageSize * pageId)
            .setMaxResults(pageSize)
            .setParameter("goodGroup", goodId);

    return ResultImpl.success(1)
            .mapSafe(s -> ResultImpl.success(
                    ((List<Good>) query.list())
                            .stream()
                            .map(good -> GoodDto.fromEntity(good))
                            .collect(Collectors.toList())
                    )
            );
  }
  //=====================================================================================================
  //=====================================================================================================

  /**
   * Привязка изображений к товару при создании / редактировании
   *
   * @param good
   * @param imageIds
   * @return
   */
  private Result<Good> bindImages(Good good, Long[] imageIds) {
    getSession().flush();
    // зачистим все фотки
    if (good.getPhotoSet() == null) {
      good.setPhotoSet(new HashSet<>());
    } else {
      good.getPhotoSet().stream()
              .forEach(goodToPhoto ->
                      getSession().remove(getSession().load(GoodToPhoto.class, goodToPhoto.id)));
      good.getPhotoSet().clear();
    }

    int index = 0;
    for (Long imageId : imageIds) {
      Photo photo = getSession().load(Photo.class, imageId);
      if (photo == null) {
        return ResultImpl.fail(SystemErrors.IMAGE_NOT_FOUND_BY_ID.getError(imageId.toString()));
      } else {
        GoodToPhoto goodToPhoto = new GoodToPhoto();
        goodToPhoto.id = new GoodToPhotoId(good.getId(), imageId);
        goodToPhoto.orderIndex = index++;
        goodToPhoto.photo = photo;
        goodToPhoto.good = good;
        getSession().save(goodToPhoto);
        good.getPhotoSet().add(goodToPhoto);
      }
    }

    return ResultImpl.success(good);
  }
  //=====================================================================================================

  public Result<GoodDto> getDtoById(Long id) {
    Good good = getSession().get(Good.class, id);
    return good == null
            ? ResultImpl.fail(SystemErrors.GOOD_NOT_FOUND_BY_ID.getError(id.toString()))
            : ResultImpl.success(GoodDto.fromEntity(good));
  }
  //=====================================================================================================

  /**
   * Получить аттрибуты товаров группы. Только для верхнего уровня
   *
   * @param groupId
   * @return
   */
  public Result<Set<GoodAttributeDto>> getGoodGroupAttributes(Long groupId) {
    GoodGroup goodGroup = getSession().get(GoodGroup.class, groupId);
    return goodGroup == null
            ? ResultImpl.fail(SystemErrors.GOOD_GROUP_NOT_FOUND_BY_ID.getError(goodGroup.toString()))
            : (goodGroup.getParentGroup() != null
            ? ResultImpl.fail(SystemErrors.GOOD_GROUP_NOT_FOUND_BY_ID.getError(groupId.toString(), goodGroup.getName()))
            : ResultImpl.success(new TreeSet(
            goodGroup.getAttributes().stream()
                    .map(goodAttribute -> GoodAttributeDto.fromEntity(goodAttribute))
                    .collect(Collectors.toList())
    )));
  }
//=====================================================================================================

  /**
   * Получить список значений атрибутов товара
   *
   * @param goodId
   * @return
   */
  public String getGoodAttributesValue(Long goodId) {
    return new GsonBuilder().create().toJson(
            ((List<Object[]>) getSession().createQuery("select ga.id, gav.attributeValue  from GoodAttributeValue gav join gav.good gd join gav.goodAttribute ga where gd.id = :goodId")
                    .setParameter("goodId", goodId)
                    .list())
                    .stream()
                    .map(goodAttributeValue -> new GoodAttributeValueDto()
                            .setId((Long) goodAttributeValue[0])
                            .setValue((String) goodAttributeValue[1]))
                    .collect(Collectors.toList())
    );

  }
//=====================================================================================================


}
