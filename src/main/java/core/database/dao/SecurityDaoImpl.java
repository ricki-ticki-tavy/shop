package core.database.dao;

import api.core.Result;
import api.core.database.dao.SecurityDao;
import core.database.dao.base.AbstractDao;
import core.database.entity.security.Role;
import core.database.entity.security.User;
import core.database.entity.security.UserAccountOperationRequest;
import core.security.SecurityUtils;
import core.system.error.SystemErrors;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Date;

import static core.database.entity.security.UserAccountOperationRequest.AccountOperation.REGISTER;
import static core.database.entity.security.UserAccountOperationRequest.AccountOperation.RESTORES_ACCOUNT_ACCESS;
import static core.system.ResultImpl.fail;
import static core.system.ResultImpl.success;

/**
 * Обеспечивает доступ к объектам БД, связанным с пользователями и безопасностью
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SecurityDaoImpl extends AbstractDao implements SecurityDao {

  private static final String FIND_USER_BY_NAME_QUERY = "from User where name = :userName";
  private static final String FIND_USER_BY_EMAIL_QUERY = "from User where eMail = :eMail";
  private static final String FIND_REGISTER_USER_REQUEST_BY_EMAIL_QUERY = "from UserAccountOperationRequest where eMail = :eMail and created > :created and accountOperation=:accountOperation";
  private static final String FIND_REGISTER_USER_REQUEST_BY_NAME_QUERY = "from UserAccountOperationRequest where name = :userName and created > :created and accountOperation=:accountOperation";
  private static final String FIND_REGISTER_USER_REQUEST_BY_CONFIRM_CODE_QUERY = "from UserAccountOperationRequest where confirmCode = :confirmCode and created > :created and accountOperation=:accountOperation";

  private static final String CLEAR_RESTORE_USER_ACCOUNT_REQUEST_RECORD_BY_CODE_QUERY = "delete from UserAccountOperationRequest where confirmCode=:confirmCode";
  private static final String CLEAR_RESTORE_USER_ACCOUNT_REQUEST_RECORD_QUERY = "delete from UserAccountOperationRequest where name=:userName";

  private static final String FIND_ROLE_BY_NAME_QUERY = "from Role where name = :roleName";

  @Value("${core.security.register.timeout:600}")
  private int REGISTER_USER_REQUEST_TIMEOUT;

  @Override
  public Result<User> loadByName(String userName) {
    return success(true)
            .mapSafe(o -> {
              User entityPlayer = (User) getSession()
                      .createQuery(FIND_USER_BY_NAME_QUERY)
                      .setParameter("userName", userName)
                      .uniqueResult();
              return entityPlayer == null
                      ? fail(SystemErrors.USER_UNKNOWN_NAME.getError(userName))
                      : success(entityPlayer);
            });
  }
  //========================================================================================

  @Override
  public Result<UserAccountOperationRequest> saveRegisterUserRequest(String userName, String password, String eMail) {
    return success(true)
            .mapSafe(o -> {
              UserAccountOperationRequest request = new UserAccountOperationRequest(userName
                      , password
                      , eMail
                      , SecurityUtils.blake2Free(256, userName + "##" + password + "##" + eMail + "##" + new Date())
                      , REGISTER);
              getSession().save(request);
              return success(request);
            });
  }
  //========================================================================================

  @Override
  public Result<UserAccountOperationRequest> saveRestoreUserAccountAccess(User user) {
    return success(true)
            .mapSafe(o -> {
              // Удалим, возможно существующую, старую запись на восстановление доступа
              getSession().createQuery(CLEAR_RESTORE_USER_ACCOUNT_REQUEST_RECORD_QUERY)
                      .setParameter("userName", user.getName())
                      .executeUpdate();

              // добавим новую запись
              UserAccountOperationRequest request = new UserAccountOperationRequest(user.getName()
                      , ""
                      , user.geteMail()
                      , SecurityUtils.blake2Free(256, user.getName() + "##" + user.getPasswordBlake2()
                      + "##" + user.getName() + "##" + new Date())
                      , RESTORES_ACCOUNT_ACCESS);
              getSession().save(request);
              return success(request);
            });
  }
  //========================================================================================

  @Override
  public Result<User> setNewPasswordForUserName(String userName, String password, String confirmCode) {
    return success(true)
            .mapSafe(o ->
                    // Удалим существующую запись запроса на восстановление доступа. ВСЕ для этого пользователя.
                    getSession().createQuery(CLEAR_RESTORE_USER_ACCOUNT_REQUEST_RECORD_BY_CODE_QUERY)
                            .setParameter("confirmCode", confirmCode)
                            .executeUpdate() == 0
                            // а ни одной записи не удалили. Значит каким-то образом нет запроса. Проверка дублирует
                            // ту, что в севисе
                            ? fail(SystemErrors.USER_CONFIRM_CODE_IS_OUTDATED.getError())
                            : success(true)
                            // найдем пользователя
                            .chain(o1 -> loadByName(userName))
                            .chain(user -> {
                              ((User) user).setPassword(password);
                              getSession().saveOrUpdate((User) user);
                              return success((User) user);
                            })
            );

  }
  //========================================================================================

  @Override
  public Result<UserAccountOperationRequest> removeRegisterUserRequest(UserAccountOperationRequest registerUserRequest) {
    return success(true)
            .mapSafe(o -> {
              getSession().delete(registerUserRequest);
              getSession().flush();
              return success(registerUserRequest);
            });
  }
  //========================================================================================

  @Override
  public Result<UserAccountOperationRequest> findLiveRegisterUserRequestByeMail(String eMail) {
    return success(true)
            .mapSafe(o -> {
              UserAccountOperationRequest registerUserRequest = (UserAccountOperationRequest) getSession()
                      .createQuery(FIND_REGISTER_USER_REQUEST_BY_EMAIL_QUERY)
                      .setParameter("eMail", eMail)
                      .setParameter("accountOperation", REGISTER)
                      .setParameter("created", new Date(new Date().getTime() - REGISTER_USER_REQUEST_TIMEOUT * 1000))
                      .uniqueResult();
              return success(registerUserRequest);
            });
  }
  //========================================================================================

  @Override
  public Result<UserAccountOperationRequest> findLiveRegisterUserRequestByUsername(String userName) {
    return success(true)
            .mapSafe(o -> {
              UserAccountOperationRequest registerUserRequest = (UserAccountOperationRequest) getSession()
                      .createQuery(FIND_REGISTER_USER_REQUEST_BY_NAME_QUERY)
                      .setParameter("userName", userName)
                      .setParameter("accountOperation", REGISTER)
                      .setParameter("created", new Date(new Date().getTime() - REGISTER_USER_REQUEST_TIMEOUT * 1000))
                      .uniqueResult();
              return success(registerUserRequest);
            });
  }
  //========================================================================================

  private Result<UserAccountOperationRequest> innerFindLiveRequestByConfirmCode(String confirmCode, UserAccountOperationRequest.AccountOperation accountOperation) {
    return success(true)
            .mapSafe(o -> {
              UserAccountOperationRequest registerUserRequest = (UserAccountOperationRequest) getSession()
                      .createQuery(FIND_REGISTER_USER_REQUEST_BY_CONFIRM_CODE_QUERY)
                      .setParameter("confirmCode", confirmCode)
                      .setParameter("accountOperation", accountOperation)
                      .setParameter("created", new Date(new Date().getTime() - REGISTER_USER_REQUEST_TIMEOUT * 1000))
                      .uniqueResult();
              return success(registerUserRequest);
            });
  }
  //========================================================================================

  @Override
  public Result<UserAccountOperationRequest> findLiveRegisterUserRequestByConfirmCode(String confirmCode) {
    return innerFindLiveRequestByConfirmCode(confirmCode, REGISTER);
  }
  //========================================================================================

  @Override
  public Result<UserAccountOperationRequest> findLiveRestoreUserAccountRequestByConfirmCode(String confirmCode) {
    return innerFindLiveRequestByConfirmCode(confirmCode, RESTORES_ACCOUNT_ACCESS);
  }
  //========================================================================================

  @Override
  public Result<User> findUserByeMail(String eMail) {
    return success(true)
            .mapSafe(o -> {
              User user = (User) getSession()
                      .createQuery(FIND_USER_BY_EMAIL_QUERY)
                      .setParameter("eMail", eMail)
                      .uniqueResult();
              return success(user);
            });
  }
  //========================================================================================

  @Override
  public Result<User> findUserByName(String userName) {
    return success(true)
            .mapSafe(o -> {
              User user = (User) getSession()
                      .createQuery(FIND_USER_BY_NAME_QUERY)
                      .setParameter("userName", userName)
                      .uniqueResult();
              return success(user);
            });
  }
  //========================================================================================

  @Override
  public Result<Role> findRoleByName(String roleName) {
    return success(true)
            .mapSafe(o -> {
              Role role = (Role) getSession()
                      .createQuery(FIND_ROLE_BY_NAME_QUERY)
                      .setParameter("roleName", roleName)
                      .uniqueResult();
              return success(role);
            });
  }
  //========================================================================================

  @Override
  public Result<User> saveUser(User user) {
    return success(true)
            .mapSafe(o -> {
              getSession().saveOrUpdate(user);
              getSession().flush();
              return success(user);
            });
  }
  //========================================================================================

}
