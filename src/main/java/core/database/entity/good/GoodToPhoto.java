package core.database.entity.good;

import core.database.entity.photo.Photo;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import java.util.Objects;

/**
 * Связка между товарами и фото
 */
@Entity
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class GoodToPhoto implements Comparable<GoodToPhoto>{

  @EmbeddedId
  public GoodToPhotoId id;

  @ManyToOne
  @MapsId("goodId")
  public Good good;

  @ManyToOne
  @MapsId("photoId")
  public Photo photo;
  /**
   * Порядок изображения при сортировке
   */
  public int orderIndex;

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;

    if (o == null || getClass() != o.getClass())
      return false;

    GoodToPhoto that = (GoodToPhoto) o;
    return Objects.equals(good.getId(), that.good.getId()) &&
            Objects.equals(photo.getId(), that.photo.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(good.getId(), photo.getId());
  }

  @Override
  public int compareTo(GoodToPhoto o) {
    return ((Integer)(orderIndex)).compareTo(o.orderIndex);
  }
}
