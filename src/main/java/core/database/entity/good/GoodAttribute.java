package core.database.entity.good;

import core.database.entity.base.BaseNamedEntity;
import core.database.entity.good.GoodAttributeValue;
import core.database.entity.good.GoodGroup;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.util.Set;

import static javax.persistence.CascadeType.ALL;

/**
 * Атрибут товара
 */
@Entity
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class GoodAttribute extends BaseNamedEntity implements Comparable<GoodAttribute>{

  /**
   * Группа, к которой принадлежат атрибуты
   */
  private GoodGroup groupOwner;

  /**
   *  Список возможных значений атрибута, если fixedValue включен
   */
  private String attributeValues;

  /**
   * Порядок сортировки при отображении
   */
  private int orderIndex;

  /**
   * Отображать значение данного атрибута на виджете товара поверх фото.
   */
  private boolean showValueOverPicture;

  /**
   * товары, имеющие этот атрибут
   */
  private Set<GoodAttributeValue>  goodAttributeValues;

  public String getAttributeValues() {
    return attributeValues;
  }

  public GoodAttribute setAttributeValues(String attributeValues) {
    this.attributeValues = attributeValues;
    return this;
  }

  @Column(nullable = false)
  public int getOrderIndex() {
    return orderIndex;
  }

  public GoodAttribute setOrderIndex(int orderIndex) {
    this.orderIndex = orderIndex;
    return this;
  }

  @Column(nullable = false)
  public boolean isShowValueOverPicture() {
    return showValueOverPicture;
  }

  public GoodAttribute setShowValueOverPicture(boolean showValueOverPicture) {
    this.showValueOverPicture = showValueOverPicture;
    return this;
  }

  @OneToMany(fetch = FetchType.LAZY, cascade = ALL, orphanRemoval = true, mappedBy = "goodAttribute")
  public Set<GoodAttributeValue> getGoodAttributeValues() {
    return goodAttributeValues;
  }

  public GoodAttribute setGoodAttributeValues(Set<GoodAttributeValue> goodAttributeValues) {
    this.goodAttributeValues = goodAttributeValues;
    return this;
  }

  @ManyToOne(optional = false, fetch = FetchType.EAGER)
  public GoodGroup getGroupOwner() {
    return groupOwner;
  }

  public GoodAttribute setGroupOwner(GoodGroup groupOwner) {
    this.groupOwner = groupOwner;
    return this;
  }

  @Override
  public int compareTo(GoodAttribute o) {
    return ((Integer)orderIndex).compareTo(o.orderIndex);
  }
}
