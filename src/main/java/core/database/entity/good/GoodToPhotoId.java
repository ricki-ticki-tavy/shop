package core.database.entity.good;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class GoodToPhotoId implements Serializable {

  @Column(name = "good_id")
  public Long goodId;

  @Column(name = "photo_id")
  public Long photoId;

  private GoodToPhotoId() {}

  public GoodToPhotoId(
          Long goodId,
          Long photoId) {
    this.goodId = goodId;
    this.photoId = photoId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;

    if (o == null || getClass() != o.getClass())
      return false;

    GoodToPhotoId that = (GoodToPhotoId) o;
    return Objects.equals(goodId, that.goodId) &&
            Objects.equals(photoId, that.photoId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(goodId, photoId);
  }

}
