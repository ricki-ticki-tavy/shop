package core.database.entity.good;


import core.database.entity.base.BaseIdentityEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

/**
 * Связь товара с атрибутами
 */
@Entity
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class GoodAttributeValue extends BaseIdentityEntity{
  /**
   * Атрибут. Тту перегрузка данных
   */
  private GoodAttribute goodAttribute;

  /**
   * Значение атрибута
   */
  private String attributeValue;

  /**
   * Значение, если атрибут допускает ручной ввод значения
   */
  private String freeValue;

  /**
   * Товар к которому относится атрибут
   */
  private Good good;

  @ManyToOne(fetch = FetchType.EAGER)
  public GoodAttribute getGoodAttribute() {
    return goodAttribute;
  }

  public GoodAttributeValue setGoodAttribute(GoodAttribute goodAttribute) {
    this.goodAttribute = goodAttribute;
    return this;
  }

  public String getAttributeValue() {
    return attributeValue;
  }

  public GoodAttributeValue setAttributeValue(String attributeValue) {
    this.attributeValue = attributeValue;
    return this;
  }

  public String getFreeValue() {
    return freeValue;
  }

  public GoodAttributeValue setFreeValue(String freeValue) {
    this.freeValue = freeValue;
    return this;
  }

  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  public Good getGood() {
    return good;
  }

  public GoodAttributeValue setGood(Good good) {
    this.good = good;
    return this;
  }
}
