package core.database.entity.good;

import core.database.entity.photo.Photo;
import core.database.entity.base.BaseNamedEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.util.Set;

/**
 * Группа товара
 */
@Entity
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class GoodGroup extends BaseNamedEntity {

  private GoodGroup parentGroup;
  private Set<GoodGroup> children;
  private Set<Good> goods;

  private Set<GoodAttribute> attributes;

  /**
   * Порядок сортировки. Сквозной индекс. Изначально берем равным полученному ID, добавляя таким образом запись
   * в самый хвост
   */
  private long orderIndex;

  /**
   * Иконка для группы
   */
  private Photo icon;

  /**
   * Код фото для перегрузки в DTO
   */
  private Long iconId;

  @ManyToOne(fetch = FetchType.LAZY, optional = true)
  @JoinColumn(name = "parentGroup")
  public GoodGroup getParentGroup() {
    return parentGroup;
  }

  public GoodGroup setParentGroup(GoodGroup parentGroup) {
    this.parentGroup = parentGroup;
    return this;
  }

  @OneToMany(fetch = FetchType.LAZY, orphanRemoval = true, mappedBy = "parentGroup")
  public Set<GoodGroup> getChildren() {
    return children;
  }

  public GoodGroup setChildren(Set<GoodGroup> children) {
    this.children = children;
    return this;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  public Photo getIcon() {
    return icon;
  }

  public GoodGroup setIcon(Photo icon) {
    this.icon = icon;
    this.iconId = icon == null ? null : icon.getId();
    return this;
  }

  @Column(nullable = false)
  public long getOrderIndex() {
    return orderIndex;
  }

  public GoodGroup setOrderIndex(long orderIndex) {
    this.orderIndex = orderIndex;
    return this;
  }

  @OneToMany(fetch = FetchType.LAZY, orphanRemoval = true, mappedBy = "goodGroup")
  public Set<Good> getGoods() {
    return goods;
  }

  public GoodGroup setGoods(Set<Good> goods) {
    this.goods = goods;
    return this;
  }

  public Long getIconId() {
    return iconId;
  }

  public GoodGroup setIconId(Long iconId) {
    this.iconId = iconId;
    return this;
  }

  @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "groupOwner")
  public Set<GoodAttribute> getAttributes() {
    return attributes;
  }

  public GoodGroup setAttributes(Set<GoodAttribute> attributes) {
    this.attributes = attributes;
    return this;
  }
}
