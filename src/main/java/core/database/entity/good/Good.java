package core.database.entity.good;

import api.dto.good.GoodDto;
import core.database.entity.base.BaseNamedEntity;
import core.database.entity.counter.ShowCounter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.CascadeType.ALL;

/**
 * Товар
 */
@Entity
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class Good extends BaseNamedEntity {

  /**
   * Описание в HTML формате
   */
  private String description;

  /**
   * Группа товара
   */
  private GoodGroup goodGroup;

  /**
   * Баловая цена
   */
  private double price;

  /**
   * Сделать видимым для покупателей.
   */
  private boolean published;

  /**
   * Всего осталось
   */
  private double quantity;

  /**
   * Свободный остаток
   */
  private double freeQuantity;

  /**
   * Изображения товара
   */
  private Set<GoodToPhoto> photoSet;

  /**
   * Значения атрибутов товара
   */
  private Set<GoodAttributeValue> goodAttributeValues;

  /**
   * Схожие товары
   */
  private Set<Good> relatedGoodSet;

  /**
   * Счетчик просмотров
   */
  private ShowCounter showCounter;


  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  public GoodGroup getGoodGroup() {
    return goodGroup;
  }

  public Good setGoodGroup(GoodGroup goodGroup) {
    this.goodGroup = goodGroup;
    return this;
  }

  @Column(columnDefinition = "text")
  public String getDescription() {
    return description;
  }

  public Good setDescription(String description) {
    this.description = description;
    return this;
  }

  public double getPrice() {
    return price;
  }

  public Good setPrice(double price) {
    this.price = price;
    return this;
  }

  public boolean isPublished() {
    return published;
  }

  public Good setPublished(boolean published) {
    this.published = published;
    return this;
  }

  public double getQuantity() {
    return quantity;
  }

  public Good setQuantity(double quantity) {
    this.quantity = quantity;
    return this;
  }

  public double getFreeQuantity() {
    return freeQuantity;
  }

  public Good setFreeQuantity(double freeQuantity) {
    this.freeQuantity = freeQuantity;
    return this;
  }

  @ManyToMany(fetch = FetchType.LAZY)
  public Set<Good> getRelatedGoodSet() {
    return relatedGoodSet;
  }

  public Good setRelatedGoodSet(Set<Good> relatedGoodSet) {
    this.relatedGoodSet = relatedGoodSet;
    return this;
  }

  @OneToMany(fetch = FetchType.LAZY, orphanRemoval = true, cascade = ALL, mappedBy = "good")
  public Set<GoodToPhoto> getPhotoSet() {
    return photoSet;
  }

  public Good setPhotoSet(Set<GoodToPhoto> photoSet) {
    this.photoSet = photoSet;
    return this;
  }

  @OneToOne(fetch = FetchType.LAZY, orphanRemoval = true, cascade = ALL)
  public ShowCounter getShowCounter() {
    return showCounter;
  }

  public void setShowCounter(ShowCounter showCounter) {
    this.showCounter = showCounter;
  }

  @OneToMany(fetch = FetchType.LAZY, cascade = ALL, orphanRemoval = true, mappedBy = "good")
  public Set<GoodAttributeValue> getGoodAttributeValues() {
    return goodAttributeValues;
  }

  public Good setGoodAttributeValues(Set<GoodAttributeValue> goodAttributeValues) {
    if (this.goodAttributeValues == null){
      this.goodAttributeValues = goodAttributeValues;
    } else {
      this.goodAttributeValues.clear();
      this.goodAttributeValues.addAll(goodAttributeValues);
    }
    return this;
  }

}
