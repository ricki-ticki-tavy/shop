package core.database.entity.discount;

import core.database.entity.base.BaseIdentityEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

/**
 * Пороги скидки
 */
@Entity
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class DiscountStep extends BaseIdentityEntity{

  private Discount parent;

  /**
   * Минимальное значение
   */
  private double min;

  /**
   * Максимальное значение
   */
  private double max;

  /**
   * Процент скидки
   */
  private double discountPercent;

  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  public Discount getParent() {
    return parent;
  }

  public DiscountStep setParent(Discount parent) {
    this.parent = parent;
    return this;
  }

  public double getMin() {
    return min;
  }

  public DiscountStep setMin(double min) {
    this.min = min;
    return this;
  }

  public double getMax() {
    return max;
  }

  public DiscountStep setMax(double max) {
    this.max = max;
    return this;
  }

  public double getDiscountPercent() {
    return discountPercent;
  }

  public DiscountStep setDiscountPercent(double discountPercent) {
    this.discountPercent = discountPercent;
    return this;
  }

}
