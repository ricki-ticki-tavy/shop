package core.database.entity.discount;

import core.database.entity.good.Good;
import core.database.entity.good.GoodGroup;
import core.database.entity.base.BaseNamedEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.util.Set;


/**
 * Скидка
 */
@Entity
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class Discount extends BaseNamedEntity{
  /**
   * Товары, участвующие в скидке. Могут быть не указаны, тогда нет огрнаичения по товарам
   */
  private Set<Good> goodSet;

  /**
   * Список групп товаров, участвующих в акции. Может быть пустым, тогда нет ограничений по группам
   */
  private Set<GoodGroup> goodGroupSet;

  /**
   * Пороги скидки
   */
  private Set<DiscountStep> discountStepSet;

  /**
   * Тип скидки
   */
  private DiscountType discountType;

  @ManyToMany(fetch = FetchType.EAGER)
  public Set<Good> getGoodSet() {
    return goodSet;
  }

  public Discount setGoodSet(Set<Good> goodSet) {
    this.goodSet = goodSet;
    return this;
  }

  @ManyToMany(fetch = FetchType.EAGER)
  public Set<GoodGroup> getGoodGroupSet() {
    return goodGroupSet;
  }

  public Discount setGoodGroupSet(Set<GoodGroup> goodGroupSet) {
    this.goodGroupSet = goodGroupSet;
    return this;
  }

  @OneToMany(fetch = FetchType.EAGER, orphanRemoval = true, cascade = CascadeType.ALL, mappedBy = "parent")
  public Set<DiscountStep> getDiscountStepSet() {
    return discountStepSet;
  }

  public Discount setDiscountStepSet(Set<DiscountStep> discountStepSet) {
    this.discountStepSet = discountStepSet;
    return this;
  }

  public DiscountType getDiscountType() {
    return discountType;
  }

  public Discount setDiscountType(DiscountType discountType) {
    this.discountType = discountType;
    return this;
  }
}
