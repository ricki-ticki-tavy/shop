package core.database.entity.discount;

public enum DiscountType{
  BY_SUMM, BY_QUANTITY
}
