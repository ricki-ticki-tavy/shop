package core.database.entity;

import core.database.entity.base.BaseNamedEntity;
import core.database.entity.discount.Discount;
import core.database.entity.photo.Photo;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

/**
 * Акции
 */
@Entity
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class Promo extends BaseNamedEntity{

  /**
   * Набор сменяемых картинок для отображения акции
   */
  private Set<Photo> photoSet;

  /**
   * Описание акции
   */
  private String description;

  /**
   * Привязанная к акции скидка, если есть.
   */
  private Discount discount;

  /**
   * Дата с которой акция видна на сайте
   */
  private Date publishStartDate;

  /**
   * Последний день видимости на сайте акции
   */
  private Date publishLastDate;

  /**
   * Первый день действия акции
   */
  private Date activeStartDate;

  /**
   * Последний день действия акции
   */
  private Date activeLastDate;

  /**
   * Ссылка для перехода при клике на картинке промоакции.
   */
  private String promoUrl;

  @ManyToMany(fetch = FetchType.EAGER)
  public Set<Photo> getPhotoSet() {
    return photoSet;
  }

  public Promo setPhotoSet(Set<Photo> photoSet) {
    this.photoSet = photoSet;
    return this;
  }

  public String getDescription() {
    return description;
  }

  public Promo setDescription(String description) {
    this.description = description;
    return this;
  }

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "discountId")
  public Discount getDiscount() {
    return discount;
  }

  public Promo setDiscount(Discount discount) {
    this.discount = discount;
    return this;
  }

  public Date getPublishStartDate() {
    return publishStartDate;
  }

  public Promo setPublishStartDate(Date publishStartDate) {
    this.publishStartDate = publishStartDate;
    return this;
  }

  public Date getPublishLastDate() {
    return publishLastDate;
  }

  public Promo setPublishLastDate(Date publishLastDate) {
    this.publishLastDate = publishLastDate;
    return this;
  }

  public Date getActiveStartDate() {
    return activeStartDate;
  }

  public Promo setActiveStartDate(Date activeStartDate) {
    this.activeStartDate = activeStartDate;
    return this;
  }

  public Date getActiveLastDate() {
    return activeLastDate;
  }

  public Promo setActiveLastDate(Date activeLastDate) {
    this.activeLastDate = activeLastDate;
    return this;
  }

  public String getPromoUrl() {
    return promoUrl;
  }

  public Promo setPromoUrl(String promoUrl) {
    this.promoUrl = promoUrl;
    return this;
  }
}
