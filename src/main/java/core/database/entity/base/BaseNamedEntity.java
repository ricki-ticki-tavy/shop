package core.database.entity.base;

import javax.persistence.*;

/**
 * Базовый класс для именованных сущностей
 */
@MappedSuperclass
public class BaseNamedEntity extends BaseIdentityEntity{
  private String name;

  @Column(nullable = false, length = 64)
  public String getName() {
    return name;
  }

  public BaseNamedEntity setName(String name) {
    this.name = name;
    return this;
  }

  public BaseNamedEntity(String name){
    this.name = name;
  }

  public BaseNamedEntity(){

  }

}
