package core.database.entity.counter;

import core.database.entity.base.BaseIdentityEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import java.util.Date;

/**
 * Накопитель для счетчика. Очищается раз в сутки
 */
@Entity
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class DayShowCounterLog extends BaseIdentityEntity{

  private DayShowCounter parent;

  /**
   * адрес с которого выполнен просмотр.
   */
  private String ip;

  /**
   * Дата первого захода.
   */
  private Date created;

  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  public DayShowCounter getParent() {
    return parent;
  }

  public void setParent(DayShowCounter parent) {
    this.parent = parent;
  }

  public String getIp() {
    return ip;
  }

  public void setIp(String ip) {
    this.ip = ip;
  }

  public Date getCreated() {
    return created;
  }

  public void setCreated(Date created) {
    this.created = created;
  }
}
