package core.database.entity.counter;

import core.database.entity.base.BaseIdentityEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

/**
 * Счетчик показов
 */
@Entity
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class DayShowCounter extends BaseIdentityEntity{

  private ShowCounter parent;

  /**
   * Значение счетчика
   */
  private int value;

  /**
   * Дата создания счетчика.
   */
  private Date created;

  /**
   * Уникальные заходы за последние сутки
   */
  private Set<DayShowCounterLog> dayShowCounterLogs;


  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  public ShowCounter getParent() {
    return parent;
  }

  public void setParent(ShowCounter parent) {
    this.parent = parent;
  }

  public int getValue() {
    return value;
  }

  public void setValue(int value) {
    this.value = value;
  }

  public Date getCreated() {
    return created;
  }

  public void setCreated(Date created) {
    this.created = created;
  }

  @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "parent")
  public Set<DayShowCounterLog> getDayShowCounterLogs() {
    return dayShowCounterLogs;
  }

  public void setDayShowCounterLogs(Set<DayShowCounterLog> dayShowCounterLogs) {
    this.dayShowCounterLogs = dayShowCounterLogs;
  }
}
