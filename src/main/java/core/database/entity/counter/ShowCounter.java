package core.database.entity.counter;

import core.database.entity.base.BaseIdentityEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import java.util.Date;
import java.util.Set;

/**
 * Счетчик показов чего-либо
 */
@Entity
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class ShowCounter extends BaseIdentityEntity{
  private Date created;
  private Set<DayShowCounter> dayShowCounters;
  private int totalValue;

  public Date getCreated() {
    return created;
  }

  public void setCreated(Date created) {
    this.created = created;
  }

  @OneToMany(fetch = FetchType.LAZY, orphanRemoval = true, cascade = CascadeType.ALL, mappedBy = "parent")
  public Set<DayShowCounter> getDayShowCounters() {
    return dayShowCounters;
  }

  public void setDayShowCounters(Set<DayShowCounter> dayShowCounters) {
    this.dayShowCounters = dayShowCounters;
  }

  public int getTotalValue() {
    return totalValue;
  }

  public void setTotalValue(int totalValue) {
    this.totalValue = totalValue;
  }
}
