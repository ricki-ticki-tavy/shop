package core.database.entity.photo;

import core.database.entity.base.BaseNamedEntity;
import core.database.entity.counter.ShowCounter;
import core.security.SecurityUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.util.Set;

/**
 * Изображение
 */
@Entity
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class Photo extends BaseNamedEntity {

  /**
   * Контрольная сумма
   */
  private String imageHash;

  /**
   * Разрешение изображения Разделитель - "X". Пусто для полноразмерных и origin для оригинала
   */
  private String resolution;

  /**
   * Оригинальное изображение.
   */
  private Photo originalPhoto;

  /**
   * Масштабированные варианты изображения с наложенным водяным знаком
   */
  private Set<Photo> resizedPhotos;

  /**
   * Файл с изображением
   */
  private byte[] image;

  /**
   * Тип изображения
   */
  private ImageType imageType;

  /**
   * Оригинальные имя и расширение файла
   */
  private String originFileName;

  /**
   * Видимость изображения покупателями
   */
  private boolean published;

  /**
   * Счетчик показов
   */
  private ShowCounter showCounter;

  @Column(nullable = false)
  @Basic(fetch = FetchType.LAZY)
  public byte[] getImage() {
    return image;
  }

  public Photo setImage(byte[] image) {
    this.image = image;
    return this;
  }

  public String getOriginFileName() {
    return originFileName;
  }

  public Photo setOriginFileName(String originFileName) {
    this.originFileName = originFileName;
    return this;
  }

  public boolean isPublished() {
    return published;
  }

  public Photo setPublished(boolean published) {
    this.published = published;
    return this;
  }

  @OneToOne(optional = true, fetch = FetchType.LAZY, orphanRemoval = true)
  public ShowCounter getShowCounter() {
    return showCounter;
  }

  public Photo setShowCounter(ShowCounter showCounter) {
    this.showCounter = showCounter;
    return this;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  public Photo getOriginalPhoto() {
    return originalPhoto;
  }

  public Photo setOriginalPhoto(Photo originalPhoto) {
    this.originalPhoto = originalPhoto;
    return this;
  }

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "originalPhoto", orphanRemoval = true, cascade = CascadeType.ALL)
  public Set<Photo> getResizedPhotos() {
    return resizedPhotos;
  }

  public Photo setResizedPhotos(Set<Photo> resizedPhotos) {
    this.resizedPhotos = resizedPhotos;
    return this;
  }

  /**
   * Тип изображения
   * @return
   */
  @Column(nullable = false)
  public ImageType getImageType() {
    return imageType;
  }

  public Photo setImageType(ImageType imageType) {
    this.imageType = imageType;
    return this;
  }

  @Column(nullable = false)
  public String getResolution() {
    return resolution;
  }

  public Photo setResolution(String resolution) {
    this.resolution = resolution;
    return this;
  }

  @Column(length = 64)
  public String getImageHash() {
    return imageHash;
  }

  public Photo setImageHash(String imageHash) {
    this.imageHash = imageHash;
    return this;
  }

  public enum ImageType {
    WATERMARK, ORIGIN, PROCEEDED;
  }
}
