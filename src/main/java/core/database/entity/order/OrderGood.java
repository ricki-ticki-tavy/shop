package core.database.entity.order;

import core.database.entity.base.BaseIdentityEntity;
import core.database.entity.discount.DiscountStep;
import core.database.entity.good.Good;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

/**
 * Товары по заказу
 */
@Entity
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class OrderGood extends BaseIdentityEntity{
  /**
   * Заказ
   */
  private Order parent;

  /**
   * Товар
   */
  private Good good;

  /**
   * Базовая цена за позицию
   */
  private double basePrice;

  /**
   * Заказанное количество
   */
  private double quantity;

  /**
   * Заказанное количество
   */
  private double acceptedQuantity;

  /**
   * Реальная цена по которой ушел товар
   */
  private double price;

  /**
   * Полученная скидка в процентах
   */
  private double calculatedDiscountPercent;

  /**
   * Конечная сумма за позицию
   */
  private double cost;

  /**
   * Если была скидка, то ссылка на ступеньку на основе которой была расчитана скидка на позицию
   */
  private DiscountStep discountStep;

  /**
   * Примечание покупателя к позиции
   */
  private String buyerDescription;

  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  public Order getParent() {
    return parent;
  }

  public void setParent(Order parent) {
    this.parent = parent;
  }

  @ManyToOne(optional = false, fetch = FetchType.LAZY)
  public Good getGood() {
    return good;
  }

  public void setGood(Good good) {
    this.good = good;
  }

  public double getBasePrice() {
    return basePrice;
  }

  public void setBasePrice(double basePrice) {
    this.basePrice = basePrice;
  }

  public double getQuantity() {
    return quantity;
  }

  public void setQuantity(double quantity) {
    this.quantity = quantity;
  }

  public double getAcceptedQuantity() {
    return acceptedQuantity;
  }

  public void setAcceptedQuantity(double acceptedQuantity) {
    this.acceptedQuantity = acceptedQuantity;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  public double getCalculatedDiscountPercent() {
    return calculatedDiscountPercent;
  }

  public void setCalculatedDiscountPercent(double calculatedDiscountPercent) {
    this.calculatedDiscountPercent = calculatedDiscountPercent;
  }

  public double getCost() {
    return cost;
  }

  public void setCost(double cost) {
    this.cost = cost;
  }

  @ManyToOne(fetch = FetchType.EAGER)
  public DiscountStep getDiscountStep() {
    return discountStep;
  }

  public void setDiscountStep(DiscountStep discountStep) {
    this.discountStep = discountStep;
  }

  public String getBuyerDescription() {
    return buyerDescription;
  }

  public void setBuyerDescription(String buyerDescription) {
    this.buyerDescription = buyerDescription;
  }
}
