package core.database.entity.order;

import core.database.entity.base.BaseIdentityEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Entity;
import java.util.Date;

/**
 * Информация по доставке
 */
@Entity
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class DeliveryInfo extends BaseIdentityEntity {

  /**
   * Стоимость за доставку
   */
  private double deliveryCost;

  /**
   * адрес доставки, если есть доставка
   */
  private String deliveryAddress;

  /**
   * Почтовый трекномер, если доставка почтой
   */
  private String trackNumber;

  /**
   * Согласованные дата и время доставки
   */
  private Date acceptedDeliveryDate;

  /**
   * Метод доставки
   */
  private DeliveryMethod deliveryMethod;

  public double getDeliveryCost() {
    return deliveryCost;
  }

  public DeliveryInfo setDeliveryCost(double deliveryCost) {
    this.deliveryCost = deliveryCost;
    return this;
  }

  public String getDeliveryAddress() {
    return deliveryAddress;
  }

  public DeliveryInfo setDeliveryAddress(String deliveryAddress) {
    this.deliveryAddress = deliveryAddress;
    return this;
  }

  public String getTrackNumber() {
    return trackNumber;
  }

  public DeliveryInfo setTrackNumber(String trackNumber) {
    this.trackNumber = trackNumber;
    return this;
  }

  public Date getAcceptedDeliveryDate() {
    return acceptedDeliveryDate;
  }

  public DeliveryInfo setAcceptedDeliveryDate(Date acceptedDeliveryDate) {
    this.acceptedDeliveryDate = acceptedDeliveryDate;
    return this;
  }

  public DeliveryMethod getDeliveryMethod() {
    return deliveryMethod;
  }

  public DeliveryInfo setDeliveryMethod(DeliveryMethod deliveryMethod) {
    this.deliveryMethod = deliveryMethod;
    return this;
  }
}
