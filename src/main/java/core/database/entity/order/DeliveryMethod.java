package core.database.entity.order;

/**
 * Способ доставки
 */
public enum DeliveryMethod {
  OWN, MAIL, COURIER;
}
