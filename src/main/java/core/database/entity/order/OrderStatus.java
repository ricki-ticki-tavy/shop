package core.database.entity.order;

public enum OrderStatus {
  CREATED, COMMITED, DELIVERY_CHANGED, PAYED, SENT, DELIVERED, CLOSED, REJECTED;
}
