package core.database.entity.order;

import core.database.entity.base.BaseIdentityEntity;
import core.database.entity.security.User;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.util.Date;

/**
 * История изменения заказа
 */
@Entity
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class OrderStatusHistory extends BaseIdentityEntity{

  private Order parent;

  private Date created;

  /**
   * Пользователь, выполнивший изменение, если он зарегистрирован (может и не быть, если заказ создан без регистрации)
   */
  private User user;

  /**
   * Новый статус заказа
   */
  private OrderStatus orderStatus;

  /**
   * Примечание к действию
   */
  private String description;

  /**
   * Адрес доставки, если он изменялся
   */
  private DeliveryInfo deliveryInfo;



  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  public Order getParent() {
    return parent;
  }

  public OrderStatusHistory setParent(Order parent) {
    this.parent = parent;
    return this;
  }

  public Date getCreated() {
    return created;
  }

  public OrderStatusHistory setCreated(Date created) {
    this.created = created;
    return this;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "user_id")
  public User getUser() {
    return user;
  }

  public OrderStatusHistory setUser(User user) {
    this.user = user;
    return this;
  }

  public OrderStatus getOrderStatus() {
    return orderStatus;
  }

  public OrderStatusHistory setOrderStatus(OrderStatus orderStatus) {
    this.orderStatus = orderStatus;
    return this;
  }

  public String getDescription() {
    return description;
  }

  public OrderStatusHistory setDescription(String description) {
    this.description = description;
    return this;
  }

  @OneToOne(fetch = FetchType.EAGER)
  @PrimaryKeyJoinColumn
  public DeliveryInfo getDeliveryInfo() {
    return deliveryInfo;
  }

  public OrderStatusHistory setDeliveryInfo(DeliveryInfo deliveryInfo) {
    this.deliveryInfo = deliveryInfo;
    return this;
  }
}
