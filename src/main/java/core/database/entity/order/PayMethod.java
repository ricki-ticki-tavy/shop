package core.database.entity.order;

/**
 * Способ оплаты
 */
public enum PayMethod {
  CASH, CARD, ONLINE_TRANSFER;
}
