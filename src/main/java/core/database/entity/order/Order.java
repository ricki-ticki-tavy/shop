package core.database.entity.order;

import core.database.entity.base.BaseNamedEntity;
import core.database.entity.security.User;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

/**
 * Заказ на сайте
 */
@Entity
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class Order extends BaseNamedEntity{
  /**
   * Пользователь, создавший заказ, если он был зарегистрирован
   */
  private User owner;

  /**
   * Дата создания заказа
   */
  private Date created;

  /**
   * Адрес электронной почты
   */
  private String eMail;

  /**
   * Номер телефона
   */
  private String phone;

  /**
   * Создатель заказа. Если пользователь зарегистрирован, то возмется из пользователя
   */
  private String creatorName;

  /**
   * Истрия заказа
   */
  private Set<OrderStatusHistory> orderStatusHistories;

  /**
   * стоимость товаров после скидки
   */
  private double goodsCost;

  /**
   * Сумма скидки
   */
  private double discountSumm;

  /**
   * Способ оплаты
   */
  private PayMethod payMethod;

  /**
   * Текущий стстус заказа
   */
  private OrderStatus orderStatus;

  /**
   * Последный актуальный способ достаки заказа
   */
  private DeliveryMethod deliveryMethod;

  /**
   * Последняя актуальная информация по доставке, если не самовывоз
   */
  private DeliveryInfo deliveryInfo;

  /**
   * Примечание покупателя к позиции
   */
  private String buyerDescription;

  /**
   * товары по заказу
   */
  private Set<OrderGood> orderGoods;


  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "user_id")
  public User getOwner() {
    return owner;
  }

  public Order setOwner(User owner) {
    this.owner = owner;
    return this;
  }

  @Column(nullable = false)
  public Date getCreated() {
    return created;
  }

  public void setCreated(Date created) {
    this.created = created;
  }

  public String geteMail() {
    return eMail;
  }

  public Order seteMail(String eMail) {
    this.eMail = eMail;
    return this;
  }

  public String getPhone() {
    return phone;
  }

  public Order setPhone(String phone) {
    this.phone = phone;
    return this;
  }

  public String getCreatorName() {
    return creatorName;
  }

  public Order setCreatorName(String creatorName) {
    this.creatorName = creatorName;
    return this;
  }

  @OneToMany(fetch = FetchType.LAZY, orphanRemoval = true, cascade = CascadeType.ALL,  mappedBy = "parent")
  public Set<OrderStatusHistory> getOrderStatusHistories() {
    return orderStatusHistories;
  }

  public Order setOrderStatusHistories(Set<OrderStatusHistory> orderStatusHistories) {
    this.orderStatusHistories = orderStatusHistories;
    return this;
  }

  public double getGoodsCost() {
    return goodsCost;
  }

  public Order setGoodsCost(double goodsCost) {
    this.goodsCost = goodsCost;
    return this;
  }

  public double getDiscountSumm() {
    return discountSumm;
  }

  public Order setDiscountSumm(double discountSumm) {
    this.discountSumm = discountSumm;
    return this;
  }

  public PayMethod getPayMethod() {
    return payMethod;
  }

  public Order setPayMethod(PayMethod payMethod) {
    this.payMethod = payMethod;
    return this;
  }

  public OrderStatus getOrderStatus() {
    return orderStatus;
  }

  public Order setOrderStatus(OrderStatus orderStatus) {
    this.orderStatus = orderStatus;
    return this;
  }

  public DeliveryMethod getDeliveryMethod() {
    return deliveryMethod;
  }

  public Order setDeliveryMethod(DeliveryMethod deliveryMethod) {
    this.deliveryMethod = deliveryMethod;
    return this;
  }

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "deliveryInfo_id")
  public DeliveryInfo getDeliveryInfo() {
    return deliveryInfo;
  }

  public Order setDeliveryInfo(DeliveryInfo deliveryInfo) {
    this.deliveryInfo = deliveryInfo;
    return this;
  }

  public String getBuyerDescription() {
    return buyerDescription;
  }

  public void setBuyerDescription(String buyerDescription) {
    this.buyerDescription = buyerDescription;
  }

  @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "parent")
  public Set<OrderGood> getOrderGoods() {
    return orderGoods;
  }

  public void setOrderGoods(Set<OrderGood> orderGoods) {
    this.orderGoods = orderGoods;
  }
}
