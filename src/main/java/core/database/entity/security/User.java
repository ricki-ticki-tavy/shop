package core.database.entity.security;

import core.database.entity.base.BaseNamedEntity;
import core.security.SecurityUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Пользователь
 */
@Entity
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class User extends BaseNamedEntity {
  private Set<Role> roles;
  private Date lockedUntil;
  private boolean enabled;
  private String passwordBlake2;
  private String eMail;
  private String personalization;

  public User(String name, String password) {
    super(name);
    this.passwordBlake2 = SecurityUtils.blake2(password);
    roles = new HashSet<>();
    enabled = false;
  }

  public User(){

  }
  //----------------------------------------------------------
  //----------------------------------------------------------

  @Override
  @Column(unique = true)
  public String getName() {
    return super.getName();
  }
  //----------------------------------------------------------

  /**
   * Роли пользователя
   * @return
   */
  @ManyToMany(cascade = CascadeType.DETACH)
  public Set<Role> getRoles() {
    return roles;
  }

  public User setRoles(Set<Role> roles) {
    this.roles = roles;
    return this;
  }
  //----------------------------------------------------------

  /**
   * Дата до которой заблокирован пользователь. Если не задана,то не заблокирован
   * @return
   */
  public Date getLockedUntil() {
    return lockedUntil;
  }

  public User setLockedUntil(Date lockedUntil) {
    this.lockedUntil = lockedUntil;
    return this;
  }
  //----------------------------------------------------------

  /**
   * Признак активности учетной запсиси
   * @return
   */
  @Column(nullable = false)
  public boolean isEnabled() {
    return enabled;
  }

  public User setEnabled(boolean enabled) {
    this.enabled = enabled;
    return this;
  }
  //----------------------------------------------------------

  /**
   * Хэш от пароля пользователя по алгоритму BLAKE2
   * @return
   */
  @Column(length = 128, nullable = false)
  public String getPasswordBlake2() {
    return passwordBlake2;
  }

  public User setPasswordBlake2(String passwordBlake2) {
    this.passwordBlake2 = passwordBlake2;
    return this;
  }

  /**
   * Установить пароль не захешенный.
   * @param passwordBlake2
   * @return
   */
  public User setPassword(String passwordBlake2) {
    this.passwordBlake2 = SecurityUtils.blake2(passwordBlake2);
    return this;
  }
  //----------------------------------------------------------

  /**
   * адрес электронной почты
   * @return
   */
  @Column(length = 128)
  public String geteMail() {
    return eMail;
  }

  public User seteMail(String eMail) {
    this.eMail = eMail;
    return this;
  }
  //----------------------------------------------------------


  /**
   * Как обращаться к пользователю
   * @return
   */
  @Column(nullable = true, length = 20)
  public String getPersonalization() {
    return personalization;
  }

  public User setPersonalization(String personalization) {
    this.personalization = personalization;
    return this;
  }
}
