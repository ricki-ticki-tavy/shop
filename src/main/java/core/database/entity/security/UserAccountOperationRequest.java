package core.database.entity.security;

import core.database.entity.base.BaseNamedEntity;
import core.security.SecurityUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.util.Date;

/**
 * запрос на действие с учетной записью пользователя
 */
@Entity
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class UserAccountOperationRequest extends BaseNamedEntity {
  private Date created;
  private String passwordBlacke2;
  private String eMail;
  private String confirmCode;
  private AccountOperation accountOperation;

  public UserAccountOperationRequest(String name
          , String password
          , String eMail
          , String confirmCode
          , AccountOperation accountOperation) {
    super(name);
    this.passwordBlacke2 = SecurityUtils.blake2(password);
    this.eMail = eMail;
    this.confirmCode = confirmCode;
    this.accountOperation = accountOperation;
    this.created = new Date();
  }

  public UserAccountOperationRequest(){
    created = new Date();
  }
  //----------------------------------------------------------
  //----------------------------------------------------------

  /**
   * Хэш от пароля пользователя по алгоритму BLAKE2
   * @return
   */
  public String getPasswordBlacke2() {
    return passwordBlacke2;
  }

  public UserAccountOperationRequest setPasswordBlacke2(String passwordBlacke2) {
    this.passwordBlacke2 = passwordBlacke2;
    return this;
  }
  //----------------------------------------------------------

  /**
   * адрес электронной почты
   * @return
   */
  @Column(length = 128)
  public String geteMail() {
    return eMail;
  }

  public UserAccountOperationRequest seteMail(String eMail) {
    this.eMail = eMail;
    return this;
  }
  //----------------------------------------------------------

  /**
   * дата и время создания записи
   * @return
   */
  public Date getCreated() {
    return created;
  }

  public UserAccountOperationRequest setCreated(Date created) {
    this.created = created;
    return this;
  }
  //----------------------------------------------------------

  /**
   * Проверочная последовательность для подтверждения пользователя по ссылке, отправленной на почту
   * @return
   */
  public String getConfirmCode() {
    return confirmCode;
  }

  public UserAccountOperationRequest setConfirmCode(String confirmCode) {
    this.confirmCode = confirmCode;
    return this;
  }
  //----------------------------------------------------------

  /**
   * тип операции
   * @return
   */
  public AccountOperation getAccountOperation() {
    return accountOperation;
  }
  //----------------------------------------------------------

  public UserAccountOperationRequest setAccountOperation(AccountOperation accountOperation) {
    this.accountOperation = accountOperation;
    return this;
  }
  //----------------------------------------------------------
  //----------------------------------------------------------


  public enum AccountOperation{
    REGISTER, RESTORES_ACCOUNT_ACCESS;
  }
}
