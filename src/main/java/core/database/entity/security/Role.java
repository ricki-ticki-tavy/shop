package core.database.entity.security;

import core.database.entity.base.BaseNamedEntity;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Entity;

/**
 * Сущность роли в базе
 */
@Entity
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
public class Role extends BaseNamedEntity {

  public static final String ROLE_BUYER_NAME = "buyer";
  public static final String ROLE_ADMIN_NAME = "admin";
  public static final String ROLE_MANAGER_NAME = "manager";

  public static final String SPRING_ROLE_BUYER_NAME = "ROLE_" + ROLE_BUYER_NAME;
  public static final String SPRING_ROLE_ADMIN_NAME = "ROLE_" + ROLE_ADMIN_NAME;
  public static final String SPRING_ROLE_MANAGER_NAME = "ROLE_" + ROLE_MANAGER_NAME;

  public Role(String name) {
    super(name);
  }

  public Role() {
    super();
  }
}
