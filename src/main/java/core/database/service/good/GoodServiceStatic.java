package core.database.service.good;

import api.core.database.service.good.GoodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

public class GoodServiceStatic {
  @Autowired
  private GoodService goodService;

  public GoodService getService(ServletContext servletContext, HttpServletRequest request){
    if (request != null){

    }

    if (goodService == null){
      synchronized (this){
        if (goodService == null){
          SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, servletContext);
        }
      }
    }
    return goodService;
  }
}
