package core.database.service.good;

import api.core.Result;
import api.core.database.service.good.GoodService;
import api.dto.good.GoodDto;
import api.dto.good.SimpleGoodGroupDto;
import core.database.dao.PhotoDao;
import core.database.dao.good.GoodAttributeDto;
import core.database.dao.good.GoodDao;
import core.database.dao.good.GoodGroupDao;
import core.database.entity.good.GoodAttribute;
import core.database.entity.good.GoodGroup;
import core.database.entity.photo.Photo;
import core.system.ResultImpl;
import core.system.error.SystemErrors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static core.database.entity.photo.Photo.ImageType.ORIGIN;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Transactional(Transactional.TxType.REQUIRED)
public class GoodServiceImpl implements GoodService {

  @Autowired
  GoodGroupDao goodGroupDao;

  @Autowired
  GoodDao goodDao;

  @Autowired
  PhotoDao photoDao;

  private void buildNextGroupTreeNode(SimpleGoodGroupDto parent, List<GoodGroup> source) {
    List<GoodGroup> source2 = new ArrayList<>(source);
    source2.iterator().forEachRemaining(goodGroup -> {
      if (goodGroup.getParentGroup() != null && goodGroup.getParentGroup().getId() == parent.getId()) {
        source.remove(goodGroup);
        if (parent.getChildren() == null) {
          parent.setChildren(new ArrayList<>());
        }
        parent.getChildren().add(new SimpleGoodGroupDto(goodGroup));
      }
    });
  }
  //===============================================================================================

  @Override
  public List<SimpleGoodGroupDto> getGoodGroupsTree() {
    List<GoodGroup> rawGroups = goodGroupDao.getAll();
    final List<GoodGroup> rawGroups2 = new ArrayList<>(rawGroups);
    final List<SimpleGoodGroupDto> resTree = new ArrayList<>(rawGroups.size());
    rawGroups.stream().filter(goodGroup -> goodGroup.getParentGroup() == null)
            .forEach(goodGroup -> {
              SimpleGoodGroupDto parent = new SimpleGoodGroupDto(goodGroup);
              rawGroups2.remove(goodGroup);
              buildNextGroupTreeNode(parent, rawGroups2);
              resTree.add(parent);
            });
    return resTree;
  }
  //===============================================================================================

  @Override
  public Result<List<SimpleGoodGroupDto>> getGoodGroupsList(Long parentGroupId) {
    return goodGroupDao.getSubGroupsByParent(parentGroupId);
  }
  //===============================================================================================

  @Override
  public SimpleGoodGroupDto getSimpleGoodGroupById(Long id) {
    GoodGroup goodGroup = goodGroupDao.getById(id);
    return goodGroup != null ? new SimpleGoodGroupDto(goodGroup) : null;
  }
  //===============================================================================================

  @Override
  public Result<Set<GoodAttributeDto>> getGoodGroupAttributes(Long groupId) {
    return goodDao.getGoodGroupAttributes(groupId);
  }
  //===============================================================================================

  @Override
  public Result<GoodDto> getGoodDtoById(Long id) {

    return goodDao.getDtoById(id);
  }
  //===============================================================================================

  @Override
  public Result<SimpleGoodGroupDto> saveOrUpdateGroup(Long id, Long parentId, String groupName, String fileName, byte[] photoData, String groupAttributesJson) {

    if (id == -1) {
      id = null;
    }

    if (parentId == -1) {
      parentId = null;
    }

    GoodGroup goodGroup = null;

    if (id != null) {
      // редактирование существующей группы
      goodGroup = goodGroupDao.getById(id);
      if (goodGroup == null) {
        return ResultImpl.fail(SystemErrors.GOOD_GROUP_NOT_FOUND_BY_ID.getError(id.toString()));
      }
    } else {
      // Новая группа
      goodGroup = new GoodGroup();
      if (parentId != null) {
        GoodGroup parentGoodGroup = goodGroupDao.getById(parentId);
        if (parentGoodGroup == null) {
          return ResultImpl.fail(SystemErrors.GOOD_GROUP_NOT_FOUND_BY_ID.getError(parentId.toString()));
        }
        goodGroup.setParentGroup(parentGoodGroup);
      }
    }

    // Проверить дубликат названия
    if (!goodGroupDao.nameIsAvail(id, parentId, groupName)) {
      return ResultImpl.fail(SystemErrors.GOOD_GROUP_DUPLICATE_NAME.getError(groupName));
    } else {

      goodGroup.setName(groupName);

      if (photoData != null) {
        Photo photoObj = photoDao.savePhoto(fileName, photoData, ORIGIN);
        goodGroup.setIcon(photoObj);
      }

      goodGroupDao.saveOrUpdate(goodGroup);

      Result<String> saveAttrResult = goodGroupDao.saveGroupAttributes(goodGroup, groupAttributesJson);

      return saveAttrResult.isSuccess()
              ? ResultImpl.success(new SimpleGoodGroupDto(goodGroup))
              : ResultImpl.fail(saveAttrResult.getError());
    }
  }
  //===============================================================================================

  @Override
  public Result<String> removeGoodGroup(Long id) {
    return goodGroupDao.remove(id);
  }
  //===============================================================================================

  @Override
  public Result<SimpleGoodGroupDto> moveGoodGroupUpOrDown(Long id, GoodGroupDao.MoveDirection direction) {
    return goodGroupDao.moveGoodGroupUpOrDown(id, direction)
            .chain(goodGroup -> ResultImpl.success(goodGroup == null ? null : new SimpleGoodGroupDto(goodGroup)));
  }
  //===============================================================================================

  @Override
  public Result<List<SimpleGoodGroupDto>> getSubGroupsByParent(Long parentGroupId) {
    return goodGroupDao.getSubGroupsByParent(parentGroupId);
  }
  //===============================================================================================

  @Override
  public Result<GoodDto> saveOrUpdateGood(Long id
          , String name
          , Long goodGroup
          , Long goodSubGroup
          , String goodDescription
          , Long imageIds[]
          , Double goodPrice
          , Double goodQuantity
          , Double goodFreeQuantity
          , boolean goodPublished
          , Map<Long, String> attributes) {

    // если есть код подгруппы, то товар в ней, а не в группе верхнего уровня. Сюда приходят оба кода, хоть один из них лишний
    if (goodSubGroup != null && goodSubGroup > -1) {
      goodGroup = goodSubGroup;
    }

    return goodDao.saveOrUpdateGood(id, name, goodGroup, goodDescription, imageIds
            , goodPrice, goodQuantity, goodFreeQuantity, goodPublished, attributes);
  }
  //===============================================================================================

  @Override
  public Result<List<GoodDto>> getGoodsList(Map<String, String> filter, int pageId, int pageSize) {
    return goodDao.getGoodsListByFilter(filter, pageId, pageSize);
  }
  //===============================================================================================

  @Override
  public Result<String> getGoodAttributesValue(Long goodId) {
    return ResultImpl.success(true)
            .mapSafe(b -> ResultImpl.success(goodDao.getGoodAttributesValue(goodId)));
  }
  //===============================================================================================

}
