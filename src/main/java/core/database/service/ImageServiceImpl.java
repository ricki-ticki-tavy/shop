package core.database.service;

import api.core.Result;
import api.core.database.service.ImageService;
import core.database.dao.PhotoDao;
import core.database.entity.photo.Photo;
import core.system.ResultImpl;
import core.system.error.SystemErrors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

import static core.database.entity.security.Role.SPRING_ROLE_ADMIN_NAME;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Transactional(Transactional.TxType.REQUIRED)
public class ImageServiceImpl implements ImageService {

  @Autowired
  PhotoDao photoDao;

  @Override
  public Result<byte[]> getImageBytes(Long id, String resolution) {
    return resolution.equalsIgnoreCase("origin")
            ?
            // скачивание оригинала изображения
            (SecurityContextHolder.getContext().getAuthentication().getAuthorities().contains(new SimpleGrantedAuthority(SPRING_ROLE_ADMIN_NAME))
                    ? photoDao.getImageBytes(id, resolution)
                    // Права админа - можно
                    : ResultImpl.fail(SystemErrors.IMAGE_BAD_REQUEST_BY_ID.getError(id.toString()))
            )
            : photoDao.getImageBytes(id, resolution);
  }
  //=========================================================================================

  @Override
  public Result<Photo> saveImage(String ext, byte[] data, Photo.ImageType imageType) {
    return ResultImpl.success(photoDao.savePhoto(ext, data, imageType));
  }
  //=========================================================================================
  //=========================================================================================
  //=========================================================================================


}
