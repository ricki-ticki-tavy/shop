package web.controller.rest;
// TODO
// Тесты на dropWeaponByWarrior


import api.core.Result;
import api.core.database.service.ImageService;
import api.core.database.service.UserService;
import api.core.database.service.good.GoodService;
import api.dto.good.GoodDto;
import api.dto.good.SimpleGoodGroupDto;
import com.google.gson.GsonBuilder;
import core.database.dao.good.GoodAttributeDto;
import core.database.dao.good.GoodGroupDao;
import core.database.entity.good.GoodAttributeValue;
import core.database.entity.good.GoodGroup;
import core.database.entity.photo.Photo;
import core.database.entity.security.User;
import core.system.ResultImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import web.controller.base.AbstractRestService;
import web.controller.base.ResultResponse;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * Основной рест-сервер
 */
@RestController
@RequestMapping({"api"})
public class ShopApiRestController extends AbstractRestService {
  public static final String ADMIN_PREFIX_METHOD = "admin/";
  public static final String MANAGER_PREFIX_METHOD = "manager/";
  public static final String BUYER_PREFIX_METHOD = "buyer/";

  public static final String GET_USERS_LIST_METHOD = ADMIN_PREFIX_METHOD + "getUsersList";
  public static final String GET_GOOD_ATTRIBUTES_LIST_METHOD = ADMIN_PREFIX_METHOD + "getGoodAttributesList";
  public static final String GET_GOOD_GROUPS_TREE_METHOD = BUYER_PREFIX_METHOD + "getGoodGroupsTree";
  public static final String GET_GOOD_GROUPS_LIST_METHOD = BUYER_PREFIX_METHOD + "getGoodGroupsList";
  public static final String GET_GOOD_GROUPS_ATTRIBUTES_METHOD = ADMIN_PREFIX_METHOD + "getGroupAttributes";
  public static final String SAVE_OR_UPDATE_GOOD_GROUP_METHOD = ADMIN_PREFIX_METHOD + "saveOrUpdateGoodGroup";
  public static final String REMOVE_GOOD_GROUP_METHOD = ADMIN_PREFIX_METHOD + "removeGoodGroup";
  public static final String MOVE_UP_OR_DOWN_GOOD_GROUP_METHOD = ADMIN_PREFIX_METHOD + "moveUpOrDownGoodGroup";

  public static final String SAVE_OR_UPDATE_GOOD_METHOD = ADMIN_PREFIX_METHOD + "saveOrUpdateGood";
  public static final String GET_GOODS_LIST_METHOD = BUYER_PREFIX_METHOD + "getGoodsList";

  public static final String GOOD_ATTRIBUTE_PREFIX = "goodAttribute_";


  public static final String UPLOAD_IMAGE_METHOD = ADMIN_PREFIX_METHOD + "uploadImage";

  public static final String GET_IMAGE_METHOD = BUYER_PREFIX_METHOD + "images/{resolution}/{imageId}.png";

  public static final String UPDATE_PROFILE_METHOD = BUYER_PREFIX_METHOD + "updateProfile";

  @Autowired
  GoodService goodService;

  @Autowired
  UserService userService;

  @Autowired
  ImageService imageService;

  /**
   * Возвращает список пользователей
   *
   * @param pageIndex
   * @param pageSize
   * @param nameFilter
   * @param lockedFilter
   * @return
   */
  @RequestMapping(method = GET, path = {"/" + GET_USERS_LIST_METHOD}, produces = "application/json; charset=utf-8")
  @ResponseBody
  public String getUsersList(@RequestParam(value = "page") int pageIndex
          , @RequestParam(value = "page", defaultValue = "30") int pageSize
          , @RequestParam(value = "sortByField", defaultValue = "") String sortByField
          , @RequestParam(value = "nameFilter", defaultValue = "") String nameFilter
          , @RequestParam(value = "lockedFilter", defaultValue = "") Boolean lockedFilter) {

    return "";
  }
  //===================================================================================================

  /**
   * Возвращает список дополнительных атрибутов товара
   *
   * @return
   */
  @RequestMapping(method = GET, path = {"/" + GET_GOOD_ATTRIBUTES_LIST_METHOD}, produces = "application/json; charset=utf-8")
  @ResponseBody
  public String getGoodAttributesList() {

    return "";
  }
  //===================================================================================================

  /**
   * Возвращает дерево групп товаров
   *
   * @return
   */
  @RequestMapping(method = GET, path = {"/" + GET_GOOD_GROUPS_TREE_METHOD}, produces = "application/json; charset=utf-8")
  @ResponseBody
  public String getGoodGroupsTree() {
    return ResultResponse.succeedAsJson(GET_GOOD_GROUPS_TREE_METHOD, new GsonBuilder().create().toJson(goodService.getGoodGroupsTree()));
  }
  //===================================================================================================

  /**
   * Возвращает подгруппы для заданной группы
   *
   * @param parentGroupId - код группы. -1 для корня
   * @return
   */
  @RequestMapping(method = GET, path = {"/" + GET_GOOD_GROUPS_LIST_METHOD}, produces = "application/json; charset=utf-8")
  @ResponseBody
  public String getGoodGroupsList(@RequestParam(value = "parentGroupId") long parentGroupId) {
    Result<List<SimpleGoodGroupDto>> listResult = goodService.getGoodGroupsList(parentGroupId);
    return listResult.isSuccess()
            ? ResultResponse.succeedAsJson(GET_GOOD_GROUPS_LIST_METHOD, new GsonBuilder().create().toJson(listResult))
            : ResultResponse.failedAsJson(GET_GOOD_GROUPS_LIST_METHOD, listResult.getError());
  }
  //===================================================================================================

  /**
   * Возвращает подгруппы для заданной группы
   *
   * @param groupId - код группы.
   * @return
   */
  @RequestMapping(method = GET, path = {"/" + GET_GOOD_GROUPS_ATTRIBUTES_METHOD}, produces = "application/json; charset=utf-8")
  @ResponseBody
  public String getGroupsAttributes(@RequestParam(value = "groupId") long groupId) {
    Result<Set<GoodAttributeDto>> listResult = goodService.getGoodGroupAttributes(groupId);
    return listResult.isSuccess()
            ? ResultResponse.succeedAsJson(GET_GOOD_GROUPS_ATTRIBUTES_METHOD, new GsonBuilder().create().toJson(listResult.getResult()))
            : ResultResponse.failedAsJson(GET_GOOD_GROUPS_ATTRIBUTES_METHOD, listResult.getError());
  }
  //===================================================================================================

  @RequestMapping(method = POST, path = {"/" + UPLOAD_IMAGE_METHOD}, produces = "application/json; charset=utf-8")
  @ResponseBody
  public String uploadImage(@RequestParam(value = "photoData") MultipartFile photoData) {
    byte[] photoBytes = null;
    if (!photoData.isEmpty()) {
      try {
        photoBytes = photoData.getBytes();
      } catch (IOException ioe) {
        throw new RuntimeException(ioe);
      }
    }

    Result<Photo> photoResult = imageService.saveImage(photoData.getOriginalFilename(), photoBytes, Photo.ImageType.ORIGIN);

    return photoResult.isSuccess() ?
            ResultResponse.succeedAsJson(UPLOAD_IMAGE_METHOD
                    , new GsonBuilder().create().toJson(photoResult.getResult().getId()))
            : ResultResponse.failedAsJson(UPLOAD_IMAGE_METHOD, photoResult.getError());
  }
  //===================================================================================================

  /**
   * Возвращает дерево групп товаров
   *
   * @param currentId
   * @param parentId
   * @param groupName
   * @param photoData
   * @return
   */
  @RequestMapping(method = POST, path = {"/" + SAVE_OR_UPDATE_GOOD_GROUP_METHOD}, produces = "application/json; charset=utf-8")
  @ResponseBody
  public String saveOrUpdateGoodGroup(@RequestParam(value = "currentId") long currentId
          , @RequestParam(value = "parentId", defaultValue = "parentId") long parentId
          , @RequestParam(value = "groupName") String groupName
          , @RequestParam(value = "groupAttributes", defaultValue = "") String groupAttributes
          , @RequestParam(value = "photoData") MultipartFile photoData
  ) {


    Result<GoodGroup> goodGroupResult = ResultImpl.success(null).mapSafe(a -> {
      byte[] photoBytes = null;
      if (!photoData.isEmpty()) {
        try {
          photoBytes = photoData.getBytes();
        } catch (IOException ioe) {
          throw new RuntimeException(ioe);
        }
      }

      return goodService.saveOrUpdateGroup(currentId
              , parentId
              , groupName
              , photoData.isEmpty() ? null : photoData.getOriginalFilename()
              , photoBytes
              , groupAttributes);
    });
    return goodGroupResult.isSuccess() ?
            ResultResponse.succeedAsJson(SAVE_OR_UPDATE_GOOD_GROUP_METHOD
                    , new GsonBuilder().create().toJson(goodGroupResult.getResult()))
            : ResultResponse.failedAsJson(SAVE_OR_UPDATE_GOOD_GROUP_METHOD, goodGroupResult.getError());
  }
  //===================================================================================================

  @RequestMapping(method = GET, path = {"/" + REMOVE_GOOD_GROUP_METHOD}, produces = "application/json; charset=utf-8")
  @ResponseBody
  public String removeGoodGroup(@RequestParam(value = "currentId") long currentId) {
    Result<String> removeGroupResult = goodService.removeGoodGroup(currentId);
    return removeGroupResult.isSuccess()
            ? ResultResponse.succeedAsJson(REMOVE_GOOD_GROUP_METHOD, removeGroupResult.getResult())
            : ResultResponse.failedAsJson(REMOVE_GOOD_GROUP_METHOD, removeGroupResult.getError());
  }
  //===================================================================================================

  @RequestMapping(method = GET, path = {"/" + MOVE_UP_OR_DOWN_GOOD_GROUP_METHOD}, produces = "application/json; charset=utf-8")
  @ResponseBody
  public String moveUpOrDownGoodGroup(@RequestParam(value = "currentId") long currentId,
                                      @RequestParam(value = "direction") GoodGroupDao.MoveDirection direction) {
    Result<SimpleGoodGroupDto> goodGroupResult = goodService.moveGoodGroupUpOrDown(currentId, direction);
    return goodGroupResult.isSuccess()
            ? ResultResponse.succeedAsJson(MOVE_UP_OR_DOWN_GOOD_GROUP_METHOD, goodGroupResult.getResult())
            : ResultResponse.failedAsJson(MOVE_UP_OR_DOWN_GOOD_GROUP_METHOD, goodGroupResult.getError());
  }
  //===================================================================================================

  @RequestMapping(method = POST, path = {"/" + SAVE_OR_UPDATE_GOOD_METHOD}, produces = "application/json; charset=utf-8")
  @ResponseBody
  public String saveOrUpdateGood(@RequestParam(value = "goodId") Long id
          , @RequestParam(value = "goodName") String name
          , @RequestParam(value = "goodGroup") Long goodGroup
          , @RequestParam(value = "goodSubGroup") Long goodSubGroup
          , @RequestParam(value = "goodDescription") String goodDescription
          , @RequestParam(value = "imageIds") String imageIds
          , @RequestParam(value = "goodPrice") Double goodPrice
          , @RequestParam(value = "goodQuantity") Double goodQuantity
          , @RequestParam(value = "goodFreeQuantity") Double goodFreeQuantity
          , @RequestParam(value = "goodPublished") boolean goodPublished
          , HttpServletRequest request) {
    final Map<Long, String> goodAttributeValues = new HashMap<>();
    Map<String, String[]> params = request.getParameterMap();
    params.keySet().stream()
            .filter(attributeName -> attributeName.startsWith(GOOD_ATTRIBUTE_PREFIX))
            .forEach(attributeName -> goodAttributeValues.put(
                    Long.parseLong(attributeName.substring(GOOD_ATTRIBUTE_PREFIX.length()))
                    , params.get(attributeName)[0])
            );

    Long[] idParts = Arrays.stream(imageIds.split(",")).map(s -> Long.parseLong(s)).collect(Collectors.toList()).toArray(new Long[]{});
    Result<GoodDto> goodResult = goodService.saveOrUpdateGood(id, name, goodGroup, goodSubGroup
            , goodDescription, idParts, goodPrice, goodQuantity, goodFreeQuantity, goodPublished
            , goodAttributeValues);

    return goodResult.isSuccess()
            ? ResultResponse.succeedAsJson(SAVE_OR_UPDATE_GOOD_METHOD, new GsonBuilder().create().toJson(goodResult.getResult()))
            : ResultResponse.failedAsJson(SAVE_OR_UPDATE_GOOD_METHOD, goodResult.getError());
  }
  //===================================================================================================

  @RequestMapping(method = POST, path = {"/" + UPDATE_PROFILE_METHOD}, produces = "application/json; charset=utf-8")
  @ResponseBody
  public String updateProfile(@RequestParam(value = "eMail") String eMail
          , @RequestParam(value = "personalization") String personalization
          , @RequestParam(value = "newPassword") String newPassword
          , @RequestParam(value = "repeatNewPassword") String repeatNewPassword) {
    Result<User> userResult = userService.updateUser(getLoggedinUserName().getResult()
            , eMail, personalization, newPassword, repeatNewPassword, null, null);
    return userResult.isSuccess()
            ? ResultResponse.succeedAsJson(UPDATE_PROFILE_METHOD, "")
            : ResultResponse.failedAsJson(UPDATE_PROFILE_METHOD, userResult.getError());
  }
  //===================================================================================================

  @RequestMapping(method = GET, path = {"/" + GET_IMAGE_METHOD}, produces = MediaType.IMAGE_PNG_VALUE)
  public byte[] getImage(@PathVariable(value = "resolution") String resolution
          , @PathVariable(value = "imageId") Long imageId
          , HttpSession session, HttpServletResponse response) {

    return imageService.getImageBytes(imageId, resolution).getResult();
  }
  //===================================================================================================

  @RequestMapping(method = GET, path = {"/" + GET_GOODS_LIST_METHOD}, produces = "application/json; charset=utf-8")
  @ResponseBody
  public String getGoodsList(@RequestParam(value = "goodsFilter") String goodsFilter
          , @RequestParam(value = "pageId", defaultValue = "0") int pageId
          , @RequestParam(value = "pageSize", defaultValue = "20") int pageSize
  ) {
    Map<String, String> filter = new GsonBuilder().create().fromJson(goodsFilter, Map.class);
    Result<List<GoodDto>> goodsResult = goodService.getGoodsList(filter, pageId, pageSize);

    return goodsResult.isSuccess()
            ? ResultResponse.succeedAsJson(SAVE_OR_UPDATE_GOOD_METHOD, new GsonBuilder().create().toJson(goodsResult.getResult()))
            : ResultResponse.failedAsJson(SAVE_OR_UPDATE_GOOD_METHOD, goodsResult.getError());
  }
  //===================================================================================================
  //===================================================================================================
  //===================================================================================================


}
