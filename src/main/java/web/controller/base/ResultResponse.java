package web.controller.base;

import com.google.gson.GsonBuilder;
import core.system.error.SystemError;

/**
 * класс для сериализации результата выполнения операции
 */
public class ResultResponse {
  private String errorCode;
  private String errorMessage;
  private boolean failed;
  private String result;
  private String doneMethod;

  /**
   * Текстовыйкод ошибки, если таковая имеет место быть
   * @return
   */
  public String getErrorCode() {
    return failed ? errorCode : null;
  }

  public ResultResponse setErrorCode(String errorCode) {
    this.errorCode = errorCode;
    return this;
  }

  /**
   * Расширенное сообщение об ошибке, если таковая имела место быть
   * @return
   */
  public String getErrorMessage() {
    return failed ? errorMessage : null;
  }

  public ResultResponse setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
    return this;
  }

  /**
   * Признак, что при выполнении произошла ошибка
   * @return
   */
  public boolean isFailed() {
    return failed;
  }

  public ResultResponse setFailed(boolean failed) {
    this.failed = failed;
    return this;
  }

  /**
   * Результат, в случае отсутствия ошибки
   * @return
   */
  public String getResult() {
    return failed ? null : result;
  }

  public ResultResponse setResult(String result) {
    this.result = result;
    return this;
  }

  public ResultResponse setResult(Object result) {
    this.result = new GsonBuilder().create().toJson(result);
    return this;
  }

  /**
   * выполненный метод
   * @return
   */
  public String getDoneMethod() {
    return doneMethod;
  }

  public ResultResponse setDoneMethod(String doneMethod) {
    this.doneMethod = doneMethod;
    return this;
  }

  /**
   * Преобразоватьв Json
   * @return
   */
  @Override
  public String toString(){
    return new GsonBuilder().create().toJson(this);
  }

  /**
   * Вернуть результат сошибкой
   * @param gameError
   * @return
   */
  public static ResultResponse failed(String doneMethod, SystemError gameError){
    return new ResultResponse()
            .setFailed(true)
            .setErrorCode(gameError.getCode())
            .setDoneMethod(doneMethod)
            .setErrorMessage(gameError.getMessage());
  }

  /**
   * Сформировать успешный ответ
   * @param result
   * @return
   */
  public static ResultResponse succeed(String doneMethod, String result){
    return new ResultResponse()
            .setFailed(false)
            .setDoneMethod(doneMethod)
            .setResult(result);
  }

  /**
   * Вернуть JSON результат сошибкой
   * @param gameError
   * @return
   */
  public static String failedAsJson(String doneMethod, SystemError gameError){
    return new ResultResponse()
            .setFailed(true)
            .setErrorCode(gameError.getCode())
            .setDoneMethod(doneMethod)
            .setErrorMessage(gameError.getMessage())
            .toString();
  }

  /**
   * Сформировать успешный ответ
   * @param result
   * @return
   */
  public static String succeedAsJson(String doneMethod, String result){
    return new ResultResponse()
            .setFailed(false)
            .setResult(result)
            .setDoneMethod(doneMethod)
            .toString();
  }

  /**
   * Сформировать успешный ответ
   * @param result
   * @return
   */
  public static String succeedAsJson(String doneMethod, Object result){
    return new ResultResponse()
            .setFailed(false)
            .setResult(result)
            .setDoneMethod(doneMethod)
            .toString();
  }

}
