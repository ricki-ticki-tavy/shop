package web.controller.index;

import api.core.Result;
import api.core.database.service.UserService;
import api.core.database.service.good.GoodService;
import api.dto.good.GoodDto;
import api.dto.good.SimpleGoodGroupDto;
import com.google.gson.GsonBuilder;
import core.system.error.SystemErrors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import web.controller.base.AbstractRestService;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Map;

@Controller
public class MainPageController extends AbstractRestService {

  @Autowired
  GoodService goodService;

  @Autowired
  UserService userService;

  @RequestMapping({"/howToBuy"})
  public String howToBuy(Map<String, Object> model) {
    return "howToBuy";
  }

  @RequestMapping({"/about"})
  public String about(Map<String, Object> model) {
    return "about";
  }

  @RequestMapping({"/promo"})
  public String promo(Map<String, Object> model) {
    return "promo";
  }

  @RequestMapping({"/payAndDelivery"})
  public String payAndDelivery(Map<String, Object> model) {
    return "payAndDelivery";
  }

  @RequestMapping({"/faq"})
  public String faq(Map<String, Object> model) {
    return "faq";
  }

  @RequestMapping({"/profile"})
  public String profile(Map<String, Object> model) {
    getLoggedinUserName()
            .chain(userName -> userService.loadByName(userName))
            .peak(user -> {
              model.put("userName", user.getName());
              model.put("eMail", user.geteMail());
              model.put("personalization", user.getPersonalization());
            });
    return "profile";
  }

  @RequestMapping({"/orders"})
  public String orders(Map<String, Object> model) {
    return "orders";
  }

  @RequestMapping({"/users"})
  public String users(Map<String, Object> model) {
    return "users";
  }

  @RequestMapping({"/goodAttributes"})
  public String goodAttributes(Map<String, Object> model) {
    return "goodAttributes";
  }

  @RequestMapping({"/goodsList"})
  public String goodsList(@RequestParam(value = "groupId", defaultValue = "-1") long groupId
          , @RequestParam(value = "topGroupId", defaultValue = "-1") long topGroupId
          , Map<String, Object> model) {
    SimpleGoodGroupDto goodGroup = null;
    SimpleGoodGroupDto topGoodGroup;

    if (groupId >= 0) {
      goodGroup = goodService.getSimpleGoodGroupById(groupId);
      if (goodGroup == null) {
        // группа не найдена
        model.put("groupName", SystemErrors.GOOD_GROUP_NOT_FOUND_BY_ID.getError(String.valueOf(groupId)).getMessage());
        model.put("failed", true);
        return "goodsList";
      }
      model.put("groupName", goodGroup.getName());
      model.put("groupId", goodGroup.getId());
    }

    if (topGroupId >= 0 && groupId >= 0) {
      topGoodGroup = goodService.getSimpleGoodGroupById(topGroupId);
      if (topGoodGroup == null) {
        // группа не найдена
        model.put("groupName", SystemErrors.GOOD_GROUP_NOT_FOUND_BY_ID.getError(String.valueOf(topGroupId)).getMessage());
        model.put("failed", true);
        return "goodsList";
      }
      model.put("groupName", topGoodGroup.getName() + " > " + goodGroup.getName());
    }

    model.put("subGroupsListScript", new GsonBuilder().create().toJson(goodService.getSubGroupsByParent(groupId).getResult()));

    return "goodsList";
  }
  //=================================================================================

  @RequestMapping({"/goodGroupForm"})
  public String goodGroupForm(@RequestParam("parentId") long parentId
          , @RequestParam("currentId") long currentId
          , Map<String, Object> model) {
    model.put("parentId", parentId);
    model.put("currentId", currentId);
    if (parentId >= 0) {
      SimpleGoodGroupDto simpleGoodGroup = goodService.getSimpleGoodGroupById(parentId);
      model.put("parentName", simpleGoodGroup.getName());
    }

    if (currentId >= 0) {
      SimpleGoodGroupDto simpleGoodGroup = goodService.getSimpleGoodGroupById(currentId);
      model.put("groupName", simpleGoodGroup.getName());
      model.put("imageId", "api/buyer/images/200x200/" + simpleGoodGroup.getIconId() + ".png");
      if (simpleGoodGroup.isTopLevel()) {
        model.put("groupAttributes", new GsonBuilder().create().toJson(goodService.getGoodGroupAttributes(currentId).getResult()));
      }
    } else {
      model.put("groupName", "");
    }

    return "goodGroupForm";
  }

  //=================================================================================

  @RequestMapping({"/goodViewForm"})
  public String goodViewForm(@RequestParam("goodId") long goodId
          , Map<String, Object> model) {
    Result<GoodDto> goodDtoResult = goodService.getGoodDtoById(goodId);
    model.put("goodId", goodId);
    if (goodDtoResult.isSuccess()) {
      GoodDto goodDto = goodDtoResult.getResult();
      model.put("goodName", goodDto.getName());
      model.put("goodDescription", goodDto.getDescription());
      model.put("goodImagesIds", goodDto.getImageIdListAsString());
      model.put("goodPrice", (long) goodDto.getPrice() + " р.");
      model.put("goodQuantity", (long) goodDto.getQuantity());
      model.put("goodFreeQuantity", goodDto.getFreeQuantity() >= goodDto.getQuantity()
              ? "ост. " + (long) goodDto.getQuantity()
              : " ост. > " + (long) goodDto.getFreeQuantity());
    }

    return goodDtoResult.isSuccess()
            ? "goodViewForm"
            : "good404";
  }
  //=================================================================================

  @RequestMapping({"/goodForm"})
  public String goodForm(@RequestParam("goodId") long goodId
          , @RequestParam("parentGroupId") long parentGroupId
          , Map<String, Object> model) {
    model.put("goodId", goodId);
    model.put("parentGroupId", parentGroupId);
    model.put("goodService", goodService);

    if (goodId >= 0) {
      SimpleGoodGroupDto simpleGoodGroup = goodService.getSimpleGoodGroupById(parentGroupId);
      model.put("groupName", simpleGoodGroup.getName());
      model.put("imageId", "api/buyer/images/200x200/" + simpleGoodGroup.getIconId() + ".png");
      goodService.getGoodDtoById(goodId)
              .peak(goodDto -> {
                NumberFormat moneyFormatter = new DecimalFormat("#0.00");

                // зачитать значения атрибутов
                model.put("attributesValue", goodService.getGoodAttributesValue(goodId).getResult());
                model.put("goodName", goodDto.getName());
                model.put("imageIds", goodDto.getImageIdListAsString());
                model.put("goodDescription", goodDto.getDescription());
                model.put("goodPrice", moneyFormatter.format(goodDto.getPrice()));
                model.put("goodQuantity", ((long) goodDto.getQuantity()));
                model.put("goodFreeQuantity", ((long) goodDto.getFreeQuantity()));
                model.put("goodPublished", goodDto.isPublished());

              });
    } else {
      model.put("groupName", "");
    }

    return "goodForm";
  }

}