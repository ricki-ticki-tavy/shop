package config;

import boot.config.DatabaseConfiguration;
import boot.config.MvcConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@ComponentScan(basePackages = {"core", "api", "web", "boot.config.security", "tests"})
@EnableTransactionManagement
@ActiveProfiles("test")
@Import({DatabaseConfiguration.class, MvcConfiguration.class})
public class TestMvcContextConfiguration {

}
