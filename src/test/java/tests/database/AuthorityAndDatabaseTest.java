package tests.database;

import boot.config.DatabaseConfiguration;
import boot.config.security.SecurityConfig;
import config.TestContextConfiguration;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.util.Assert;

import static tests.Consts.USER_NAME;
import static tests.Consts.USER_PASSWORD;

/**
 * Проверка Пользователей и авторизации
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {AuthorityAndDatabaseTest.class})
@ContextConfiguration(loader = AnnotationConfigContextLoader.class, classes = {TestContextConfiguration.class, DatabaseConfiguration.class, SecurityConfig.class})
@TestPropertySource(locations = {"classpath:application.yml"})
@ActiveProfiles({"mockMail", "test"})
public class AuthorityAndDatabaseTest {
  public static final String USER_BAD_NAME = "admin12";
  public static final String USER_BAD_PASSWORD = "121212";


  @Autowired
  SessionFactory sessionFactory;

  @Autowired
  AuthenticationProvider authenticationProvider;

  public AuthorityAndDatabaseTest innerDoLoginTest() {
    try {
      authenticationProvider.authenticate(new UsernamePasswordAuthenticationToken(USER_BAD_NAME, USER_PASSWORD, null));
      Assert.isTrue(false, "Авторизация не верным пользователем прошла");
    } catch (Throwable th) {
      Assert.isTrue(th instanceof BadCredentialsException, "Авторизация администратором не прошла");
    }

    try {
      authenticationProvider.authenticate(new UsernamePasswordAuthenticationToken(USER_NAME, USER_BAD_PASSWORD, null));
      Assert.isTrue(false, "Авторизация не верным пользователем прошла");
    } catch (Throwable th) {
      Assert.isTrue(th instanceof BadCredentialsException, "Авторизация администратором не прошла");
    }

    try {
      authenticationProvider.authenticate(new UsernamePasswordAuthenticationToken(USER_NAME, USER_PASSWORD, null));
    } catch (Throwable th) {
      Assert.isTrue(false, "Авторизация администратором не прошла");
    }

    return this;
  }

  @Test
  public void doTest(){
    innerDoLoginTest();
  }

}
