package tests.rest;

import boot.config.DatabaseConfiguration;
import boot.config.security.SecurityConfig;
import config.TestContextConfiguration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.util.Assert;
import tests.Consts;
import web.controller.rest.ShopApiRestController;

/**
 * тест REST
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class, classes = {TestContextConfiguration.class, DatabaseConfiguration.class, SecurityConfig.class})
@TestPropertySource(locations = {"classpath:application.yml"})
@WebMvcTest(controllers = ShopApiRestController.class, secure = true)
@ActiveProfiles({"mockMail", "test"})
public class RestLoadArmyTest {

  public static final String WARRIOR_NAME = "Каша";
  public static final String WARRIOR2_NAME = "Каша 2";
  public static final String WARRIOR_SAVE_NAME = WARRIOR_NAME + " вооружен";
  public static final String WARRIOR_SAVE_NAME_2 = WARRIOR_NAME + " вооружен 2";
  public static final String WARRIOR_NEW_SAVE_NAME = WARRIOR_SAVE_NAME + ". Новый";

  @Autowired
  AuthenticationProvider authenticationProvider;



  @Autowired
  ShopApiRestController gameApiRest;

  private void doLogin() {
    try {
      SecurityContextHolder.getContext().setAuthentication(
              authenticationProvider.authenticate(
                      new UsernamePasswordAuthenticationToken(
                              Consts.USER_NAME
                              , Consts.USER_PASSWORD
                              , null)));
    } catch (Throwable th) {
      Assert.isTrue(false, "Авторизация администратором не прошла");
    }
  }
  //======================================================================================================

  private void doLogout() {
    SecurityContextHolder.clearContext();
  }
  //======================================================================================================

  public RestLoadArmyTest innerDoTest() {
    doLogin();

    // запросим список карт
    String restResult = "";
//    ResultResponse rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
//    Assert.isTrue(rr != null, "Ответ на запрос списка карт не является объектом");
//    Assert.isTrue(!rr.isFailed(), rr.getErrorMessage());

    // удаление экипировки правильное
//    restResult = gameApiRest.removeWarriorAmmunition(contextId, warriorId, WARRIOR_NEW_SAVE_NAME);
//    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
//    Assert.isTrue(rr != null, "Ответ на запрос удадления экипировки не является объектом");
//    Assert.isTrue(!rr.isFailed(), rr.getErrorMessage());
//    restResult = gameApiRest.findHero(contextId, WARRIOR_NAME);
//    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
//    playerWarrior = new GsonBuilder().create().fromJson(rr.getResult(), PlayerHeroDto.class);
//    Assert.isTrue(playerWarrior.getEquipments().size() == 1
//            && !playerWarrior.getEquipments().stream().anyMatch(warriorEquipment -> warriorEquipment.getName().equals(WARRIOR_NEW_SAVE_NAME))
//            , "аммуниция не удалилась");

    doLogout();
    return this;
  }


  @Test
  public void test() {
    innerDoTest();
  }
}
