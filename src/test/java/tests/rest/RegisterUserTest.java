package tests.rest;

import api.core.database.dao.SecurityDao;
import boot.config.DatabaseConfiguration;
import boot.config.security.SecurityConfig;
import com.google.gson.GsonBuilder;
import config.TestContextConfiguration;
import core.database.entity.security.UserAccountOperationRequest;
import core.security.CaptchaService;
import core.security.CaptchaStorageSingleton;
import core.system.error.SystemErrors;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.util.Assert;
import web.controller.base.ResultResponse;
import web.controller.security.UserAccountRestController;

import javax.servlet.http.HttpSession;
import java.util.UUID;

import static org.mockito.Mockito.when;

/**
 * Проверка регистрации пользователей
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class, classes = {TestContextConfiguration.class, DatabaseConfiguration.class, SecurityConfig.class})
@TestPropertySource(locations = { "classpath:application.yml" })
@WebMvcTest(controllers = UserAccountRestController.class, secure = true)
@ActiveProfiles({"mockMail", "test"})
public class RegisterUserTest {
  public static final String USER_DUP_NAME_1 = "admin";
  public static final String USER_NAME_1 = "player1";
  public static final String USER_BAD_NAME_1 = "";
  public static final String USER_PASSWORD_1 = "pwd1";
  public static final String USER_BAD_PASSWORD_1 = "pwd2";
  public static final String USER_EMAIL_1 = "player1@gmail.com";
  public static final String USER_DUP_EMAIL_1 = "admin@admin.ru";
  public static final String USER_BAD_EMAIL_1 = "";

  public static final String USER_NAME_2 = "player2";
  public static final String USER_EMAIL_2 = "player2@gmail.com";



  @Autowired
  AuthenticationProvider authenticationProvider;

  @Autowired
  SecurityDao securityDao;

  @Autowired
  UserAccountRestController registerUserRestController;

  @Autowired
  CaptchaStorageSingleton captchaStorageSingleton;

  @Autowired
  CaptchaService captchaService;

  HttpSession session;


  private void doLogout() {
    SecurityContextHolder.clearContext();
  }
  //======================================================================================================

  public RegisterUserTest innerDoMapTest() {


    // Регистрируем с отсутствующей для сессии капчей. Ошибка
    String restResult = registerUserRestController.registerUser(USER_BAD_NAME_1, USER_PASSWORD_1, USER_PASSWORD_1, USER_EMAIL_1, "0", session);
    ResultResponse rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос регистрации с отсутствующей для сессии капчей не объект" );
    Assert.isTrue(rr.isFailed(), "Пользователь c отсутствующей для сессии капчей создался");
    Assert.isTrue(rr.getErrorCode().equals(SystemErrors.USER_BAD_CAPTCHA.getError().getCode())
            , "Неверная ошибка для создания пользователь c отсутствующей для сессии капчей.");


    byte captchaImg[] = captchaService.getImage(session);
    Assert.isTrue(captchaImg.length > 100, "Неверное изображение капчи");

    String captcha = captchaStorageSingleton.findCaptcha(session.getId());

    // Регистрируем кривой капчей. Ошибка
    restResult = registerUserRestController.registerUser(USER_BAD_NAME_1, USER_PASSWORD_1, USER_PASSWORD_1, USER_EMAIL_1, captcha + "0", session);
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос регистрации с кривой капчей не объект" );
    Assert.isTrue(rr.isFailed(), "Пользователь c кривой капчей создался");
    Assert.isTrue(rr.getErrorCode().equals(SystemErrors.USER_CAPTCHA_NOT_EQUIVALENT.getError().getCode())
            , "Неверная ошибка для создания пользователь кривой капчей.");

    // Регистрируем с пустым именем пользователя. Ошибка
    restResult = registerUserRestController.registerUser(USER_BAD_NAME_1, USER_PASSWORD_1, USER_PASSWORD_1, USER_EMAIL_1, captcha, session);
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос регистрации с пустым пользователем не объект" );
    Assert.isTrue(rr.isFailed(), "Пользователь с пустым именем создался");
    Assert.isTrue(rr.getErrorCode().equals(SystemErrors.USER_NAME_IS_EMPTY.getError().getCode())
            , "Неверная ошибка для создания пользователь с пустым именем.");

    // Регистрируем с разными паролями, один из которых пустой. Ошибка
    restResult = registerUserRestController.registerUser(USER_NAME_1, USER_PASSWORD_1, USER_BAD_PASSWORD_1, USER_EMAIL_1, captcha, session);
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос регистрации с кривыми паролями не объект" );
    Assert.isTrue(rr.isFailed(), "Пользователь с кривыми паролями создался");
    Assert.isTrue(rr.getErrorCode().equals(SystemErrors.USER_PASSWORDS_NOT_EQUALS.getError().getCode())
            , "Неверная ошибка для создания пользователь с кривыми паролями");

    // Регистрируем с разными паролями. Ошибка
    restResult = registerUserRestController.registerUser(USER_NAME_1, USER_PASSWORD_1, "", USER_EMAIL_1, captcha, session);
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос регистрации с кривыми паролями не объект" );
    Assert.isTrue(rr.isFailed(), "Пользователь с кривыми паролями создался");
    Assert.isTrue(rr.getErrorCode().equals(SystemErrors.USER_PASSWORDS_NOT_EQUALS.getError().getCode())
            , "Неверная ошибка для создания пользователь с кривыми паролями");

    // Регистрируем с пустым eMail. Ошибка
    restResult = registerUserRestController.registerUser(USER_NAME_1, USER_PASSWORD_1, USER_PASSWORD_1, USER_BAD_EMAIL_1, captcha, session);
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос регистрации с кривым eMail не объект" );
    Assert.isTrue(rr.isFailed(), "Пользователь с кривым eMail создался");
    Assert.isTrue(rr.getErrorCode().equals(SystemErrors.USER_EMAIL_IS_EMPTY.getError().getCode())
            , "Неверная ошибка для создания пользователь с кривым eMail");

    // Регистрируем правильно. Успех
    restResult = registerUserRestController.registerUser(USER_NAME_1, USER_PASSWORD_1, USER_PASSWORD_1, USER_EMAIL_1, captcha, session);
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос регистрации с правильными данными не объект" );
    Assert.isTrue(!rr.isFailed(), rr.getErrorMessage());


    // Регистрируем второго пользователя с повторным логином admin. ошибка
    restResult = registerUserRestController.registerUser(USER_DUP_NAME_1, USER_PASSWORD_1, USER_PASSWORD_1, USER_EMAIL_1, captcha, session);
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос регистрации с существующим пользователем не объект" );
    Assert.isTrue(rr.isFailed(), "Пользователь с существующим пользователем создался");
    Assert.isTrue(rr.getErrorCode().equals(SystemErrors.USER_NAME_ALREADY_USED.getError("").getCode())
            , "Неверный тип ошибки при отказе регистрации пользователя с существующим пользователем");

    // Регистрируем второго пользователя с повторным eMail-ом от admin. ошибка
    restResult = registerUserRestController.registerUser(USER_NAME_2, USER_PASSWORD_1, USER_PASSWORD_1, USER_DUP_EMAIL_1, captcha, session);
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос регистрации с существующим eMail у пользователя admin не объект" );
    Assert.isTrue(rr.isFailed(), "Пользователь с существующим eMail у пользователя admin создался");
    Assert.isTrue(rr.getErrorCode().equals(SystemErrors.USER_EMAIL_ALREADY_USED.getError("").getCode())
            , "Неверный тип ошибки при отказе регистрации пользователя с существующим eMail у пользователя admin");

    // Регистрируем третьего пользователя с повторным логином от первого зарегистрированного. ошибка
    restResult = registerUserRestController.registerUser(USER_NAME_1, USER_PASSWORD_1, USER_PASSWORD_1, USER_EMAIL_2, captcha, session);
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос регистрации с существующим регистрантом не объект" );
    Assert.isTrue(rr.isFailed(), "Пользователь с существующим регистрантом создался");
    Assert.isTrue(rr.getErrorCode().equals(SystemErrors.USER_NAME_ALREADY_USED.getError("").getCode())
            , "Неверный тип ошибки при отказе регистрации пользователя с существующим регистрантом");

    // Регистрируем пользователя с повторным eMail от первого зарегистрированного. ошибка
    restResult = registerUserRestController.registerUser(USER_NAME_2, USER_PASSWORD_1, USER_PASSWORD_1, USER_EMAIL_1, captcha, session);
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос регистрации с eMail существующего регистранта не объект" );
    Assert.isTrue(rr.isFailed(), "Пользователь с eMail существующего регистранта создался");
    Assert.isTrue(rr.getErrorCode().equals(SystemErrors.USER_EMAIL_ALREADY_USED.getError("").getCode())
            , "Неверный тип ошибки при отказе регистрации пользователя с eMail существующего регистранта");

    // ждем время, отводящееся на подтверждение регестриации, после которого запись уже не считается
    try{
      Thread.sleep(3000);
    } catch (Throwable th){

    }

    // Регистрируем пользователя с повторным логином от первого зарегистрированного. Успех, так как запись устарела
    restResult = registerUserRestController.registerUser(USER_NAME_1, USER_PASSWORD_1, USER_PASSWORD_1, USER_EMAIL_2, captcha, session);
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос регистрации с правильными данными не объект" );
    Assert.isTrue(!rr.isFailed(), rr.getErrorMessage());
    UserAccountOperationRequest registerUserRequest = new GsonBuilder().create().fromJson(rr.getResult(), UserAccountOperationRequest.class);

    //Пробуем выполнить подтверждение регистрации. Даем неверный код. Ошибка
    restResult = registerUserRestController.confirmRegisterUser("asasasas");
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос подтверждения регистрации не объект" );
    Assert.isTrue(rr.isFailed(), "подтверждения регистрации с неверным confirmCode не вернуло ошибки");
    Assert.isTrue(rr.getErrorCode().equals(SystemErrors.USER_CONFIRM_CODE_IS_OUTDATED.getError("").getCode())
            , "Неверный тип ошибки при отказе подтверждения регистрации с неверным confirmCode ");

    //Пробуем выполнить подтверждение регистрации с верным кодом. Успех
    restResult = registerUserRestController.confirmRegisterUser(registerUserRequest.getConfirmCode());
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос подтверждения регистрации не объект" );
    Assert.isTrue(!rr.isFailed(), rr.getErrorMessage());

    // Попытка авторизоваться созданным пользователем
    try {
      SecurityContextHolder.getContext().setAuthentication(
              authenticationProvider.authenticate(
                      new UsernamePasswordAuthenticationToken(
                              USER_NAME_1
                              , USER_PASSWORD_1
                              , null)));
    } catch (Throwable th) {
      Assert.isTrue(false, "Авторизация созданным и подтвержденным пользователем не прошла");
    }

    doLogout();

    captcha = captchaStorageSingleton.createAndGet(session.getId());

    // запрос на восстановление доступа с кривой капчей
    restResult = registerUserRestController.restoreAccountAccess("", "", captcha + "2", session);
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос восстановления доступа к учетной записис (фаза 1) с кривой капчей не объект" );
    Assert.isTrue(rr.isFailed(), "восстановления доступа к учетной записис (фаза 1) с кривой капчей не вернуло ошибки");
    Assert.isTrue(rr.getErrorCode().equals(SystemErrors.USER_CAPTCHA_NOT_EQUIVALENT.getError("").getCode())
            , "Неверный тип ошибки при отказе восстановления доступа к учетной записи (фаза 1) с кривой капчей");

    // запрос на восстановление доступа с незаполненными полями
    restResult = registerUserRestController.restoreAccountAccess("", "", captcha, session);
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос восстановления доступа к учетной записис (фаза 1) с пустыми полями не объект" );
    Assert.isTrue(rr.isFailed(), "восстановления доступа к учетной записис (фаза 1) с пустыми полями не вернуло ошибки");
    Assert.isTrue(rr.getErrorCode().equals(SystemErrors.USER_RESTORE_ACCOUNT_FIELDS_ARE_EMPTY.getError("").getCode())
            , "Неверный тип ошибки при отказе восстановления доступа к учетной записи (фаза 1) с пустыми полями");

    // запрос на восстановление доступа с несуществующим пользователем
    restResult = registerUserRestController.restoreAccountAccess(USER_NAME_1 + "12", "", captcha, session);
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос восстановления доступа к учетной записис (фаза 1) с несуществующим пользователем не объект" );
    Assert.isTrue(rr.isFailed(), "восстановления доступа к учетной записис (фаза 1) с несуществующим пользователем не вернуло ошибки");
    Assert.isTrue(rr.getErrorCode().equals(SystemErrors.USER_RESTORE_ACCOUNT_DOES_NOT_EXISTS.getError("").getCode())
            , "Неверный тип ошибки при отказе восстановления доступа к учетной записи (фаза 1) с несуществующим пользователем");

    // запрос на восстановление доступа с несуществующей почтой
    restResult = registerUserRestController.restoreAccountAccess("", USER_EMAIL_2 + "12", captcha, session);
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос восстановления доступа к учетной записис (фаза 1) с несуществующей почтой не объект" );
    Assert.isTrue(rr.isFailed(), "восстановления доступа к учетной записис (фаза 1) с несуществующей почтой не вернуло ошибки");
    Assert.isTrue(rr.getErrorCode().equals(SystemErrors.USER_RESTORE_ACCOUNT_DOES_NOT_EXISTS.getError("").getCode())
            , "Неверный тип ошибки при отказе восстановления доступа к учетной записи (фаза 1) с несуществующей почтой");

    // запрос на восстановление доступа правильным именем
    restResult = registerUserRestController.restoreAccountAccess(USER_NAME_1, "", captcha, session);
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос восстановления доступа к учетной записиси по логину (фаза 1) не объект" );
    Assert.isTrue(!rr.isFailed(), rr.getErrorMessage());

    // запрос на восстановление доступа c правильной почтой
    restResult = registerUserRestController.restoreAccountAccess("", USER_EMAIL_2, captcha, session);
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос восстановления доступа к учетной записиси по почте (фаза 1) не объект" );
    Assert.isTrue(!rr.isFailed(), rr.getErrorMessage());
    String confirmCode = rr.getResult();

    // запрос на смену пароля с неверным кодом
    restResult = registerUserRestController.confirmRestoreAccountAccess(confirmCode + "1", USER_BAD_PASSWORD_1, USER_BAD_PASSWORD_1);
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос смены пароля с неверным кодом не объект" );
    Assert.isTrue(rr.isFailed(), "запрос смены пароля с неверным кодом не вернул ошибки");
    Assert.isTrue(rr.getErrorCode().equals(SystemErrors.USER_CONFIRM_CODE_IS_OUTDATED.getError("").getCode())
            , "Неверный тип ошибки при отказе запроса смены пароля с неверным кодом ");

    // запрос на смену пароля с пустыми паролями
    restResult = registerUserRestController.confirmRestoreAccountAccess(confirmCode, "", "");
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос смены пароля с пустыми паролями не объект" );
    Assert.isTrue(rr.isFailed(), "запрос смены пароля с пустыми паролями не вернул ошибки");
    Assert.isTrue(rr.getErrorCode().equals(SystemErrors.USER_PASSWORDS_NOT_EQUALS.getError("").getCode())
            , "Неверный тип ошибки при отказе запроса смены пароля с пустыми паролями ");

    // запрос на смену пароля с не совпадающими паролями
    restResult = registerUserRestController.confirmRestoreAccountAccess(confirmCode, "", USER_BAD_PASSWORD_1);
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос смены пароля с не совпадающими паролями не объект" );
    Assert.isTrue(rr.isFailed(), "запрос смены пароля с не совпадающими паролями не вернул ошибки");
    Assert.isTrue(rr.getErrorCode().equals(SystemErrors.USER_PASSWORDS_NOT_EQUALS.getError("").getCode())
            , "Неверный тип ошибки при отказе запроса смены пароля с не совпадающими паролями");

    // запрос на смену пароля с не совпадающими паролями
    restResult = registerUserRestController.confirmRestoreAccountAccess(confirmCode, USER_BAD_PASSWORD_1 + "1", USER_BAD_PASSWORD_1);
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос смены пароля с не совпадающими паролями не объект" );
    Assert.isTrue(rr.isFailed(), "запрос смены пароля с не совпадающими паролями не вернул ошибки");
    Assert.isTrue(rr.getErrorCode().equals(SystemErrors.USER_PASSWORDS_NOT_EQUALS.getError("").getCode())
            , "Неверный тип ошибки при отказе запроса смены пароля с не совпадающими паролями");

    // запрос на смену пароля с не совпадающими паролями
    restResult = registerUserRestController.confirmRestoreAccountAccess(confirmCode, USER_BAD_PASSWORD_1, "");
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос смены пароля с не совпадающими паролями не объект" );
    Assert.isTrue(rr.isFailed(), "запрос смены пароля с не совпадающими паролями не вернул ошибки");
    Assert.isTrue(rr.getErrorCode().equals(SystemErrors.USER_PASSWORDS_NOT_EQUALS.getError("").getCode())
            , "Неверный тип ошибки при отказе запроса смены пароля с не совпадающими паролями");

    // запрос на смену пароля с правильно заполненными данными
    restResult = registerUserRestController.confirmRestoreAccountAccess(confirmCode, USER_BAD_PASSWORD_1, USER_BAD_PASSWORD_1);
    rr = new GsonBuilder().create().fromJson(restResult, ResultResponse.class);
    Assert.isTrue(rr != null, "Ответ на запрос смену пароля с правильно заполненными данными не объект" );
    Assert.isTrue(!rr.isFailed(), rr.getErrorMessage());

    // попытка авторизоваться со старым паролем. Провал
    try {
      SecurityContextHolder.getContext().setAuthentication(
              authenticationProvider.authenticate(
                      new UsernamePasswordAuthenticationToken(
                              USER_NAME_1
                              , USER_PASSWORD_1
                              , null)));
      Assert.isTrue(false, "Авторизация со старым паролем прошла");
    } catch (Throwable th) {
    }

    // попытка авторизоваться с новым паролем. Удача
    try {
      SecurityContextHolder.getContext().setAuthentication(
              authenticationProvider.authenticate(
                      new UsernamePasswordAuthenticationToken(
                              USER_NAME_1
                              , USER_BAD_PASSWORD_1
                              , null)));
    } catch (Throwable th) {
      Assert.isTrue(false, "Авторизация с новым паролем НЕ прошла");
    }


    return this;
  }

  public RegisterUserTest init() {
    return this;
  }

  @Test
  public void doTest() {
    session = Mockito.mock(HttpSession.class);
    when(session.getId()).thenReturn(UUID.randomUUID().toString());
    init()
            .innerDoMapTest();
  }
}
